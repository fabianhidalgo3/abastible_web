class RepVisualizacionEntregasController < ApplicationController
  respond_to :js, :html, :json
  def index
    user = current_usuario
    zonas = user.empleado.zona
    		@regiones = Region.joins(:zona).where(zonas: {id: zonas}).group(:id).order(:id).order(:id)

    @estadoReparto = EstadoReparto.where(id: [1,2])
  end

  def carga_zonas
    user = current_usuario
    @zonas = user.empleado.zona.where(region_id: params[:region]) if !params[:region].blank?
  end

  # GET
  # PARCIAL: Muestra las porciones
  def carga_porciones
    @porciones = PorcionReparto.where(zona_id: params[:zona], abierto: true).order(:codigo)
    respond_with @porciones
  end

  def carga_rutas
    @rutas = RutaReparto.where(porcion_reparto_id: params[:porcion]).order(:codigo)
    respond_with @rutas
  end

  def carga_ordenes
    ruta = params[:ruta].to_i
    listaOrdenes = OrdenReparto.where(estado_reparto_id: [4..5], ruta_reparto_id: ruta)
    @listaOrdenes = listaOrdenes.paginate(:page => params[:page], :per_page => 10)
    respond_with @listaOrdenes
  end


end
