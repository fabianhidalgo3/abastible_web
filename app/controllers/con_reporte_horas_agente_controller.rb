class ConReporteHorasAgenteController < ApplicationController
  def index
    user = current_usuario
    zonas = user.empleado.zona
    @regiones = Region.joins(:zona).where(zonas:{id: zonas}).group(:id)
   end
end
