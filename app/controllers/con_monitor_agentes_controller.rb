class ConMonitorAgentesController < ApplicationController
  def index
    user = current_usuario
    zonas = user.empleado.zona
    @regiones = Region.joins(:zona).where(zonas:{id: zonas}).group(:id)
    @procesoConvenios = ProcesoConvenio.all
   end
end
