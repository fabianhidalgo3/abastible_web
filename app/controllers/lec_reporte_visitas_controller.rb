class LecReporteVisitasController < ApplicationController
  respond_to :js, :html, :json, :pdf
  # => Control de Coordenadas GPS
  def index # > Pagina Principal
    user = current_usuario	# > Busca usuario y zonas y porcion del usuario logueado
    zonas = user.empleado.zona
    @regiones = Region.joins(:zona).where(zonas:{id: zonas}).group(:id)
  end

  # GET
  def carga_zonas
    user = current_usuario
    @zonas = user.empleado.zona.where(region_id: params[:region])
  end

  # GET
  def carga_porciones
    zona = params[:zona].to_i
    mes = params[:mes].to_i
    ano = params[:ano].to_i
    @porciones = Porcion.where(zona_id: zona, mes: mes, ano: ano).order(:codigo)
    respond_with @porciones
  end

  # GET
  # PARCIAL: Muestra las Rutas
  def carga_rutas
    @rutas = Rutum.where(porcion_id: params[:porcion]).order(:codigo)
    respond_with @rutas
  end

  # GET
  # Devuelve @hash con coordenadas GPS
  def carga_ubicacion
    ubicaciones = Array.new
    latitud = params[:latitud].to_s
    longitud = params[:longitud].to_s
    cliente = Cliente.find(params[:cliente_id])
    ubicaciones.push([latitud, longitud, cliente.numero_cliente])
    @hash = Gmaps4rails.build_markers(ubicaciones) do |u, marker|
      marker.lat u[0]
      marker.lng u[1]
      marker.infowindow u[2]
    end
    respond_with @hash
  end

  # > GET
  # > Tabla contenido
  def carga_filtro
     zona_id = params[:zona].to_i
     porcion_id = params[:porcion].to_i
     mes = params[:mes].to_i
     ano = params[:ano].to_i
    if (porcion_id == 0)
      porciones = Porcion.where(zona_id: zona_id, mes: mes, ano: ano)
      rutas = Rutum.where(porcion_id: porciones)
    else
      rutas = Rutum.where(porcion_id: porcion_id, mes: mes, ano:ano)
    end

    listaOrdenes = OrdenLectura.where(estado_lectura_id: [4..5], verificacion: false, rutum_id: rutas)
    @listaOrdenes = listaOrdenes.paginate(:page => params[:page], :per_page => 20)
    respond_with @listaOrdenes
  end





  # Exportar Csv
  def exportar_csv

  end
end
