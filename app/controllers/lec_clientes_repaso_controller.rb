class LecClientesRepasoController < ApplicationController
	respond_to :js, :html, :json
	
	def index
		user = current_usuario
    zonas = user.empleado.zona
    		@regiones = Region.joins(:zona).where(zonas: {id: zonas}).group(:id).order(:id).order(:id)

	end

  def carga_ordenes
    # Rescato Variables de la Vista
      region_id = params[:region].to_i
      zona_id = params[:zona].to_i
      porcion = params[:porcion].to_i
      rutas = params[:ruta].to_i
      lector = params[:lector].to_i
      #Busco Porciones Seleccionadas
      if region_id != 0 && zona_id == 0
         zonas = Zona.where(region_id: region_id)
      else
         zonas = Zona.find(zona_id)
     end
     
     if porcion == 0 && rutas == 0
        porciones = Porcion.where(abierto:true, zona_id: zonas)
        rutas = Rutum.where(porcion_id: porciones)
      end
      if porcion !=0 && rutas == 0
        rutas = Rutum.where(porcion_id: porcion)
      end
   
      if porcion != 0  && rutas != 0
        rutas = rutas
      end
   
      if lector != 0
        empleados = Empleado.find(lector)
      else
        empleados = Empleado.joins(:usuario, :zona).where(usuarios:{perfil_id: [5]}, zonas:{id: zonas})
      end
      p empleados
      @ordenes = OrdenLectura.where(rutum_id: rutas, verificacion: false, estado_lectura_id: [1..3], tipo_lectura: 2).paginate(:page => params[:page], :per_page => 20)
     end
end
