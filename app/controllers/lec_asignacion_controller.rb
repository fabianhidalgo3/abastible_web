class LecAsignacionController < ApplicationController
  respond_to :js, :html, :json

  def index
    user = current_usuario
    zonas = user.empleado.zona
    @regiones = Region.joins(:zona).where(zonas: {id: zonas}).group(:id).order(:id)
    @tipo_lectura = TipoLectura.all
    @estados = EstadoLectura.where(id: [1,2])
  end

  def carga_zonas
    user = current_usuario
    @zonas = user.empleado.zona.where(region_id: params[:region]) if !params[:region].blank?
  end

  # GET
  # PARCIAL: Muestra las porciones
  def carga_porciones
    @porciones = Porcion.where(zona_id: params[:zona], abierto: true).order(:codigo)
    respond_with @porciones
  end

  # GET
  # PARCIAL: Muestra las comunas
  def carga_comunas
    @comunas = Comuna.where(zona_id: params[:zona]).joins(:orden_lectura).where(orden_lecturas: {estado_lectura_id: [1..2]}).group(:id).order(:nombre)
    respond_with @comunas
  end

  # GET
  # Retorna lista de ordenes asignadas o ordenes con asignación
  def carga_rutas
    # Rescato parametros desde la vista y parseo a entero
    zona = params[:zona].to_i
    porcion = params[:porcion].to_i
    tipo_lectura = params[:tipo_lectura].to_i
    estado_orden = params[:estado_lectura].to_i

    # Valido porciones
    (zona == 0) ? porciones = Porcion.where(abierto: true) : porciones = Porcion.where(zona_id: zona, abierto:true)
    if porcion != 0
      porciones = Porcion.find(porcion)
    end
    rutas = Rutum.where(porcion_id: porciones)
    zona = Zona.find(zona) if zona != 0
    # Creo Lista Vacia para devolver las ordenes
    @lista = []
    
    @empleados = zona.empleado.joins(:usuario).where(usuarios: {perfil_id: 5}).order("usuarios.email ASC")
    # Ordenes Sin Asignación
    if estado_orden == 1 && zona != 0
      @variable = 0
      #Creo lista vacia para las guardar las ordenes
      # Busco todas las rutas sin asignar y que esten abiertas
      rutasSinAsignar = Rutum.where(porcion_id: porciones)
      # Recorro todas las rutas sin asignar
      rutasSinAsignar.each do |ruta|
        ordenes = OrdenLectura.where(rutum_id: ruta.id, tipo_lectura_id: tipo_lectura, estado_lectura_id: estado_orden)
        next unless ordenes.count > 0
        row = []
        row.push(ruta.porcion.codigo)
        row.push(ruta.codigo)
        row.push(ordenes.count)
        row.push(ruta.id)
        @lista.push(row)
      end
    end

    #Ordenes con Asignación
    if estado_orden.to_i == 2 && zona != 0
    	@variable = 1
     @empleados.each do |empleado|
        asignaciones = AsignacionLectura.where(empleado_id: empleado.id).joins(:orden_lectura).where(orden_lecturas:{rutum_id: rutas, estado_lectura_id: [2,3], tipo_lectura_id: tipo_lectura}).group(:rutum_id)
        if !asignaciones.nil?
          asignaciones.each do |asignacion|
            #Total de asignaciones por empleado
            asignaciones = AsignacionLectura.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura).where(orden_lecturas: {estado_lectura_id: [2,3],tipo_lectura_id: tipo_lectura}).count 
            p asignaciones
            #Creo lista
            row = []
            row.push(asignacion.rutum.porcion.codigo)
            row.push(asignacion.orden_lectura.rutum.codigo)
            row.push(empleado.nombre_completo)
            row.push(empleado.id)
            row.push(asignacion.orden_lectura.rutum.id)
            row.push(params[:tipo_lectura])
            row.push(asignaciones)
            @lista.push(row)
          end
        end
      end
    end
    @lista.sort! {|a,b| a[1] <=> b[1]}
    respond_with @lista
  end


  # GET
  # Actualiza un lote prefinido de lecturas
  def asignacion_completa
    ruta = Rutum.find(params[:ruta])
    ordenes = OrdenLectura.where(rutum_id: ruta.id,  estado_lectura_id: 1,  tipo_lectura_id: params[:tipo_lectura])
    @totalOrdenes = ordenes.count
    ordenes.each do |orden|
      AsignacionLectura.create(orden_lectura_id: orden.id, empleado_id: params[:empleado], rutum_id: ruta.id)
      orden.update(estado_lectura_id: 2, fecha_asignacion: Time.zone.now)
    end
    respond_with @totalOrdenes
  end

  # GET
  # Dividir Rutas para asignacion
  def dividir_ruta_asignacion
    @ruta_seleccionada = Rutum.where(id: params[:ruta]).first
    @lector = Empleado.where(id: params[:empleado]).first
    @ordenes_lectura = OrdenLectura.where(rutum_id: params[:ruta], estado_lectura_id: 1).order(:secuencia_lector)
  end

  # GET
  # Guarda las Asignaciónes Parciales
  def asignacion_parcial
    @completado = params[:id_boton_asignacion].to_i
    ruta = Rutum.where(id: params[:ruta]).first
    ordenes = params[:ordenes].split(',')
    ordenes.each do |orden_id|
      orden = OrdenLectura.where(id: orden_id).first
      AsignacionLectura.new(rutum_id: ruta.id, orden_lectura_id: orden.id, empleado_id: params[:empleado].to_i).save
      orden.update(estado_lectura_id:  2, fecha_asignacion: Time.zone.now)
      orden.save
    end
    @lector_id = Empleado.where(id: params[:empleado].to_i).first.id
    @ruta_seleccionada_id = ruta.id
    @tipo_lectura_id = OrdenLectura.find(ordenes.first).tipo_lectura_id
    respond_with @completado
  end

  # GET
  # Elimina la ruta por comuna o solo comuna..
  def desasignacion_completa
    ruta = Rutum.find(params[:ruta])
    ordenes = OrdenLectura.where(rutum_id: ruta.id, estado_lectura_id: [2,3], tipo_lectura_id: params[:tipo_lectura]).joins(:asignacion_lectura).where(asignacion_lecturas: {empleado_id: params[:empleado]})
    @totalOrdenes = ordenes.count
    ordenes.each do |o|
      o.asignacion_lectura.destroy
      o.update(estado_lectura_id: 1, fecha_asignacion: '')
    end
    respond_with @totalOrdenes
  end

  # GET
  # Dividir Rutas para Desasignacion
  def dividir_ruta_desasignacion
    @ruta_seleccionada = Rutum.find(params[:ruta])
    @lector = Empleado.find(params[:empleado])
    @asignaciones = AsignacionLectura.where(empleado_id: @lector.id, rutum_id: @ruta_seleccionada.id).joins(:orden_lectura).where(
                                     orden_lecturas: {tipo_lectura_id: params[:tipo_lectura], estado_lectura_id: [2,3]})
  end

  # GET
  # Elimina las asignaciones parciales
  def desasignacion_parcial
    @completado_uno = params[:id_boton_desasignacion].to_i
    ruta = Rutum.find(params[:ruta])
    empleado = params[:empleado]
    ordenes = params[:ordenes].split(',')
    ordenes.each do |orden_id|
      orden = OrdenLectura.find(orden_id)
      orden.asignacion_lectura.destroy
      orden.update(estado_lectura_id:1, fecha_asignacion: '')
    end
    @lector_id = params[:empleado].to_i
    @ruta_seleccionada_id = ruta.id
    @tipo_lectura_id = OrdenLectura.find(ordenes.first).tipo_lectura_id
    respond_with @completado_uno
  end
end
