class LecImportarDespachosPostalesController < ApplicationController

  def import_csv
    csv = params[:file]
    clientes = Cliente.all
    clientes.each do |cliente|
      cliente.update(despacho_postal: false)
    end
    CSV.foreach((csv.path), headers: true) do |row|
      cliente = Cliente.where(numero_cliente: row[0]).first
      cliente.update(despacho_postal: true)
    end
    p Cliente.where(despacho_postal:1)
  end

end
