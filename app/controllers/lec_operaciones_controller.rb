class LecOperacionesController < ApplicationController
  respond_to :js, :html

  def ver_ubicacion 
    ubicaciones = Array.new
		ordenLectura = OrdenLectura.find(params[:orden_id])
    if (!ordenLectura.gps_latitud.nil? && !ordenLectura.gps_longitud.nil?) && (ordenLectura.gps_latitud.to_i != 0  && ordenLectura.gps_longitud.to_i != 0)
    #	detalle = ordenLectura.detalle_orden_lectura.take#DetalleOrdenLectura.where(orden_lectura_id: a.id).first
      ventana = "N Cliente: " + ordenLectura.cliente.numero_cliente.to_s + "\n" + ordenLectura.fecha_ejecucion.strftime('%e/%m/%Y %I:%M:%S %p')
      ubicaciones.push([ordenLectura.gps_latitud, ordenLectura.gps_longitud, ventana])
    end
    @hash = Gmaps4rails.build_markers(ubicaciones) do |u, marker|
      marker.lat u[0]
      marker.lng u[1]
      marker.infowindow u[2]
		end
		respond_with @hash
  end

  def ver_fotografia
    @orden_lectura = OrdenLectura.find(params[:id_orden])
    @numero_cliente = @orden_lectura.cliente.numero_cliente
    @lista_fotos = Fotografium.where(orden_lectura_id: @orden_lectura)
    respond_with @lista_fotos
  end

  # GET
  # Devuelve hash con ubicación del recinto
  def ver_ubicacion_cliente
    ubicaciones = Array.new
    cliente = Cliente.find(params[:cliente])
    @cliente = cliente
    @clienteID = cliente.id
    if (!cliente.gps_latitud.nil? && !cliente.gps_longitud.nil?) && (cliente.gps_latitud.to_i != 0  && cliente.gps_longitud.to_i != 0)
      ventana = cliente.nombre.to_s + " | " + cliente.direccion
      ubicaciones.push([cliente.gps_latitud.to_s, cliente.gps_longitud.to_s, ventana])
      @latitud = cliente.gps_latitud.to_s
      @longitud = cliente.gps_longitud.to_s
    end
    @hash = Gmaps4rails.build_markers(ubicaciones) do |u, marker|
      marker.lat u[0]
      marker.lng u[1]
      marker.infowindow u[2]
    end
		respond_with @hash
  end

  def actualizar_ubicacion
    cliente = Cliente.find(params[:cliente])
    cliente.update(gps_latitud: params[:latitud], gps_longitud: params[:longitud], observacion_recinto: params[:observacion], verificado: true)
    cliente.save
  end

  def remplazar_ubicacion
    cliente = Cliente.find(params[:cliente])
    ultimaOrden = cliente.orden_lectura.last
    if (!ultimaOrden.gps_latitud.nil? && !ultimaOrden.gps_longitud.nil?)
      cliente.update(gps_latitud: ultimaOrden.gps_latitud, gps_longitud: ultimaOrden.gps_longitud, verificado:1)
      cliente.save
    end
  end

  def modificar_orden
    ordenID = params[:orden_id].to_i
    @claves = ClaveLectura.all
    @ordenLectura = OrdenLectura.find(ordenID)
  end

  def editar_orden
   # imagen = params[:file]
    ordenId = params[:orden_id].to_i
    lecturaActual = params[:lectura_actual].to_i
    claveLectura = params[:clave_lectura].to_i
    ordenLectura = OrdenLectura.find(ordenId)
    claveAnterior = ordenLectura.clave_lectura
    #Actualizo Orden Lectura
    ordenLectura.update(clave_lectura_id: claveLectura, lectura_actual: lecturaActual)
    # Creo la Asignacion de modificación
    asignacionModificacion = AsignacionModificacionLectura.create(
      empleado_id: current_usuario.empleado.id,
      lectura_actual: lecturaActual,
      orden_lectura_id: ordenLectura.id,
      clave_lectura_id: claveAnterior,
      fecha_modificacion: Time.now,
      rutum_id: ordenLectura.rutum.id
    )
    
    # #Creo Agrego Imagen Nueva de Lectura.
    # fotografiaAntigua = Fotografium.where(orden_lectura_id: ordenLectura.id).first
    # if !fotografiaAntigua.nil?
    #   #Eliminart fotografia Antigua
    #   fotografiaAntigua.delete
    #   # Crear nueva Fotografia y Subo la nueva Fotografia
    # end


  end



end