class CmImportarOrdenesController < ApplicationController
  def index
    user = current_usuario
    zonas = user.empleado.zona
    @regiones = Region.joins(:zona).where(zonas:{id: zonas}).group(:id)
  end

  def import_csv
    file_csv = params[:file]
    CSV.foreach((file_csv.path), headers: true) do |row|
      estadoCambioMedidor = 1 # Estado de Lectura Sin Asignar
      # Validación de campos de carga masiva.
      secuencia_lector = row[0]
      secuencia_lector = "" if secuencia_lector.nil?
      nombre_ruta = row[1]
      nombre_ruta = "" if nombre_ruta.nil?
      nombre_porcion = row[1][0..4]
      nombre_porcion = "" if nombre_porcion.nil?
      codigo_zonal = row[2]
      codigo_zonal = "" if codigo_zonal.nil?
      nombre_comuna = row[3]
      nombre_comuna = "" if nombre_comuna.nil?
      calle = row[4]
      calle = "" if calle.nil?
      numero_domicilio = row[5]
      numero_domicilio = "" if numero_domicilio.nil?
      ubicacion_medidor = row[11]
      ubicacion_medidor = "" if ubicacion_medidor.nil?
      numero_cliente = row[6]
      numero_cliente = "Error" if numero_cliente.nil?
      numero_medidor = row[7]
      numero_medidor = "Error" if numero_medidor.nil?
      nombre_marca_medidor = row[8]
      nombre_marca_medidor = "" if nombre_marca_medidor.nil?
      nombre_modelo_medidor = row[9]
      nombre_modelo_medidor = "" if nombre_modelo_medidor.nil?
      numero_instalacion = row[10]
      numero_instalacion = "Error" if numero_instalacion.nil?
      color = row[12]
      color = 0 if color.nil?
      if (numero_instalacion != "Error") || (numero_cliente != "Error")
        # Cambiar Región Por defecto
        zona = Zona.where(codigo: codigo_zonal).first
        zona = Zona.create(codigo: codigo_zonal, region_id: 10) if zona.nil?
        #Busco Comunas Precargadas
        comuna = Comuna.where(nombre: nombre_comuna).first
        # Si no existe comuna vinculada la creo
        comuna = Comuna.create(nombre: nombre_comuna, zona_id: zona.id) if comuna.nil?
        # Busco la porcion
        porcion = PorcionCambioMedidor.where(codigo:nombre_porcion, mes: Time.now.month, ano: Time.now.year, zona_id: zona.id).first
        # Si no existe la creo
        porcion = PorcionCambioMedidor.create(codigo: nombre_porcion, mes: Time.now.month, ano: Time.now.year, zona_id: zona.id, abierto:true) if porcion.nil?
        # Busco la ruta
        ruta = RutaCambioMedidor.where(codigo: nombre_ruta, mes: Time.now.month, ano: Time.now.year).first
        # Si no existe lo creo
        ruta = RutaCambioMedidor.create(codigo: nombre_ruta, mes: Time.now.month, ano: Time.now.year, abierto:true, porcion_cambio_medidor_id: porcion.id) if ruta.nil?

        cliente = Cliente.where(numero_cliente: numero_cliente).first if numero_cliente != "0000"
        # Si No Existe lo Creo
        cliente = Cliente.create(numero_cliente: numero_cliente, direccion_completa: calle + " " + numero_domicilio, calle: calle, numero_domicilio: numero_domicilio) if cliente.nil?
        # Busco Instalación
        instalacion = Instalacion.where(codigo: numero_instalacion).first if numero_instalacion != "0000"
        # Si no Existe la creo
        instalacion = Instalacion.create(codigo: numero_instalacion) if instalacion.nil?
        # Busco Marca de Medidor
        marca_medidor = MarcaMedidor.where(nombre: nombre_marca_medidor).first
        # Si no la encuentro la creo
        marca_medidor = MarcaMedidor.create(nombre: nombre_marca_medidor)  if marca_medidor.nil?
        # Busco Modelo Medidor
        modelo_medidor = ModeloMedidor.where(nombre: nombre_modelo_medidor, marca_medidor_id: marca_medidor.id).first
        # Si no lo encuentr lo creo
        modelo_medidor = ModeloMedidor.create(nombre: nombre_modelo_medidor,marca_medidor_id: marca_medidor.id) if modelo_medidor.nil?
        # Creo el Medidor
        medidor = Medidor.where(numero_medidor: numero_medidor, modelo_medidor_id: modelo_medidor.id, ubicacion_medidor: ubicacion_medidor).first if numero_medidor != "0000"
        # Si no existe lo creo
        medidor = Medidor.create(numero_medidor: numero_medidor, modelo_medidor_id: modelo_medidor.id, ubicacion_medidor: ubicacion_medidor) if medidor.nil?
        #Creo el Correlativo de los Códigos de las Ordenes de Lectura
        codigoOrden = OrdenCambioMedidor.last
        if codigoOrden.nil?
          codigoOrden = 1
          codigoOrden = "CN-"+ codigoOrden.to_s
        else
        codigoOrden = codigoOrden.id
        codigoOrden = codigoOrden + 1
        codigoOrden = "CN-" + codigoOrden.to_s
        end
        #Valido si la orden Lectura Existe
        ordenCambioMedidor = OrdenCambioMedidor.where(codigo: codigoOrden).first
        # Si no Existe la creo junto con el detalle_orden_lectura
        if ordenCambioMedidor.nil?
          ordenCambioMedidor = OrdenCambioMedidor.create(
            numero_medidor: medidor.numero_medidor, 
            numero_cliente: cliente.numero_cliente, 
            codigo: codigoOrden,
            secuencia: secuencia_lector,
            instalacion_id: instalacion.id,
            cliente_id: cliente.id,
            ruta_cambio_medidor_id: ruta.id,
            estado_cambio_medidor_id: estadoCambioMedidor,
            fecha_carga: Time.now,
            direccion: calle + " " + numero_domicilio , 
            calle: calle , 
            numero: numero_domicilio,
            verificacion: false,
            comuna_id: comuna.id,
            medidor_id: medidor.id, 
            color: 0)
        end
      end
   end
  end 

end
