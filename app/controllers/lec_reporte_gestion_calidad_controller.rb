class LecReporteGestionCalidadController < ApplicationController
  respond_to :js, :html, :json, :pdf

	def index # > Pagina Principal
		user = current_usuario	# > Busca usuario y zonas y porcion del usuario logueado
    zonas = user.empleado.zona
		@regiones = Region.joins(:zona).where(zonas:{id: zonas}).group(:id)
	end

	# GET
	def carga_porciones 			
		zona = params[:zona].to_i
		mes = params[:mes].to_i
		ano = params[:ano].to_i
		@porciones = Porcion.where(zona_id: zona, mes: mes, ano: ano).order(:codigo)
		respond_with @porciones
	end

	def carga_zonas
    user = current_usuario
    @zonas = user.empleado.zona.where(region_id: params[:region])
  end
	
	# > Funcion encargada de buscar ordenes..
	# > Params[porcion]
	def carga_filtro 			
	p 	porcion_id = params[:porcion].to_i
		if porcion_id == 0
	 		porcion = Porcion.where(zona_id: params[:zona], mes: params[:mes].to_i, ano: params[:ano].to_i)
		else
		 porcion = Porcion.find(params[:porcion])
		end
		rutas = Rutum.where(porcion_id: porcion, mes:params[:mes].to_i, ano: params[:ano].to_i)
    @asignaciones = AsignacionModificacionLectura.joins(:orden_lectura).where(orden_lecturas:{rutum_id: rutas}).order(:rutum_id)
		 @asignaciones 
		respond_with @asignaciones
	end


end
