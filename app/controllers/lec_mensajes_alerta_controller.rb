class LecMensajesAlertaController < ApplicationController
  respond_to :js, :html, :json

  def aprobar_orden
    ordenLectura = OrdenLectura.find(params[:orden_id])
    ordenLectura.estado_lectura_id = 5
    ordenLectura.aprobado = true
    #asignacion_analisis = AsignacionAnalisis.create(empleado_id: current_usuario.empleado.id, orden_lectura_id: ordenLectura.id, lectura_modificada: ordenLectura.detalle_orden_lectura.first.lectura_actual)
    ordenLectura.modulo_analisis = params[:analisis].to_i
    ordenLectura.save
    @completado = true
    respond_with @completado
  end

  def modificar_lectura
    ordenLectura = OrdenLectura.find(params[:orden_id])
    #asignacion_analisis = AsignacionAnalisis.create(empleado_id: current_usuario.empleado.id, orden_lectura_id: ordenLectura.id, lectura_modificada: ordenLectura.detalle_orden_lectura.first.lectura_actual)
    ordenLectura.update(lectura_actual: params[:lectura])
    ordenLectura.update(aprobado: true, estado_lectura_id: 5,modulo_analisis: params[:analisis].to_i)
    asignacionModificacion = AsignacionModificacionLectura.create(
      empleado_id: current_usuario.empleado.id,
      lectura_actual: params[:lectura],
      orden_lectura_id: ordenLectura.id,
      fecha_modificacion: Time.now,
      rutum_id: ordenLectura.rutum.id
    )
    @completado = true
    respond_with @completado
  end

  def enviar_verificacion
    # Busco y actualizo la orden dudosa
    ordenLectura = OrdenLectura.where(id: params[:orden_id]).first
   # asignacion_analisis = AsignacionAnalisis.create(empleado_id: current_usuario.empleado.id, orden_lectura_id: ordenLectura.id, lectura_modificada: ordenLectura.detalle_orden_lectura.first.lectura_actual)
    ordenLectura.verificacion = true
    ordenLectura.modulo_analisis = params[:analisis].to_i
    ordenLectura.save

    # > Creo la nueva orden lectura en estado de Verificación
    nuevaOrdenLectura = OrdenLectura.create(
      :codigo => ordenLectura.codigo,
      :secuencia_lector => ordenLectura.secuencia_lector,
      :direccion => ordenLectura.direccion,
      :direccion_entrega => ordenLectura.direccion_entrega,
      :numero_poste => ordenLectura.numero_poste,
      :observacion_lector => ordenLectura.observacion_lector,
      :fecha_carga => ordenLectura.fecha_carga,
      :fecha_propuesta => ordenLectura.fecha_propuesta,
      :instalacion_id => ordenLectura.instalacion_id,
      :verificacion => false,
      :cliente_id => ordenLectura.cliente_id,
      :rutum_id => ordenLectura.rutum_id,
      :estado_lectura_id => 1,
      :tipo_lectura_id => 2,
      :medidor_id => ordenLectura.medidor_id,
      :comuna_id => ordenLectura.comuna_id,
      :modulo_analisis => params[:analisis].to_i,
      :color => ordenLectura.color,
      :lectura_anterior => ordenLectura.lectura_anterior,
      :consumo_promedio => ordenLectura.consumo_promedio,
      :consumo_minimo => ordenLectura.consumo_minimo,
      :consumo_maximo => ordenLectura.consumo_maximo,
      :numero_medidor => ordenLectura.numero_medidor  
    )
    nuevaOrdenLectura.save
    @completado = true
    respond_with @completado
  end

end
