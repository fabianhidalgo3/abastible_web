class LecMonitorLectoresController < ApplicationController
  respond_to :js, :html, :json

	def index
		user = current_usuario
    zonas = user.empleado.zona
    @regiones = Region.joins(:zona).where(zonas:{id: zonas}).group(:id).order(:id)
    @tipoLecturas = TipoLectura.all
  end

  # GET
  # Retorna lista con zonas
  def carga_zonas
    user = current_usuario
    empleado = user.empleado
    @zonas = Zona.where(region_id: params[:region]).joins(:empleado).where(empleados: {id: empleado.id})
    respond_with @zonas
  end

  # GET
  # Muestra todas las comunas con ordenes en estado 1, 2
  def carga_comunas
    @comunas = Comuna.where(zona_id: params[:zona]).joins(:orden_lectura).where(orden_lecturas: {estado_lectura_id: [1..2]}).group(:id).order(:nombre)
    respond_with @comunas
  end

  # GET
  # Retorna lista con porciones
	def carga_porciones
		zona = params[:zona].to_i
		mes = params[:mes].to_i
		ano = params[:ano].to_i
		@porciones = Porcion.where(zona_id: zona, mes: mes, ano: ano).order(:codigo)
		respond_with @porciones
	end

  # GET
  # Retorna lista con rutas
  def carga_rutas
    if !params[:comuna].blank?
      @rutas = Rutum.where(porcion_id: params[:porcion]).joins(:orden_lectura).where(orden_lecturas: {comuna_id: params[:comuna], estado_lectura_id:[2..5]}).group(:id)
    else
      @rutas = Rutum.where(porcion_id: params[:porcion].to_i)
    end
    respond_with @rutas
  end

  # GET
  # Retorna lista con empleados
  def carga_lectores
    @lectores = Empleado.joins(:usuario, :zona).where(usuarios:{perfil_id: [6,7]}, zonas:{id: params[:zona]})
    respond_with @lectores
  end

  # GET
  # Devuelve @hash con ubicaciones
  def carga_ubicacion
    ubicaciones = Array.new
    ordenes = OrdenLectura.where(rutum_id: params[:ruta_id], estado_lectura_id: [4,5]).joins(:asignacion_lectura).where(asignacion_lecturas: {empleado_id: params[:emp_id]}).order("orden_lecturas.fecha_ejecucion ASC")
      ordenes.each do |orden|
        fecha_ejecucion = orden.fecha_ejecucion
        ventana = "N Cliente: " + orden.cliente.numero_cliente.to_s + "\n" +
        orden.fecha_ejecucion.strftime('%e/%m/%Y %I:%M:%S %p')
        ubicaciones.push([orden.gps_latitud, orden.gps_longitud, ventana])
      end
    @hash = Gmaps4rails.build_markers(ubicaciones) do |u, marker|
      marker.lat u[0]
      marker.lng u[1]
      marker.infowindow u[2]
    end
    respond_with @hash
  end

  # GET
  # Contenido de la tabla
  def carga_filtro
    mes = params[:mes].to_i
    ano = params[:ano].to_i
    porcion  = params[:porcion].to_i
    lista = Array.new
    @totales = Array.new
    tAsignado = 0, tCargado = 0, tPendiente = 0, tEfectivo = 0, tNoEfectivo = 0, tLeido = 0, tVerificaciones = 0, tEfectividad = 0
    asignado = 0, cargado = 0, efectivo = 0, noEfectivo = 0, pendientes = 0, leido = 0, ol_repaso = 0, efectividad = 0
    tipo_lectura = params[:tipo_lectura]
    contador = 0
    if !params[:region_id].blank? && params[:porcion].blank?
      region = Region.find(params[:region_id])
    end
   
    p rutas = Rutum.where(porcion_id: params[:porcion], mes: mes, ano:ano)
    if rutas.blank?
      p rutas = Rutum.joins(:porcion).where(porcions: {zona_id: params[:zona], mes:mes, ano:ano})
    end

    empleados = Empleado.joins(:usuario, :asignacion_lectura).where(usuarios: {perfil_id: 5}, asignacion_lecturas: {rutum_id: rutas}).group(:id)
    empleados.each do |empleado|
      asignaciones = AsignacionLectura.where(empleado_id: empleado.id).joins(:orden_lectura).where(orden_lecturas:{estado_lectura_id: [2..5], rutum_id: rutas, tipo_lectura_id: tipo_lectura}).group(:rutum_id)
      asignaciones.each do |asignacion|
        ordenes = AsignacionLectura.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura).where(orden_lecturas: {tipo_lectura_id: tipo_lectura})
        asignado = ordenes.joins(:orden_lectura).where(orden_lecturas: {estado_lectura_id: [2..5]}).count
        cargado = ordenes.joins(:orden_lectura).where(orden_lecturas: {estado_lectura_id: 3}).count
        efectivo = ordenes.joins(:orden_lectura => [:clave_lectura]).where(clave_lecturas:{ efectivo: true},orden_lecturas: {estado_lectura_id: [2..5]}).count
        noEfectivo = ordenes.joins(:orden_lectura => [:clave_lectura]).where(clave_lecturas:{ efectivo: false}, orden_lecturas: {estado_lectura_id: [2..5]}).count
        pendientes = ordenes.joins(:orden_lectura).where(orden_lecturas: {estado_lectura_id: [2..3]}).count
        leido = ordenes.joins(:orden_lectura).where(orden_lecturas: {estado_lectura_id: [4..5]}).count
        efectividad = ((efectivo*100.0)/leido).round(2)
        hora = ordenes.joins(:orden_lectura).where(orden_lecturas: {estado_lectura_id: [4..5]}).order("orden_lecturas.fecha_ejecucion ASC")
        if !hora.blank? then
          horaInicio = hora.first.orden_lectura.fecha_ejecucion.strftime("%H:%M:%S %d/%m/%Y")
          horaTermino = hora.last.orden_lectura.fecha_ejecucion.strftime("%H:%M:%S %d/%m/%Y")
        else
          horaInicio = " "
          horaTermino = " "
        end
        transmitido = AsignacionLectura.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura).where(orden_lecturas: {estado_lectura_id: 5,tipo_lectura_id: tipo_lectura}).count                      
        lista.push([empleado.nombre_completo, asignacion.orden_lectura.rutum.codigo, asignado,
                    cargado, efectivo, noEfectivo, pendientes, leido, efectividad, horaInicio, horaTermino,
                    transmitido, asignacion.rutum_id, asignacion.empleado_id, ol_repaso,
                    asignacion.orden_lectura.rutum.porcion.codigo])
      asignado = 0, cargado = 0, efectivo = 0, noEfectivo = 0, pendientes = 0, leido = 0, ol_repaso = 0, efectividad = 0
      end

    end

    @lista = lista.sort! {|a,b| a[1] <=> b[1]}
    respond_with @lista.paginate(:page => params[:page], :per_page => 10)
  end


end
