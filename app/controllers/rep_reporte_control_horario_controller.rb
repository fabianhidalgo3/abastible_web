class RepReporteControlHorarioController < ApplicationController
  respond_to :js, :html, :json

	def index
		user = current_usuario
    zonas = user.empleado.zona
		@regiones = Region.joins(:zona).where(zonas:{id: zonas}).group(:id)
	end

	# GET
	def carga_zonas
    user = current_usuario
    @zonas = user.empleado.zona.where(region_id: params[:region])
  end

	# => Carga Porciones
	def carga_porciones
    @porciones = PorcionReparto.where(zona_id: params[:zona], abierto: true).order(:codigo)
		respond_with @porciones
	end

	def carga_filtro
		totalHoras = 0
		totalMinutos = 0
		totalSegundos = 0
		#porciones = PorcionReparto.where(zona_id: params[:zona],abierto: true)
    rutas = RutaReparto.where(porcion_reparto_id: params[:porcion])
		empleados = Empleado.where(id: params[:empleado].to_i).joins(:usuario).group(:id)
		if empleados.blank?
			empleados = Empleado.joins(:usuario).where(usuarios: {perfil_id: 5}).group(:id)
		end
   	lista = Array.new
		empleados.each do |empleado|
      asignaciones = AsignacionReparto.where(empleado_id: empleado.id).joins(:orden_reparto).where(orden_repartos:{estado_reparto_id: [2..5], ruta_reparto_id: rutas})
			asignacionesRutas = asignaciones.group(:ruta_reparto_id)
			asignacionesRutas.each do |asignacion|
				ruta = asignacion.ruta_reparto
				asignado = AsignacionReparto.where(empleado_id: empleado.id).joins(:orden_reparto).where(orden_repartos:{estado_reparto_id: [2..5], ruta_reparto_id: ruta.id}).count
        pendiente = AsignacionReparto.where(empleado_id: asignacion.empleado_id, ruta_reparto_id: asignacion.ruta_reparto_id).joins(:orden_reparto).where(orden_repartos: {estado_reparto_id: 3}).count

				hora = OrdenReparto.where(ruta_reparto_id: asignacion.ruta_reparto_id, estado_reparto_id: [4,5]).joins(:asignacion_reparto).where(asignacion_repartos: {empleado_id: asignacion.empleado_id}).order("orden_repartos.fecha_ejecucion ASC")
        if !hora.blank? then
          hora_inicio  = hora.first.fecha_ejecucion
					hora_termino = hora.last.fecha_ejecucion
					as = TimeDifference.between(hora_termino , hora_inicio).in_seconds
					total = hora_termino - hora_inicio
					mm, ss = total.divmod(60)
					hh, mm = mm.divmod(60)
					if hh == 0
						hh = "00"
					end
					if mm == 0
						mm = "00"
					end
				else
					ss = 00
					mm = 00
					hh= 00
          hora_inicio = ""
          hora_termino = ""
				end
				if (hora_inicio == "" ) || (hora_termino == "" )
					lista.push([ruta.porcion_reparto.codigo,empleado.nombre_completo, ruta.codigo,asignado,pendiente, hora_inicio, hora_termino, "0"])
				else
					lista.push([ruta.porcion_reparto.codigo,empleado.nombre_completo, ruta.codigo,asignado,pendiente, hora_inicio.strftime("%H:%M:%S %d/%m/%Y"), hora_termino.strftime("%H:%M:%S %d/%m/%Y"), "%s:%s:%d" % [hh, mm, ss]])
				end
			end
			total = 0, hora_inicio = 0, hora_termino = 0
		end
		#> Ordena Lista por Nombre Ruta
		lista.sort! {|a,b| a[0] <=> b[0]}
		@lista = lista.paginate(:page => params[:page], :per_page => 10)
 		respond_with @lista
	end

end
