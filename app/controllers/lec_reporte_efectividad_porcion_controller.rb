class LecReporteEfectividadPorcionController < ApplicationController
	respond_to :js, :html, :json, :pdf

	def index # > Pagina Principal
		user = current_usuario	# > Busca usuario y zonas y porcion del usuario logueado
    zonas = user.empleado.zona
		@regiones = Region.joins(:zona).where(zonas:{id: zonas}).group(:id)
	end

	# GET
	def carga_porciones 			
		zona = params[:zona].to_i
		mes = params[:mes].to_i
		ano = params[:ano].to_i
		@porciones = Porcion.where(zona_id: zona, mes: mes, ano: ano).order(:codigo)
		respond_with @porciones
	end

	def carga_zonas
    user = current_usuario
    @zonas = user.empleado.zona.where(region_id: params[:region])
  end
	
	# > Funcion encargada de buscar ordenes..
	# > Params[porcion]
	def carga_filtro 			
		lista = []
		@totales = Array.new
	
		#Variables de Totales
		totalAsignado = 0
		totalEfectivo = 0
		totalNoEfectivo = 0
		totalCasaCerrada = 0
		totalVerificaciones = 0
		totalGasNatural = 0
		#Variables Tabla
		asignado = 0
		efectivo = 0
		noEfectivo = 0
		claveEfectivo = 0
		contador = 0
		efectividad = 0
		tipo_lectura = 1
		
		porcion_id = params[:porcion].to_i
		if porcion_id == 0
			porcion = Porcion.where(zona_id: params[:zona], mes: params[:mes].to_i, ano: params[:ano].to_i)
		else
			porcion = Porcion.find(params[:porcion])
		end
		rutas = Rutum.where(porcion_id: porcion, abierto:false, mes:params[:mes].to_i, ano: params[:ano].to_i)

		empleados = Empleado.joins(:usuario, :asignacion_lectura).where(usuarios: {perfil_id: 5}, asignacion_lecturas: {rutum_id: rutas}).group(:id)
		empleados.each do |empleado|
      asignaciones = AsignacionLectura.where(empleado_id: empleado.id).joins(:orden_lectura).where(orden_lecturas:{estado_lectura_id: [1..5], rutum_id: rutas}).group(:rutum_id)
      asignaciones.each do |asignacion|
				ordenes = AsignacionLectura.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura).where(orden_lecturas:{verificacion: false})
				asignado = ordenes.joins(:orden_lectura).where(orden_lecturas: {estado_lectura_id: [2..5]}).count
        efectivo =  ordenes.joins(:orden_lectura => [:clave_lectura]).where(clave_lecturas:{ efectivo: true},orden_lecturas: {estado_lectura_id: [2..5]}).count
        noEfectivo=  ordenes.joins(:orden_lectura => [:clave_lectura]).where(clave_lecturas:{ efectivo: false}, orden_lecturas: {estado_lectura_id: [2..5]}).count
				casaCerrada = ordenes.joins(:orden_lectura).where(orden_lecturas: {clave_lectura_id: 3}).count							
				gasNatural = ordenes.joins(:orden_lectura).where(orden_lecturas: {clave_lectura_id: 18}).count
				verificaciones = ordenes.joins(:orden_lectura).where(orden_lecturas: {tipo_lectura_id: 2}).count
				pendientes = ordenes.joins(:orden_lectura).where(orden_lecturas: {estado_lectura_id: [4..5]}).count
        hora = ordenes.joins(:orden_lectura).where(orden_lecturas: { estado_lectura_id: [4..5]}).order("orden_lecturas.fecha_ejecucion ASC")
        if !hora.blank? then
					horaInicio = hora.first.orden_lectura.fecha_ejecucion.strftime("%H:%M:%S")
					fechaInicio = hora.first.orden_lectura.fecha_ejecucion.strftime("%d/%m/%Y")
					fechaTermino = hora.last.orden_lectura.fecha_ejecucion.strftime("%d/%m/%Y")
          horaTermino = hora.last.orden_lectura.fecha_ejecucion.strftime("%H:%M:%S")
        else
          horaInicio = " "
          horaTermino = " "
				end
				lista.push([asignacion.rutum.porcion.codigo,asignacion.rutum.codigo,empleado.nombre_completo,fechaInicio,horaInicio,fechaTermino, horaTermino, asignado,efectivo,((efectivo*100.0)/asignado).round(2), noEfectivo, ((noEfectivo*100.0)/asignado).round(2), casaCerrada, ((casaCerrada*100.0)/asignado).round(2), gasNatural,((gasNatural*100.0)/asignado).round(2), verificaciones])
				totalAsignado = totalAsignado + asignado
				totalEfectivo = totalEfectivo + efectivo
				totalCasaCerrada = totalCasaCerrada + casaCerrada
				totalVerificaciones = totalVerificaciones + verificaciones
				totalGasNatural = totalGasNatural + gasNatural
				totalNoEfectivo = totalNoEfectivo + noEfectivo
			end
			@totales = [totalAsignado,totalEfectivo,((totalEfectivo*100.0)/totalAsignado).round(2),totalNoEfectivo,((totalNoEfectivo*100.0)/totalAsignado).round(2), totalCasaCerrada,((totalCasaCerrada*100.0)/totalAsignado).round(2),totalGasNatural,((totalGasNatural*100.0)/totalAsignado).round(2),totalVerificaciones]
	 end
	 # > Ordena Lista por Nombre Ruta
	 @lista = lista.sort! {|a,b| a[1] <=> b[1]}
	 respond_with @lista
	end

	def exportar_csv
		#Declaro Lista y Arreglos
		lista = []
		totales = Array.new
		#Variables de Totales
		totalAsignado = 0
		totalEfectivo = 0
		totalNoEfectivo = 0
		totalCasaCerrada = 0
		totalVerificaciones = 0
		totalGasNatural = 0
		#Variables Tabla
		asignado = 0
		efectivo = 0
		noEfectivo = 0
		claveEfectivo = 0
		contador = 0
		efectividad = 0
		tipo_lectura = 1
		
		porcion_id = params[:porcion].to_i
		if porcion_id == 0
			porcion = Porcion.where(zona_id: params[:zona], mes: params[:mes].to_i, ano: params[:ano].to_i)
		else
			porcion = Porcion.find(params[:porcion])
		end
		rutas = Rutum.where(porcion_id: porcion, abierto:false, mes:params[:mes].to_i, ano: params[:ano].to_i)

		empleados = Empleado.joins(:usuario, :asignacion_lectura).where(usuarios: {perfil_id: 5}, asignacion_lecturas: {rutum_id: rutas}).group(:id)
		empleados.each do |empleado|
      asignaciones = AsignacionLectura.where(empleado_id: empleado.id).joins(:orden_lectura).where(orden_lecturas:{estado_lectura_id: [1..5], rutum_id: rutas}).group(:rutum_id)
      asignaciones.each do |asignacion|
				ordenes = AsignacionLectura.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura).where(orden_lecturas:{verificacion: false})
				asignado = ordenes.joins(:orden_lectura).where(orden_lecturas: {estado_lectura_id: [2..5]}).count
        efectivo =  ordenes.joins(:orden_lectura => [:clave_lectura]).where(clave_lecturas:{ efectivo: true},orden_lecturas: {estado_lectura_id: [2..5]}).count
        noEfectivo=  ordenes.joins(:orden_lectura => [:clave_lectura]).where(clave_lecturas:{ efectivo: false}, orden_lecturas: {estado_lectura_id: [2..5]}).count
				casaCerrada = ordenes.joins(:orden_lectura).where(orden_lecturas: {clave_lectura_id: 3}).count							
				gasNatural = ordenes.joins(:orden_lectura).where(orden_lecturas: {clave_lectura_id: 18}).count
				verificaciones = ordenes.joins(:orden_lectura).where(orden_lecturas: {tipo_lectura_id: 2}).count
				pendientes = ordenes.joins(:orden_lectura).where(orden_lecturas: {estado_lectura_id: [4..5]}).count
        hora = ordenes.joins(:orden_lectura).where(orden_lecturas: { estado_lectura_id: [4..5]}).order("orden_lecturas.fecha_ejecucion ASC")
        if !hora.blank? then
					horaInicio = hora.first.orden_lectura.fecha_ejecucion.strftime("%H:%M:%S")
					fechaInicio = hora.first.orden_lectura.fecha_ejecucion.strftime("%d/%m/%Y")
					fechaTermino = hora.last.orden_lectura.fecha_ejecucion.strftime("%d/%m/%Y")
          horaTermino = hora.last.orden_lectura.fecha_ejecucion.strftime("%H:%M:%S")
        else
          horaInicio = " "
          horaTermino = " "
				end
				lista.push([asignacion.rutum.porcion.codigo,asignacion.rutum.codigo,empleado.nombre_completo,fechaInicio,horaInicio,fechaTermino, horaTermino, asignado,efectivo,((efectivo*100.0)/asignado).round(2), noEfectivo, ((noEfectivo*100.0)/asignado).round(2), casaCerrada, ((casaCerrada*100.0)/asignado).round(2), gasNatural,((gasNatural*100.0)/asignado).round(2), verificaciones])
				totalAsignado = totalAsignado + asignado
				totalEfectivo = totalEfectivo + efectivo
				totalCasaCerrada = totalCasaCerrada + casaCerrada
				totalVerificaciones = totalVerificaciones + verificaciones
				totalGasNatural = totalGasNatural + gasNatural
				totalNoEfectivo = totalNoEfectivo + noEfectivo
			end
			totales = [totalAsignado,totalEfectivo,((totalEfectivo*100.0)/totalAsignado).round(2),totalNoEfectivo,((totalNoEfectivo*100.0)/totalAsignado).round(2), totalCasaCerrada,((totalCasaCerrada*100.0)/totalAsignado).round(2),totalGasNatural,((totalGasNatural*100.0)/totalAsignado).round(2),totalVerificaciones]
	 end
	 # > Ordena Lista por Nombre Ruta
	 lista.sort! {|a,b| a[1] <=> b[1]}
	 header = "Porcion,Ruta,Nombre Operador,Fecha Inicio, Hora Inicio,Fecha Termino, Hora Termino,Total Asignacion,Efectivo,%Efectivo,No efectivo, %Efectivo,Casa Cerrada, %Casa Cerrada, Gas Natural, %Gas Natural, Verificaciones \n"
	 fecha = Time.now.strftime("%e%m%Y_ %H%M%S")
	 file = "efectividad_porcion_" +  fecha.to_s + ".csv"
	 stringTotales = ",,,,,,Totales:,"
	 File.open(file, "w+:UTF-16LE:UTF-8") do |csv|
		 csv << header
		 lista.each do |l|
			 csv << l[0].to_s + "," +
							l[1].to_s + "," +
							l[2].to_s + ","  +
							l[3].to_s + ","  +
							l[4].to_s + "," +
							l[5].to_s + "," +
							l[6].to_s + "," +
							l[7].to_s + "," +
							l[8].to_s + "," +
							l[9].to_s + "," +
							l[10].to_s + "," +
							l[11].to_s + "," +
							l[12].to_s + "," +
							l[13].to_s + "," +
							l[14].to_s + "," +
							l[15].to_s + "," +
							l[16].to_s + "," + "\n"
		 end
		 csv <<  stringTotales
		 csv << 	totales[0].to_s + "," +
							totales[1].to_s + "," +
							totales[2].to_s + "," +
							totales[3].to_s + "," +
							totales[4].to_s + "," +
							totales[5].to_s + "," +
							totales[6].to_s + "," +
							totales[7].to_s + "," +
							totales[8].to_s + "," +
							totales[9].to_s + "," + "\n"		 	
	 end
	 send_file(file, x_sendfile: true, buffer_size: 512, disposition: 'attachment')
	end

end
