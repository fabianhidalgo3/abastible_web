class ConAsignacionOrdenesController < ApplicationController
  respond_to :js, :html, :json

  def index
    user = current_usuario
    zonas = user.empleado.zona
    @regiones = Region.joins(:zona).where(zonas:{id: zonas}).group(:id)
    @procesoConvenios = ProcesoConvenio.all
    @estadoConvenios = EstadoConvenio.where(id: [1..2])
  end

  def carga_zonas
    user = current_usuario
    @zonas = user.empleado.zona.where(region_id: params[:region]) if !params[:region].blank?
  end

  # GET
  # PARCIAL: Muestra las porciones
  def carga_porciones
    @porciones = PorcionConvenio.where(zona_id: params[:zona], abierto: true).order(:codigo)
    respond_with @porciones
  end

   # GET
  # Retorna lista de ordenes asignadas o ordenes con asignación
  def carga_rutas
    # Rescato parametros desde la vista y parseo a entero
    zona = params[:zona].to_i
    porcion = params[:porcion].to_i
    estado_orden = params[:estado_convenio].to_i
    proceso_convenio = params[:proceso_convenio].to_i

    # Valido porciones
    (zona == 0) ? porciones = PorcionConvenio.where(abierto: true) : porciones = PorcionConvenio.where(zona_id: zona, abierto:true)
    if porcion != 0
      porciones = PorcionConvenio.find(porcion)
    end
    p porciones
    rutas = RutaConvenio.where(porcion_convenio_id: porciones)
    p rutas
    zona = Zona.find(zona) if zona != 0
    p zona
    # Creo Lista Vacia para devolver las ordenes
    @lista = []
    @empleados = zona.empleado.joins(:usuario).where(usuarios: {perfil_id: 5})
    # Ordenes Sin Asignación
    if estado_orden == 1 && zona != 0
      @variable = 0
      # Creo lista vacia para las guardar las ordenes
      # Busco todas las rutas sin asignar y que esten abiertas
      rutasSinAsignar = RutaConvenio.where(porcion_convenio_id: porciones)
      # Recorro todas las rutas sin asignar
      rutasSinAsignar.each do |ruta|
        ordenes = OrdenConvenio.where(ruta_convenio_id: ruta.id, estado_convenio_id: estado_orden, proceso_convenio_id: proceso_convenio)
        next unless ordenes.count > 0
        row = []

        row.push(ruta.porcion_convenio.codigo)
        row.push(ruta.codigo)
        row.push(ordenes.count)
        row.push(ruta.id)
        @lista.push(row)
      end
    end

    #Ordenes con Asignación
    if estado_orden.to_i == 2 && zona != 0
    @variable = 1
     @empleados.each do |empleado|
     	#Busco Asignaciones por empleado
        asignaciones = AsignacionConvenio.where(empleado_id: empleado.id).joins(:orden_convenio).where(orden_convenios:{ruta_convenio_id: rutas, estado_convenio_id: [2,3]}).group(:ruta_convenio_id)
        if !asignaciones.nil?
          asignaciones.each do |asignacion|
            # Total de asignaciones por empleado
            asignaciones = AsignacionConvenio.where(empleado_id: asignacion.empleado_id, ruta_convenio_id: asignacion.ruta_convenio_id).joins(:orden_convenio).where(orden_convenios: {estado_convenio_id: [2,3]}).count 
            # Creo lista
            row = []
            row.push(asignacion.ruta_convenio.porcion_convenio.codigo)
            row.push(asignacion.orden_convenio.ruta_convenio.codigo)
            row.push(empleado.nombre_completo)
            row.push(empleado.id)
            row.push(asignacion.orden_convenio.ruta_convenio.id)
            row.push("")
            row.push(asignaciones)
            @lista.push(row)
          end
        end
      end
    end
    @lista.sort! {|a,b| a[1] <=> b[1]}
    respond_with @lista
  end

  # GET
  # Actualiza un lote prefinido de convenios
  def asignacion_completa
    #Params desde la vista
    procesoConvenio = params[:proceso_convenio]
    rutaConvenio = RutaConvenio.find(params[:ruta])
    ordenes = OrdenConvenio.where(ruta_convenio_id: ruta.id,  estado_convenio_id: 1)
    @totalOrdenes = ordenes.count
    ordenes.each do |orden|
      AsignacionConvenio.create(orden_convenio_id: orden.id, empleado_id: params[:empleado], ruta_convenio_id: ruta.id)
      orden.update(estado_convenio_id: 2, fecha_asignacion: Time.zone.now)
    end
    respond_with @totalOrdenes
  end

  # GET
  # Dividir Rutas para asignacion
  def dividir_ruta_asignacion
    @ruta_seleccionada = RutaConvenio.find(params[:ruta])
    @lector = Empleado.find(params[:empleado])
    @ordenes_convenio = OrdenConvenio.where(ruta_convenio_id: params[:ruta], estado_convenio_id: 1).order(:secuencia)
  end

  # GET
  # Guarda las Asignaciónes Parciales
  def asignacion_parcial
    @completado = params[:id_boton_asignacion].to_i
    ruta = RutaConvenio.find(params[:ruta])
    ordenes = params[:ordenes].split(',')
    ordenes.each do |orden_id|
      orden = OrdenConvenio.find(orden_id)
      AsignacionConvenio.new(ruta_convenio_id: ruta.id, orden_convenio_id: orden.id, empleado_id: params[:empleado].to_i).save
      orden.update(estado_convenio_id:  2, fecha_asignacion: Time.zone.now)
      orden.save
    end
    @lector_id = Empleado.where(params[:empleado]).id
    @ruta_seleccionada_id = ruta.id
    respond_with @completado
  end

  # GET
  # Elimina la ruta por comuna o solo comuna..
  def desasignacion_completa
    ruta = RutaConvenio.find(params[:ruta])
    ordenes = OrdenConvenio.where(ruta_convenio_id: ruta.id, estado_convenio_id: [2,3]).joins(:asignacion_convenio).where(asignacion_convenios: {empleado_id: params[:empleado]})
    @totalOrdenes = ordenes.count
    ordenes.each do |o|
      o.asignacion_convenio.destroy
      o.update(estado_convenio_id: 1, fecha_asignacion: '')
    end
    respond_with @totalOrdenes
  end

  # GET
  # Dividir Rutas para Desasignacion
  def dividir_ruta_desasignacion
    @ruta_seleccionada = RutaConvenio.find(params[:ruta])
    @lector = Empleado.find(params[:empleado])
    @asignaciones = AsignacionConvenio.where(empleado_id: @lector.id, ruta_convenio_id: @ruta_seleccionada.id).joins(:orden_convenio).where(orden_convenios: {estado_convenio_id: [2,3]})
  end

  # GET
  # Elimina las asignaciones parciales
  def desasignacion_parcial
    @completado_uno = params[:id_boton_desasignacion].to_i
    ruta = RutaConvenio.find(params[:ruta])
    empleado = params[:empleado]
    ordenes = params[:ordenes].split(',')
    ordenes.each do |orden_id|
      orden = OrdenConvenio.find(orden_id)
      orden.asignacion_convenio.destroy
      orden.update(estado_convenio_id:1, fecha_asignacion: '')
    end
    @lector_id = params[:empleado].to_i
    @ruta_seleccionada_id = ruta.id
    respond_with @completado_uno
  end

end
