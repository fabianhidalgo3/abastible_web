class RepReporteEfectividadPorcionController < ApplicationController
	respond_to :js, :html, :json, :pdf

	def index # > Pagina Principal
		user = current_usuario	# > Busca usuario y zonas y porcion del usuario logueado
    zonas = user.empleado.zona
    @tipo_lectura = TipoLectura.all
		@claves = ClaveLectura.all
		@totales = Array.new
		@regiones = Region.joins(:zona).where(zonas:{id: zonas}).group(:id)
	end

	# => Carga Porciones
	def carga_porciones
		zona = params[:zona].to_i
		mes = params[:mes].to_i
		ano = params[:ano].to_i
		@porciones = PorcionReparto.where(zona_id: zona, mes: mes, ano: ano).order(:codigo)
		respond_with @porciones
	end

	def carga_zonas
    user = current_usuario
    @zonas = user.empleado.zona.where(region_id: params[:region])
  end

	# > Funcion encargada de buscar ordenes..
	# > parametros porcion
	def carga_filtro
		@lista = Array.new
		@totales = Array.new
		t_asignado = 0
		t_visitados = 0
		t_efectividad = 0
		asignado = 0
		total = 0
		t_leidos = 0
		contador = 0
		efectividad = 0
		mes = params[:mes].to_i
		ano = params[:ano].to_i
		porcion_id = params[:porcion].to_i
		if porcion_id == 0
			porcion = PorcionReparto.where(abierto: true, zona_id: params[:zona],mes: mes, ano: ano)
		else
			porcion = PorcionReparto.find(params[:porcion])
		end

		rutas = RutaReparto.where(porcion_reparto_id: porcion, abierto:true)
			listaAsignaciones = AsignacionReparto.joins(:ruta_reparto, :orden_reparto).where(ruta_repartos:{ id: rutas }, orden_repartos:{estado_reparto_id: [2..5]}).group(:ruta_reparto_id)
			p listaAsignaciones
		 if !listaAsignaciones.blank?
			 listaAsignaciones.each do |asignacion|
					ruta = asignacion.ruta_reparto
				 	asignado = AsignacionReparto.where(ruta_reparto_id: asignacion.ruta_reparto_id).joins(:orden_reparto).where(orden_repartos:{ estado_reparto_id:[2..5]}).count
					visitados = AsignacionReparto.where(ruta_reparto_id: asignacion.ruta_reparto_id).joins(:orden_reparto).where(orden_repartos:{estado_reparto_id: [4..5]}).count
					totalLecturas = AsignacionReparto.where(ruta_reparto_id: asignacion.ruta_reparto_id).joins(:orden_reparto).where(orden_repartos: {estado_reparto_id: [2..5]}).count
					efectividad = ((asignado*100.0)/visitados).round(2)
					if efectividad.infinite?
						efectividad = 0
					end
					@lista.push([ruta.porcion_reparto.codigo,ruta.codigo, asignado, visitados, efectividad])
					t_asignado = t_asignado + asignado
					t_visitados = t_visitados + visitados
					t_efectividad = ((t_visitados * 100.0)/t_asignado).round(2)
			 end
		end
		if t_efectividad.infinite?
			t_efectividad = 0
		end
	  @totales.push(t_asignado, t_visitados, t_efectividad)
	  # > Ordena Lista por Nombre Ruta
	  @lista.sort! {|a,b| a[1] <=> b[1]}
	  respond_with @lista
	end

end
