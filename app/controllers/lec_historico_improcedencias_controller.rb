class LecHistoricoImprocedenciasController < ApplicationController
	respond_to :js, :html, :json
	def index # > Pagina Principal
		user = current_usuario	# > Busca usuario y zonas y porcion del usuario logueado
    zonas = user.empleado.zona
		@claves = ClaveLectura.where(efectivo:false)
		@regiones = Region.joins(:zona).where(zonas:{id: zonas}).group(:id)
  end
  
  def carga_filtro 
    zonal = params[:zona].to_i
    mes = params[:mes].to_i
    ano = params[:ano].to_i
    clave_lectura = params[:clave].to_i
    if clave_lectura != 0
      @ordenes = OrdenLectura.where(clave_lectura_id: clave_lectura,verificacion:false).joins(:rutum => [:porcion]).where(porcions:{zona_id: zonal, mes:mes ,ano:ano}).order(:secuencia_lector)
    end
    if clave_lectura == 0 
      @ordenes = OrdenLectura.where(verificacion:false).joins(:clave_lectura,:rutum => [:porcion]).where(porcions:{zona_id: zonal, mes:mes, ano:ano}, clave_lecturas:{efectivo:false}).order(:secuencia_lector)
    end
    respond_with @ordenes
  end

  def exportar_csv
    zonal = params[:zona].to_i
    mes = params[:mes].to_i
    ano = params[:ano].to_i
    clave_lectura = params[:clave].to_i
    if clave_lectura != 0
      ordenes = OrdenLectura.where(clave_lectura_id: clave_lectura,verificacion:false).joins(:rutum => [:porcion]).where(porcions:{zona_id: zonal, mes:mes ,ano:ano}).order(:secuencia_lector)
    end
    if clave_lectura == 0 
      ordenes = OrdenLectura.where(verificacion:false).joins(:clave_lectura,:rutum => [:porcion]).where(porcions:{zona_id: zonal, mes:mes, ano:ano}, clave_lecturas:{efectivo:false}).order(:secuencia_lector)
    end
  
    header = "Porcion,Ruta, Secuencia, Cliente, Medidor,Direccion,Comuna,Clave Lectura, Nveces Improcedente \n"
		fecha = Time.now.strftime("%e%m%Y_ %H%M%S")
    file = "historico improcedencias_" +  fecha.to_s + ".csv"
    secuencia_lector = ""
    rutaCodigo = ""
    porcionCodigo = ""
    File.open(file, "w+:UTF-16LE:UTF-8") do |csv|
      csv << header
      
      ordenes.each do |l|
        cliente = l.cliente
        nveces = cliente.orden_lectura.joins(:clave_lectura).where(clave_lecturas: {efectivo:false}, orden_lecturas:{verificacion: false}).count  
        csv << l.rutum.porcion.codigo.to_s + "," +
               l.rutum.codigo.to_s + "," +
               l.secuencia_lector.to_s + "," +
               l.cliente.numero_cliente.to_s + "," +
               l.medidor.numero_medidor.to_s + ","+
               l.direccion + "," +
               l.comuna.nombre + "," +
               l.clave_lectura.nombre + "," +
               nveces.to_s + "\n"
      end
    end
    send_file(file, x_sendfile: true, buffer_size: 512, disposition: 'attachment')
  end

end
