class LecReporteTiposClavesController < ApplicationController
	
	def index # > Pagina Principal
		user = current_usuario	# > Busca usuario y zonas y porcion del usuario logueado
    zonas = user.empleado.zona
    @tipo_lectura = TipoLectura.all
		@clave_lecturas = ClaveLectura.all
		@totales = Array.new
		@regiones = Region.joins(:zona).where(zonas:{id: zonas}).group(:id)
	end

	respond_to :js, :html, :json
	def carga_porciones 	# => Carga Porciones		
		zona = params[:zona].to_i
		@porciones = Porcion.where(zona_id: zona)
		respond_with @porciones
	end

	# GET
	# Mostrar Resultados Por Ruta.
	def carga_filtro
		totales = []
		@lista = Array.new
		zona_id = params[:zona].to_i
		porcion_id = params[:porcion].to_i
		mes = params[:mes]
		ano = params[:ano]
	
		if porcion_id == 0 && zona_id != 0
			rutas = Rutum.joins(:porcion).where(porcions:{zona_id: zona_id, mes: mes, ano: ano}, ruta:{abierto:false})
		else
			rutas = Rutum.where(porcion_id: porcion_id, abierto:false)
		end
		totalRutas = 0
		totalGasNatural = 0
		totalNadieEnCasa = 0
		totalClienteNoPermite = 0
		totalOtrasClaves = 0
		totalImprocedencias = 0
		totalEfectivo = 0
		rutas.count
		rutas.each do |ruta|
			ordenes = OrdenLectura.where(rutum_id: ruta.id,verificacion: false, estado_lectura_id: [4..5])
			efectivo = ordenes.joins(:clave_lectura).where(clave_lecturas:{efectivo:true}).count
			improcedencias = ordenes.joins(:clave_lectura).where(clave_lecturas:{efectivo:false}).count							
			nadieEnCasa = ordenes.where(clave_lectura_id: 3).count							
			gasNatural = ordenes.where(clave_lectura_id: 18).count
			clienteNoPermite = ordenes.where(clave_lectura_id: 21).count
			otrasClaves = ordenes.where.not(clave_lectura_id: [1,3,18,21]).count
			totalRuta = ordenes.count
			@lista.push([ruta.porcion.codigo,ruta.codigo,totalRuta,efectivo,((efectivo*100)/totalRuta),improcedencias,((improcedencias*100.0)/totalRuta).round(2),nadieEnCasa,((nadieEnCasa*100.0)/totalRuta).round(2),gasNatural,((gasNatural*100.0)/totalRuta).round(2), otrasClaves, ((otrasClaves*100.0)/totalRuta).round(2)])
			totalRutas = totalRutas + totalRuta
			totalGasNatural = totalGasNatural + gasNatural
			totalNadieEnCasa = totalNadieEnCasa + nadieEnCasa
			totalClienteNoPermite = totalClienteNoPermite + clienteNoPermite
			totalOtrasClaves = totalOtrasClaves + otrasClaves	
			totalImprocedencias = totalImprocedencias + improcedencias	
			totalEfectivo = totalEfectivo + efectivo	
		end
		totales = [totalRutas, totalEfectivo, ((totalEfectivo*100.0)/totalRutas).round(2),totalImprocedencias,((totalImprocedencias*100.0)/totalRutas).round(2), totalNadieEnCasa,((totalNadieEnCasa*100.0)/totalRutas).round(2), totalGasNatural, ((totalGasNatural*100.0)/totalRutas).round(2), totalOtrasClaves, ((totalOtrasClaves*100.0)/totalRutas).round(2)]
		@totales = totales
		@lista = @lista.sort! {|a,b| a[1] <=> b[1]}
		respond_with @lista
	end
	
	def exportar_csv
		totales = []
		lista = Array.new
		zona_id = params[:zona].to_i
		porcion_id = params[:porcion].to_i
		mes = params[:mes]
		ano = params[:ano]
	
		if porcion_id == 0 && zona_id != 0
			rutas = Rutum.joins(:porcion).where(porcions:{zona_id: zona_id, mes: mes, ano: ano}, ruta:{abierto:false})
		else
			rutas = Rutum.where(porcion_id: porcion_id, abierto:false)
		end
		totalRutas = 0
		totalGasNatural = 0
		totalNadieEnCasa = 0
		totalClienteNoPermite = 0
		totalOtrasClaves = 0
		rutas.count
		rutas.each do |ruta|
			ordenes = OrdenLectura.where(rutum_id: ruta.id,verificacion: false, estado_lectura_id: [4..5])
			nadieEnCasa = ordenes.where(clave_lectura_id: 3).count							
			gasNatural = ordenes.where(clave_lectura_id: 18).count
			clienteNoPermite = ordenes.where(clave_lectura_id: 21).count
			otrasClaves = ordenes.where.not(clave_lectura_id: [1,3,18,21]).count
			totalRuta = ordenes.count
			lista.push([ruta.porcion.codigo,ruta.codigo,totalRuta,nadieEnCasa,((nadieEnCasa*100.0)/totalRuta).round(2),gasNatural,((gasNatural*100.0)/totalRuta).round(2),clienteNoPermite,((clienteNoPermite*100.0)/totalRuta).round(2), otrasClaves, ((otrasClaves*100.0)/totalRuta).round(2)])
			totalRutas = totalRutas + totalRuta
			totalGasNatural = totalGasNatural + gasNatural
			totalNadieEnCasa = totalNadieEnCasa + nadieEnCasa
			totalClienteNoPermite = totalClienteNoPermite + clienteNoPermite
			totalOtrasClaves = totalOtrasClaves + otrasClaves			
		end
		totales = [totalRutas, totalNadieEnCasa,((totalNadieEnCasa*100.0)/totalRutas).round(2), totalGasNatural, ((totalGasNatural*100.0)/totalRutas).round(2),totalClienteNoPermite, ((totalClienteNoPermite*100.0)/totalRutas).round(2), totalOtrasClaves, ((totalOtrasClaves*100.0)/totalRutas).round(2)]	
		# > Ordena Lista por Nombre Ruta
		lista.sort! {|a,b| a[1] <=> b[1]}
		header = "Porcion,Ruta,Total Ruta, Nadie en Casa, % Nadie en Casa,Gas Natural, % Gas Natural,Cliente no Permite, % Cliente no Permite, Otras Claves, % Otras Claves \n"
		fecha = Time.now.strftime("%e%m%Y_ %H%M%S")
		file = "reporte_tipos_claves_" +  fecha.to_s + ".csv"
		stringTotales = ",Totales:,"
		File.open(file, "w+:UTF-16LE:UTF-8") do |csv|
			csv << header
			lista.each do |orden_lectura|
				csv << orden_lectura[0].to_s + "," +
							 orden_lectura[1].to_s + "," +
							 orden_lectura[2].to_s + ","  +
							 orden_lectura[3].to_s + ","  +
							 orden_lectura[4].to_s + "%," +
							 orden_lectura[5].to_s + "," +
							 orden_lectura[6].to_s + "%," +
							 orden_lectura[7].to_s + "," +
							 orden_lectura[8].to_s + "%," +
							 orden_lectura[9].to_s + "," +
							 orden_lectura[10].to_s + "%," + "\n"
			end
			csv <<  stringTotales
			csv << 	totales[0].to_s + "," +
							totales[1].to_s + "," +
							totales[2].to_s + "%," +
							totales[3].to_s + "," +
							totales[4].to_s + "%," +
							totales[5].to_s + "," +
							totales[6].to_s + "%," +
							totales[7].to_s + "," +
							totales[8].to_s + "%," +
							totales[9].to_s + "," +
							totales[10].to_s + "%," + "\n"		 	
		send_file(file, x_sendfile: true, buffer_size: 512, disposition: 'attachment')
		end
	end
end
