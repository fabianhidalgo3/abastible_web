class LecReporteEfectividadLectorController < ApplicationController
	respond_to :js, :html, :json, :pdf

	def index # > Pagina Principal
		user = current_usuario	# > Busca usuario y zonas y porcion del usuario logueado
    zonas = user.empleado.zona
		@regiones = Region.joins(:zona).where(zonas:{id: zonas}).group(:id)
	end

	# GET
	def carga_porciones
		zona = params[:zona].to_i
		mes = params[:mes].to_i
		ano = params[:ano].to_i
		@porciones = Porcion.where(zona_id: zona, mes: mes, ano: ano).order(:codigo)
		respond_with @porciones
	end

	def carga_zonas
    user = current_usuario
    @zonas = user.empleado.zona.where(region_id: params[:region])
  end

	def carga_lectores 	# => Carga Porciones
		zona = params[:zona].to_i
		p zona
		@lectores = Empleado.joins(:usuario, :zona).where(usuarios:{perfil_id: [5]}, zonas:{id: zona})
		respond_with @lectores
	end

	def carga_filtro
		@lista = Array.new
		totales = []
		totalAsignado = 0
		totalEfectivo = 0
		totalImprocedente = 0

		asignado = 0
		efectivo = 0
		improcedente = 0

		mes = params[:mes].to_i
		ano = params[:ano].to_i
		zona_id = params[:zona].to_i
		usuario_id = params[:lector].to_i
		porciones = Porcion.where(zona_id: params[:zona].to_i, abierto: false, mes: mes, ano: ano)
		rutas = Rutum.where(porcion_id: porciones)
		if zona_id != 0  && usuario_id == 0
			listaEmpleados = Empleado.joins(:usuario, :asignacion_lectura).where(usuarios: {perfil_id: 5}, asignacion_lecturas: {rutum_id: rutas}).group(:id)
		end

		if zona_id != 0  && usuario_id != 0
			listaEmpleados = Empleado.where(id: usuario_id)
		end
	 	listaEmpleados.each do |empleado|
			listaAsignaciones = AsignacionLectura.where(empleado_id: empleado.id).joins(:rutum,:orden_lectura).where(ruta:{id: rutas},orden_lecturas:{estado_lectura_id: [2..5]}).group(:rutum_id)
		 if !listaAsignaciones.blank?
				listaAsignaciones.each do |asignacion|
					asignado = AsignacionLectura.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura).count
				 	efectivo = AsignacionLectura.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5]}, clave_lecturas: {efectivo: true}).count
				 	improcedente = AsignacionLectura.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura =>[:clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5]}, clave_lecturas: {efectivo: false}).count
				 	efectividad = ((efectivo*100.0)/asignado).round(2)
				 	@lista.push([asignacion.rutum.porcion.codigo, asignacion.empleado.nombre_completo,asignacion.rutum.codigo,asignado, efectivo, improcedente, efectividad])
					totalAsignado = totalAsignado + asignado
					totalEfectivo = totalEfectivo + efectivo
					totalImprocedente = totalImprocedente + improcedente
				end
				totales = [totalAsignado, totalEfectivo, totalImprocedente,((totalEfectivo*100.0)/totalAsignado).round(2)]
		 end
	 end
	 @totales = totales
	 @lista.sort! {|a,b| a[2] <=> b[2]}
	 # > Ordena Lista por Nombre Ruta
	 respond_with @lista
	end

	def exportar_csv
		lista = Array.new
		totales = []
		totalAsignado = 0
		totalEfectivo = 0
		totalImprocedente = 0

		asignado = 0
		efectivo = 0
		improcedente = 0

		mes = params[:mes].to_i
		ano = params[:ano].to_i
		zona_id = params[:zona].to_i
		usuario_id = params[:lector].to_i
		porciones = Porcion.where(zona_id: params[:zona].to_i, abierto: false, mes: mes, ano: ano)
		rutas = Rutum.where(porcion_id: porciones)
		if zona_id != 0  && usuario_id == 0
			listaEmpleados = Empleado.joins(:usuario, :asignacion_lectura).where(usuarios: {perfil_id: 5}, asignacion_lecturas: {rutum_id: rutas}).group(:id)
		end

		if zona_id != 0  && usuario_id != 0
			listaEmpleados = Empleado.where(id: usuario_id)
		end
	 	listaEmpleados.each do |empleado|
			listaAsignaciones = AsignacionLectura.where(empleado_id: empleado.id).joins(:rutum,:orden_lectura).where(ruta:{id: rutas},orden_lecturas:{estado_lectura_id: [2..5]}).group(:rutum_id)
		 if !listaAsignaciones.blank?
				listaAsignaciones.each do |asignacion|
					asignado = AsignacionLectura.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura).count
				 	efectivo = AsignacionLectura.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5]}, clave_lecturas: {efectivo: true}).count
				 	improcedente = AsignacionLectura.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura =>[:clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5]}, clave_lecturas: {efectivo: false}).count
				 	efectividad = ((efectivo*100.0)/asignado).round(2)
				 	lista.push([asignacion.rutum.porcion.codigo, asignacion.empleado.nombre_completo,asignacion.rutum.codigo,asignado, efectivo, improcedente, efectividad])
					totalAsignado = totalAsignado + asignado
					totalEfectivo = totalEfectivo + efectivo
					totalImprocedente = totalImprocedente + improcedente
				end
				totales = [totalAsignado, totalEfectivo, totalImprocedente,((totalEfectivo*100.0)/totalAsignado).round(2)]
		 end
	 end

		header = "Porcion,Operador de Medidor,Ruta,Total asignado, Total efectivo,Total claves,%Efectividad \n"
		fecha = Time.now.strftime("%e%m%Y_ %H%M%S")
		file = "efectividad_lector_" +  fecha.to_s + ".csv"
		stringTotales = ",,Totales:,"
		File.open(file, "w+:UTF-16LE:UTF-8") do |csv|
			csv << header
			lista.each do |l|
				csv << l[0].to_s + "," +
							 l[1].to_s + "," +
							 l[2].to_s + ","  +
							 l[3].to_s + ","  +
							 l[4].to_s + "," +
							 l[5].to_s + "," +
							 l[6].to_s + "%," + "\n"
			end
			csv <<  stringTotales
			csv << 	totales[0].to_s + "," +
							 totales[1].to_s + "," +
							 totales[2].to_s + "," +
							 totales[3].to_s + "%," + "\n"
		end
		send_file(file, x_sendfile: true, buffer_size: 512, disposition: 'attachment')
	end


end
