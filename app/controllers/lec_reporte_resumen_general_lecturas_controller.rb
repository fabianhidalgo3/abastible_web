class LecReporteResumenGeneralLecturasController < ApplicationController
  respond_to :js, :html, :json, :pdf

	def index # > Pagina Principal
		user = current_usuario	# > Busca usuario y zonas y porcion del usuario logueado
    zonas = user.empleado.zona
    @tipo_lectura = TipoLectura.all
		@claves = ClaveLectura.all
		@totales = Array.new
		@regiones = Region.joins(:zona).where(zonas:{id: zonas}).group(:id)
	end

	def carga_zonas
    user = current_usuario
    @zonas = user.empleado.zona.where(region_id: params[:region])
  end

	def carga_lectores 	# => Carga Porciones
		zona = params[:zona].to_i
		@lectores = Empleado.joins(:usuario, :zona).where(usuarios:{perfil_id: [5]}, zonas:{id: zona})
		respond_with @lectores
	end
	# > Funcion encargada de buscar ordenes..
	# > Params[porcion]
	def carga_filtro
		zona_id = params[:zona].to_i
		ano = params[:ano].to_i
		lector = params[:lector].to_i
		p lector
		if lector != 0
			empleado = Empleado.where(usuario_id: lector).first
		end
		# Zonas a Recorrer
		porciones = Porcion.where(zona_id: zona_id, ano: ano).group(:mes)
		efectividadPorcion = Array.new()
		porciones.each do |porcion|
			rutas = Rutum.where(porcion_id: porciones, mes: porcion.mes, ano: ano)
			p rutas
			p empleado
			if !empleado.nil?
				leidos =  OrdenLectura.where(rutum_id: rutas, verificacion: false, estado_lectura_id: [4..5]).joins(:asignacion_lectura).where(asignacion_lecturas: {empleado_id: empleado.id}).count
				efectivo =  OrdenLectura.where(rutum_id: rutas, verificacion: false, estado_lectura_id:[4..5]).joins(:clave_lectura, :asignacion_lectura).where(clave_lecturas: {efectivo:true}, asignacion_lecturas: {empleado_id: empleado.id}).count
			else
				leidos =  OrdenLectura.where(rutum_id: rutas, verificacion: false, estado_lectura_id: [4..5]).count
				efectivo =  OrdenLectura.where(rutum_id: rutas, verificacion: false, estado_lectura_id:[4..5]).joins(:clave_lectura).where(clave_lecturas: {efectivo:true}).count
			end
			p leidos
			p efectivo
			if porcion.mes == 1
				mes = "Enero"
			elsif porcion.mes == 2
				mes = "Febrero"
			elsif porcion.mes == 3
				mes = "Marzo"
			elsif porcion.mes == 4
				mes = "Abril"
			elsif porcion.mes == 5
				mes = "Mayo"
			elsif porcion.mes == 6
				mes = "Junio"
			elsif porcion.mes == 7
				mes = "Julio"
			elsif porcion.mes == 8
				mes = "Agosto"
			elsif porcion.mes == 9
				mes = "Septiembre"
			elsif porcion.mes == 10
				mes = "Octubre"
			elsif porcion.mes == 11
				mes = "Noviembre"
			else
				mes = "Diciembre"
			end
			efectividadPorcion.push([mes,leidos, efectivo, ((efectivo*100.0)/leidos).round(2)])
		end
		@efectividadPorcion = efectividadPorcion
	end

end
