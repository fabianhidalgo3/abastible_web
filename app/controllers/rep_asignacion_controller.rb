class RepAsignacionController < ApplicationController
  respond_to :js, :html, :json

  def index
    user = current_usuario
    zonas = user.empleado.zona
    		@regiones = Region.joins(:zona).where(zonas: {id: zonas}).group(:id).order(:id).order(:id)
.order(:id)
    @estadoReparto = EstadoReparto.where(id: [1,2])
  end

  # GET
  # PARCIAL: Muestra Zonas
  # params[:region] > Integer
  def carga_zonas
    user = current_usuario
    @zonas = user.empleado.zona.where(region_id: params[:region]) if !params[:region].blank?
  end

  # GET
  # PARCIAL: Muestra las porciones
  def carga_porciones
    @porciones = PorcionReparto.where(zona_id: params[:zona], abierto: true).order(:codigo)
    respond_with @porciones
  end

  # GET
  # Retorna lista de ordenes asignadas o ordenes con asignación
  def carga_rutas
    # Rescato parametros desde la vista y parseo a entero
    zona = params[:zona].to_i
    porcion = params[:porcion].to_i
    estado_orden = params[:estado_reparto].to_i

    # Valido porciones
    (zona == 0) ? porciones = PorcionReparto.where(abierto: true) : porciones = PorcionReparto.where(zona_id: zona, abierto:true)
    if porcion != 0
      porciones = PorcionReparto.find(porcion)
    end
    rutas = RutaReparto.where(porcion_reparto_id: porciones)
    zona = Zona.find(zona) if zona != 0
    # Creo Lista Vacia para devolver las ordenes
    @lista = []
    @empleados = zona.empleado.joins(:usuario).where(usuarios: {perfil_id: 5})
    # Ordenes Sin Asignación
    if estado_orden == 1 && zona != 0
      @variable = 0
      # Creo lista vacia para las guardar las ordenes
      # Busco todas las rutas sin asignar y que esten abiertas
      rutasSinAsignar = RutaReparto.where(porcion_reparto_id: porciones)
      # Recorro todas las rutas sin asignar
      rutasSinAsignar.each do |ruta|
        ordenes = OrdenReparto.where(ruta_reparto_id: ruta.id, estado_reparto_id: estado_orden)
        next unless ordenes.count > 0
        row = []
        row.push(ruta.porcion_reparto.codigo)
        row.push(ruta.codigo)
        row.push(ordenes.count)
        row.push(ruta.id)
        @lista.push(row)
      end
    end

    # Ordenes con Asignación
    if estado_orden.to_i == 2 && zona != 0
    @variable = 1
     @empleados.each do |empleado|
     	#Busco Asignaciones por empleado
        asignaciones = AsignacionReparto.where(empleado_id: empleado.id).joins(:orden_reparto).where(orden_repartos:{ruta_reparto_id: rutas, estado_reparto_id: [2,3]}).group(:ruta_reparto_id)
        if !asignaciones.nil?
          asignaciones.each do |asignacion|
            # Total de asignaciones por empleado
            asignaciones = AsignacionReparto.where(empleado_id: asignacion.empleado_id, ruta_reparto_id: asignacion.ruta_reparto_id).joins(:orden_reparto).where(orden_repartos: {estado_reparto_id: [2,3]}).count 
            # Creo lista
            row = []
            row.push(asignacion.ruta_reparto.porcion_reparto.codigo)
            row.push(asignacion.orden_reparto.ruta_reparto.codigo)
            row.push(empleado.nombre_completo)
            row.push(empleado.id)
            row.push(asignacion.orden_reparto.ruta_reparto.id)
            row.push("")
            row.push(asignaciones)
            @lista.push(row)
          end
        end
      end
    end
    @lista.sort! {|a,b| a[1] <=> b[1]}
    respond_with @lista
  end

  # GET
  # Actualiza un lote prefinido de repartos
  def asignacion_completa
    ruta = RutaReparto.find(params[:ruta])
    ordenes = OrdenReparto.where(ruta_reparto_id: ruta.id,  estado_reparto_id: 1)
    @totalOrdenes = ordenes.count
    ordenes.each do |orden|
      AsignacionReparto.create(orden_reparto_id: orden.id, empleado_id: params[:empleado], ruta_reparto_id: ruta.id)
      orden.update(estado_reparto_id: 2, fecha_asignacion: Time.zone.now)
    end
    respond_with @totalOrdenes
  end

  # GET
  # Dividir Rutas para asignacion
  def dividir_ruta_asignacion
    @ruta_seleccionada = RutaReparto.find(params[:ruta])
    @lector = Empleado.find(params[:empleado])
    @ordenes_reparto = OrdenReparto.where(ruta_reparto_id: params[:ruta], estado_reparto_id: 1).order(:secuencia_lector)
  end

  # GET
  # Guarda las Asignaciónes Parciales
  def asignacion_parcial
    @completado = params[:id_boton_asignacion].to_i
    ruta = Rutum.where(id: params[:ruta]).first
    ordenes = params[:ordenes].split(',')
    ordenes.each do |orden_id|
      orden = OrdenReparto.where(id: orden_id).first
      AsignacionReparto.new(ruta_reparto_id: ruta.id, orden_reparto_id: orden.id, empleado_id: params[:empleado].to_i).save
      orden.update(estado_reparto_id:  2, fecha_asignacion: Time.zone.now)
      orden.save
    end
    @lector_id = Empleado.where(id: params[:empleado].to_i).first.id
    @ruta_seleccionada_id = ruta.id
    respond_with @completado
  end

  # GET
  # Elimina la ruta por comuna o solo comuna..
  def desasignacion_completa
    ruta = RutaReparto.find(params[:ruta])
    ordenes = OrdenReparto.where(ruta_reparto_id: ruta.id, estado_reparto_id: [2,3]).joins(:asignacion_reparto).where(asignacion_repartos: {empleado_id: params[:empleado]})
    @totalOrdenes = ordenes.count
    ordenes.each do |o|
      o.asignacion_reparto.destroy
      o.update(estado_reparto_id: 1, fecha_asignacion: '')
    end
    respond_with @totalOrdenes
  end

  # GET
  # Dividir Rutas para Desasignacion
  def dividir_ruta_desasignacion
    @ruta_seleccionada = RutaReparto.find(params[:ruta])
    @lector = Empleado.find(params[:empleado])
    @asignaciones = AsignacionReparto.where(empleado_id: @lector.id, ruta_reparto_id: @ruta_seleccionada.id).joins(:orden_reparto).where(orden_repartos: {estado_reparto_id: [2,3]})
  end

  # GET
  # Elimina las asignaciones parciales
  def desasignacion_parcial
    @completado_uno = params[:id_boton_desasignacion].to_i
    ruta = RutaReparto.find(params[:ruta])
    empleado = params[:empleado]
    ordenes = params[:ordenes].split(',')
    ordenes.each do |orden_id|
      orden = OrdenReparto.find(orden_id)
      orden.asignacion_reparto.destroy
      orden.update(estado_reparto_id:1, fecha_asignacion: '')
    end
    @lector_id = params[:empleado].to_i
    @ruta_seleccionada_id = ruta.id
    respond_with @completado_uno
  end

end
