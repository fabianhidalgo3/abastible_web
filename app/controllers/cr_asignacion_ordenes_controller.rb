class CrAsignacionOrdenesController < ApplicationController
  respond_to :js, :html, :json

  def index
    user = current_usuario
    zonas = user.empleado.zona
    		@regiones = Region.joins(:zona).where(zonas: {id: zonas}).group(:id).order(:id).order(:id)

    @tipos_corte_reposicion = TipoCorteReposicion.all
    @estados_corte_reposicion = EstadoCorteReposicion.where(id: [1,2])
    @procesos_corte_reposicion = ProcesoCorteReposicion.all
  end

  def carga_zonas
    user = current_usuario
    @zonas = user.empleado.zona.where(region_id: params[:region]) if !params[:region].blank?
  end

  # GET
  # PARCIAL: Muestra las porciones
  def carga_porciones
    @porciones = PorcionCorteReposicion.where(zona_id: params[:zona], abierto: true).order(:codigo)
    respond_with @porciones
  end

  # GET
  # Retorna lista de ordenes asignadas o ordenes con asignación
  def carga_rutas
    # Rescato parametros desde la vista y parseo a entero
    zona = params[:zona].to_i
    porcion = params[:porcion].to_i
    tipo_corte_reposicion = params[:tipo_corte_reposicion].to_i
    estado_corte_reposicion = params[:estado_corte_reposicion].to_i
    proceso_corte_reposicion = params[:proceso_corte_reposicion].to_i
    # Valido porciones
    (zona == 0) ? porciones = PorcionCorteReposicion.where(abierto: true) : porciones = PorcionCorteReposicion.where(zona_id: zona)
    if porcion != 0
      porciones = PorcionCorteReposicion.find(porcion)
    end
    rutas = RutaCorteReposicion.where(porcion_corte_reposicion_id: porciones)
    p porciones
    zona = Zona.find(zona) if zona != 0
    # Creo Lista Vacia para devolver las ordenes
    @lista = []
    @empleados = zona.empleado.joins(:usuario).where(usuarios: {perfil_id: 5})
    # Ordenes Sin Asignación
    if estado_corte_reposicion == 1 && zona != 0
      @variable = 0
      #Creo lista vacia para las guardar las ordenes
      # Busco todas las rutas sin asignar y que esten abiertas
      rutasSinAsignar = RutaCorteReposicion.where(porcion_corte_reposicion_id: porciones)
      # Recorro todas las rutas sin asignar
      rutasSinAsignar.each do |ruta|
        ordenes = OrdenCorteReposicion.where(ruta_corte_reposicion_id: ruta.id, tipo_corte_reposicion_id: tipo_corte_reposicion, proceso_corte_reposicion_id: proceso_corte_reposicion, estado_corte_reposicion_id: estado_corte_reposicion)
        p ordenes
        next unless ordenes.count > 0
        row = []
        row.push(ruta.porcion_corte_reposicion.codigo)
        row.push(ruta.codigo)
        row.push(ordenes.count)
        row.push(ruta.id)
        @lista.push(row)
      end
    end

    #Ordenes con Asignación
    if estado_corte_reposicion.to_i == 2 && zona != 0
    	@variable = 1
      @empleados.each do |empleado|
        #Si no existe la comuna busca toda la ruta de lo contratio buscara solo las ordenes de esa ruta con la comuna seleccionada
        asignaciones = AsignacionCorteReposicion.where(empleado_id: empleado.id).joins(:orden_corte_reposicion).where(orden_corte_reposicions:{ruta_corte_reposicion_id: rutas, estado_corte_reposicion_id: [2,3], tipo_corte_reposicion_id: tipo_corte_reposicion, proceso_corte_reposicion_id: proceso_corte_reposicion}).group(:ruta_corte_reposicion_id)
        if !asignaciones.nil?
          asignaciones.each do |asignacion|
            #Total de asignaciones por empleado
            asignaciones = AsignacionCorteReposicion.where(empleado_id: asignacion.empleado_id, ruta_corte_reposicion_id: asignacion.ruta_corte_reposicion_id).joins(:orden_corte_reposicion).where(orden_corte_reposicions: {estado_corte_reposicion_id: [2,3],tipo_corte_reposicion_id: tipo_corte_reposicion, proceso_corte_reposicion_id: proceso_corte_reposicion}).count
            #Creo lista
            row = []
            row.push(asignacion.ruta_corte_reposicion.porcion_corte_reposicion.codigo)
            row.push(asignacion.ruta_corte_reposicion.codigo)
            row.push(empleado.nombre_completo)
            row.push(empleado.id)
            row.push(asignacion.ruta_corte_reposicion.id)
            row.push(params[:tipo_corte_reposicion])
            row.push(asignaciones)
            @lista.push(row)
          end
        end
      end
    end
    @lista.sort! {|a,b| a[1] <=> b[1]}
    respond_with @lista
  end


  # GET
  # Actualiza un lote prefinido de lecturas
  def asignacion_completa
    ruta_corte_reposicion = RutaCorteReposicion.find(params[:ruta])
    tipo_corte_reposicion = params[:tipo_corte_reposicion].to_i
    estado_corte_reposicion = params[:estado_corte_reposicion].to_i
    proceso_corte_reposicion = params[:proceso_corte_reposicion].to_i
    ordenes = OrdenCorteReposicion.where(ruta_corte_reposicion_id: ruta_corte_reposicion.id,estado_corte_reposicion_id: estado_corte_reposicion,tipo_corte_reposicion_id: tipo_corte_reposicion,proceso_corte_reposicion_id: proceso_corte_reposicion)
    @totalOrdenes = ordenes.count
    ordenes.each do |orden|
      AsignacionCorteReposicion.create(orden_corte_reposicion_id: orden.id, empleado_id: params[:empleado], ruta_corte_reposicion_id: ruta_corte_reposicion.id)
      orden.update(estado_corte_reposicion_id: 2, fecha_asignacion: Time.zone.now)
      orden.save
    end
    respond_with @totalOrdenes
  end

  # GET
  # Dividir Rutas para asignacion
  def dividir_ruta_asignacion
    ruta_corte_reposicion = RutaCorteReposicion.find(params[:ruta])
    tipo_corte_reposicion = params[:tipo_corte_reposicion].to_i
    estado_corte_reposicion = params[:estado_corte_reposicion].to_i
    proceso_corte_reposicion = params[:proceso_corte_reposicion].to_i
    @ruta_seleccionada = RutaCorteReposicion.find(params[:ruta].to_i) 
    @lector = Empleado.find(params[:empleado].to_i)
    @ordenes_corte_reposicion = OrdenCorteReposicion.where(ruta_corte_reposicion_id: ruta_corte_reposicion.id,estado_corte_reposicion_id: estado_corte_reposicion,tipo_corte_reposicion_id: tipo_corte_reposicion, proceso_corte_reposicion_id: proceso_corte_reposicion).order(:secuencia)
  end

  # GET
  # Guarda las Asignaciónes Parciales
  def asignacion_parcial
    @completado = params[:id_boton_asignacion].to_i
    ruta = RutaCorteReposicion.find(params[:ruta].to_i)
    ordenes = params[:ordenes].split(',')
    ordenes.each do |orden_id|
      orden = OrdenCorteReposicion.find(orden_id)
      asignacionCorteReposicion = AsignacionCorteReposicion.new(ruta_corte_reposicion_id: ruta.id, orden_corte_reposicion_id: orden.id, empleado_id: params[:empleado].to_i)
      asignacionCorteReposicion.save
      orden.update(estado_corte_reposicion_id:  2, fecha_asignacion: Time.zone.now)
      orden.save
    end
    @lector_id = Empleado.find(params[:empleado].to_i).id
    @ruta_seleccionada_id = ruta.id
    @tipo_corte_reposicion_id = OrdenCorteReposicion.find(ordenes.first).tipo_corte_reposicion_id
    @proceso_corte_reposicion_id = OrdenCorteReposicion.find(ordenes.first).proceso_corte_reposicion_id
    respond_with @completado
  end

  # GET
  # Elimina la ruta por comuna o solo comuna..
  def desasignacion_completa
    ruta_corte_reposicion = RutaCorteReposicion.find(params[:ruta])
    tipo_corte_reposicion = params[:tipo_corte_reposicion].to_i
    proceso_corte_reposicion = params[:proceso_corte_reposicion].to_i
    ordenes = OrdenCorteReposicion.where(ruta_corte_reposicion_id: ruta_corte_reposicion.id,estado_corte_reposicion_id: [2,3],tipo_corte_reposicion_id: tipo_corte_reposicion,proceso_corte_reposicion_id: proceso_corte_reposicion)
    @totalOrdenes = ordenes.count
    ordenes.each do |o|
      o.asignacion_corte_reposicion.destroy
      o.update(estado_corte_reposicion_id: 1, fecha_asignacion: '')
    end
    respond_with @totalOrdenes
  end

  # GET
  # Dividir Rutas para Desasignacion
  def dividir_ruta_desasignacion
    params.inspect
    @ruta_seleccionada = RutaCorteReposicion.find(params[:ruta])
    @lector = Empleado.find(params[:empleado])
    @asignaciones = AsignacionCorteReposicion.where(empleado_id: @lector.id, ruta_corte_reposicion_id: @ruta_seleccionada.id).joins(:orden_corte_reposicion).where(
                                     orden_corte_reposicions: {tipo_corte_reposicion_id: params[:tipo_corte_reposicion], estado_corte_reposicion_id: [2,3]})
  end

  # GET
  # Elimina las asignaciones parciales
  def desasignacion_parcial
    @completado_uno = params[:id_boton_desasignacion].to_i
    ruta = RutaCorteReposicion.find(params[:ruta])
    empleado = params[:empleado]
    ordenes = params[:ordenes].split(',')
    ordenes.each do |orden_id|
      orden = OrdenCorteReposicion.find(orden_id)
      orden.asignacion_corte_reposicion.destroy
      orden.update(estado_corte_reposicion_id:1, fecha_asignacion: '')
    end
    @lector_id = params[:empleado].to_i
    @ruta_seleccionada_id = ruta.id
    @tipo_lectura_id = OrdenCorteReposicion.find(ordenes.first).tipo_corte_reposicion_id
    respond_with @completado_uno
  end
end
