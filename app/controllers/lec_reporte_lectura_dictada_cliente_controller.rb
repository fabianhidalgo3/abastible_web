class LecReporteLecturaDictadaClienteController < ApplicationController
  respond_to :js, :html, :json, :pdf

	def index # > Pagina Principal
		user = current_usuario	# > Busca usuario y zonas y porcion del usuario logueado
    zonas = user.empleado.zona
		@regiones = Region.joins(:zona).where(zonas:{id: zonas}).group(:id)
	end

	# GET
	def carga_porciones 			
		zona = params[:zona].to_i
		mes = params[:mes].to_i
		ano = params[:ano].to_i
		@porciones = Porcion.where(zona_id: zona, mes: mes, ano: ano).order(:codigo)
		respond_with @porciones
	end

	def carga_zonas
    user = current_usuario
    @zonas = user.empleado.zona.where(region_id: params[:region])
  end
	
	# > Funcion encargada de buscar ordenes..
	# > Params[porcion]
	def carga_filtro 			
		lista = []
		@totales = Array.new
	
		#Variables de Totales
		totalAsignado = 0
		totalEfectivo = 0
		totalNoEfectivo = 0
    totalGasNatural = 0
    totalLecturaDictada = 0
		#Variables Tabla
		asignado = 0
		efectivo = 0
		noEfectivo = 0
		claveEfectivo = 0
		contador = 0
		efectividad = 0
		tipo_lectura = 1
		
		porcion_id = params[:porcion].to_i
		if porcion_id == 0
			porcion = Porcion.where(zona_id: params[:zona], mes: params[:mes].to_i, ano: params[:ano].to_i)
		else
			porcion = Porcion.find(params[:porcion])
		end
		rutas = Rutum.where(porcion_id: porcion, abierto:false, mes:params[:mes].to_i, ano: params[:ano].to_i)
		rutas.each do |ruta| 
	
      asignaciones = AsignacionLectura.joins(:orden_lectura).where(orden_lecturas:{estado_lectura_id: [1..5], rutum_id: ruta}).group(:rutum_id)
      asignaciones.each do |asignacion|
				ordenes = AsignacionLectura.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura).where(orden_lecturas:{verificacion: false})
				asignado = ordenes.joins(:orden_lectura).where(orden_lecturas: {estado_lectura_id: [2..5]}).count
				lecturaDictada = ordenes.joins(:orden_lectura).where(orden_lecturas: {clave_lectura_id: 46}).count							
     
				lista.push([asignacion.rutum.porcion.codigo,asignacion.rutum.codigo, asignado,lecturaDictada, ((lecturaDictada*100.0)/asignado).round(2)])
				totalAsignado = totalAsignado + asignado
				totalLecturaDictada = totalLecturaDictada + lecturaDictada
			end
			@totales = [totalAsignado, totalLecturaDictada,((totalLecturaDictada*100.0)/totalAsignado).round(2)]
	 end
	 # > Ordena Lista por Nombre Ruta
	 @lista = lista.sort! {|a,b| a[1] <=> b[1]}
	 respond_with @lista
	end

end
