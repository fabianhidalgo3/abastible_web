class LecReporteControlCoordenadasController < ApplicationController
	respond_to :js, :html, :json, :pdf
	# => Control de Coordenadas GPS
	def index # > Pagina Principal
		user = current_usuario	# > Busca usuario y zonas y porcion del usuario logueado
    zonas = user.empleado.zona
		@regiones = Region.joins(:zona).where(zonas:{id: zonas}).group(:id)
	end

	# GET
	def carga_zonas
    user = current_usuario
    @zonas = user.empleado.zona.where(region_id: params[:region])
  end

	# GET
	def carga_porciones
		zona = params[:zona].to_i
		mes = params[:mes].to_i
		ano = params[:ano].to_i
		@porciones = Porcion.where(zona_id: zona, mes: mes, ano: ano).order(:codigo)
		respond_with @porciones
	end

	# GET
  # PARCIAL: Muestra las Rutas
  def carga_rutas
    @rutas = Rutum.where(porcion_id: params[:porcion]).order(:codigo)
    respond_with @rutas
	end


	# > GET
	# > Tabla contenido
	def carga_filtro
		rutas = params[:ruta].to_i
		listaOrdenes = OrdenLectura.where(estado_lectura_id: [4..5], verificacion: false, rutum_id: rutas)
		@listaOrdenes = listaOrdenes.paginate(:page => params[:page], :per_page => 10)
		respond_with @listaOrdenes
	end

	# GET
	# Devuelve @hash con coordenadas GPS
	def carga_ubicacion
		ubicaciones = Array.new
	  latitud = params[:latitud]
	  longitud = params[:longitud]
	  ubicaciones.push([latitud.to_s, longitud.to_s, ""])
	  @hash = Gmaps4rails.build_markers(ubicaciones) do |u, marker|
	    marker.lat u[0]
	    marker.lng u[1]
	    marker.infowindow u[2]
		end
		respond_with @hash
	end

	# Exportar Csv
	def exportar_csv
		rutas = Rutum.where(mes: 9, ano: 2020)
		listaOrdenes = OrdenLectura.where(estado_lectura_id: [1..5], verificacion: false, rutum_id: rutas)
		header = "ID,REGION,SECUENCIA,COMUNA,PORCION,RUTA,MES,ANO,CALLE, NUMERO,DIRECCION, CTA CONTRATO,MEDIDOR,UBICACION MEDIDOR ,FECHA LECTURA,CLAVE LECTURA, LATITUD, LONGITUD, CONSUMO MINIMO, CONSUMO MAXIMO, LECTURA ANTERIOR, LECTURA ACTUAL, EMPLEADO \n"
    	fecha = Time.now.strftime("%e%m%Y_ %H%M%S")
    	file = "ABASTIBLEDB_SEPTIEMBRE.csv"
    	File.open(file, "w+:UTF-16LE:UTF-8") do |csv|
				 csv << header
					listaOrdenes.each do |orden_lectura|
						if !orden_lectura.fecha_ejecucion.nil?
							fecha_ejecucion = orden_lectura.fecha_ejecucion.strftime("%e/%m/%Y %H:%M:%S")
						else
							fecha_ejecucion = "00/00/00"
						end
						if !orden_lectura.asignacion_lectura.nil?
						 	empleado = orden_lectura.asignacion_lectura.empleado.nombre_completo
						else
						 	empleado = "SIN EMPLEADO"
						end
						if !orden_lectura.clave_lectura.nil?
							clave_lectura =  orden_lectura.clave_lectura.nombre
						else
							clave_lectura =  "SIN CLAVE"
						end

						if !orden_lectura.medidor.nil?
							numero_medidor = orden_lectura.medidor.numero_medidor
						else
							numero_medidor = "SIN MEDIDOR"
						end
						csv << orden_lectura.id.to_s + "," +
									 orden_lectura.rutum.porcion.zona.region.nombre + "," +
									 orden_lectura.secuencia_lector.to_s + "," +
									 orden_lectura.comuna.nombre + "," +
									 orden_lectura.rutum.porcion.codigo.to_s + "," +
									 orden_lectura.rutum.codigo + "," +
									 orden_lectura.rutum.mes.to_s + "," +
									 orden_lectura.rutum.ano.to_s + "," +
									 orden_lectura.cliente.calle.to_s + "," +
									 orden_lectura.cliente.numero_domicilio.to_s + "," +
									 orden_lectura.direccion + "," +
									 orden_lectura.cliente.numero_cliente.to_s + "," +
									 numero_medidor.to_s + "," +
									 orden_lectura.medidor.ubicacion_medidor.to_s + "," +
									 fecha_ejecucion + "," +
									 clave_lectura + "," +
									 orden_lectura.gps_latitud.to_s+ "," +
									 orden_lectura.gps_longitud.to_s+ "," +
							 		 orden_lectura.consumo_maximo.to_s + "," +
									 orden_lectura.consumo_minimo.to_s + "," +
									 orden_lectura.lectura_anterior.to_s + "," +
								 	 orden_lectura.lectura_actual.to_s+ "," +
								 	 empleado + "\n" 
					end
    	end
    	send_file(file, x_sendfile: true, buffer_size: 512)
	end
end
