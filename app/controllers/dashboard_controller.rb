class DashboardController < ApplicationController
	respond_to :js

	def index
		user  = current_usuario
		zonas = user.empleado.zona
				@regiones = Region.joins(:zona).where(zonas: {id: zonas}).group(:id).order(:id).order(:id)
.order(:id)
		@usuario_nombre = user.empleado.nombre_completo
		@perfil_nombre = user.perfil.nombre
		#actualizar_ordenes()
		#actualizar_fotografias()
	end


	def carga_graficos_efectividad
		region_id = params[:region].to_i
		zona_id = params[:zona].to_i
		porcion_id = params[:porcion].to_i
		ruta_id = params[:ruta].to_i
		mes = params[:mes].to_i
		ano = params[:ano].to_i
		# Zonas a Recorrer
		if region_id != 0 && zona_id == 0
      zonas = Zona.where(region_id: region_id)
   	else
			zonas = Zona.where(id: zona_id)
		end

		if porcion_id != 0
			porciones = Porcion.where(id: porcion_id, mes: mes, ano: ano)
		end
		efectividad = Array.new()
		zonas.each do |zona|
			if !porciones.nil?
				rutas = Rutum.where(porcion_id: porciones,  mes: mes, ano: ano)
			else
				rutas = Rutum.where(mes: mes, ano: ano).joins(porcion: :zona).where(porcions: {zona_id: zona})
			end

			leidos =  OrdenLectura.where(rutum_id: rutas, verificacion: false, estado_lectura_id: [4..5]).count
			efectivo =  OrdenLectura.where(rutum_id: rutas, verificacion: false, estado_lectura_id:[4..5]).joins(:clave_lectura).where(clave_lecturas: {efectivo:true}).count
			efectividad.push([zona.nombre, leidos, efectivo])
		end
		@efectividad = efectividad
	end

	# GET
	# @asignacion > Array List[3](zona, Leido, asignado)
	def carga_graficos_asignacion
		region_id = params[:region].to_i
		zona_id = params[:zona].to_i
		porcion_id = params[:porcion].to_i
		ruta_id = params[:ruta].to_i
		mes = params[:mes].to_i
		ano = params[:ano].to_i
		# Zonas a Recorrer
		if region_id != 0 && zona_id == 0
      zonas = Zona.where(region_id: region_id)
   	else
			zonas = Zona.where(id: zona_id)
		end

		if porcion_id != 0
			porciones = Porcion.where(id: porcion_id, mes: mes, ano: ano)
		end
		asignacion = Array.new()
		zonas.each do |zona|
			if !porciones.nil?
				rutas = Rutum.where(porcion_id: porciones, mes: mes, ano: ano)
			else
				rutas = Rutum.where(mes: mes, ano: ano).joins(porcion: :zona).where(porcions: {zona_id: zona})
			end

			sinAsignar =  OrdenLectura.where(rutum_id: rutas, verificacion: false, estado_lectura_id: 1).count
			leidos =  OrdenLectura.where(rutum_id: rutas, verificacion: false, estado_lectura_id: [4..5]).count
			asignados =  OrdenLectura.where(rutum_id: rutas, verificacion: false, estado_lectura_id: [2..3]).count
			asignacion.push([zona.nombre, leidos, asignados, sinAsignar])
		end
		p asignacion
		@asignacion = asignacion
	end

	# GET
	# @ConsumosCero > devuelve valor entero
	# @ConsumosMinimos > devuelve valor entero
	# @ConsumosMaximos > devuelve valor entero
	# Improcedentes > devuelve valor entero
	def carga_indicadores
		region_id = params[:region].to_i
		zona_id = params[:zona].to_i
		porcion_id = params[:porcion].to_i
		mes = params[:mes].to_i
		ano = params[:ano].to_i
		# Busco todas las zonas por región
		if (region_id != 0) && (zona_id == 0)
			zonas = Zona.where(region_id: region_id)
		end
		# busco la zona
		if (region_id != 0) && (zona_id != 0)
			zonas = Zona.find(zona_id)
		end

		# > Busco Todas las Rutas
		if (porcion_id != 0)
			rutas = Rutum.where(porcion_id: porcion_id)
		else
			porciones = Porcion.where(zona_id: zonas, mes: mes, ano: ano)
			rutas = Rutum.where(porcion_id: porciones, mes: mes, ano: ano)
		end

		leido =  OrdenLectura.where(rutum_id: rutas, estado_lectura_id: [4..5], verificacion:false).count
		efectivo =  OrdenLectura.where(rutum_id: rutas, estado_lectura_id:[4..5], verificacion:false).joins(:clave_lectura).where(clave_lecturas: {efectivo:true}).count
		@efectividad =  ((efectivo*100.0)/leido).round(2)

		if @efectividad.to_f.nan?
			@efectividad = 0
		end
		@consumosCero =  OrdenLectura.where(rutum_id: rutas, estado_lectura_id: [4..5], verificacion: false).joins(
	     :asignacion_lectura).where("consumo_actual = 0").count
		if @consumosCero.to_f.nan?
			@consumosCero = 0
		end			 
		@consumosMinimos = OrdenLectura.where(rutum_id: rutas, estado_lectura_id: [4..5], verificacion: false).joins(
	     :asignacion_lectura).where("consumo_actual < consumo_minimo").count
		if @consumosMinimos.to_f.nan?
			@consumosMinimos = 0
		end		
		@consumosMaximos = OrdenLectura.where(rutum_id: rutas, estado_lectura_id: [4..5], verificacion: false).joins(:asignacion_lectura).where("consumo_actual > consumo_maximo").count
		if @ConsumosMaximos.to_f.nan?
			@ConsumosMaximos = 0
		end				
		@improcedencias = OrdenLectura.where(rutum_id: rutas, estado_lectura_id: [4..5], verificacion: false).joins(:asignacion_lectura, :clave_lectura).where(clave_lecturas:{efectivo:false}).count
		if @improcedencias.to_f.nan?
			@improcedencias = 0
		end
	end


	def carga_graficos_efectividad_porcion
		region_id = params[:region].to_i
		zona_id = params[:zona].to_i
		porcion_id = params[:porcion].to_i
		ruta_id = params[:ruta].to_i
		mes = params[:mes].to_i
		ano = params[:ano].to_i
		# Zonas a Recorrer
		if region_id != 0 && zona_id == 0
      zonas = Zona.where(region_id: region_id)
   	else
			zonas = Zona.where(id: zona_id)
		end

		if porcion_id != 0
			porciones = Porcion.where(id: porcion_id, mes: mes, ano: ano)
		else
			porciones = Porcion.where(zona_id: zona_id, mes: mes, ano: ano)
		end
		p porciones
		if porciones.blank?
			porciones = Porcion.where(mes: mes, ano: ano).joins(:zona).where(zonas: {region_id: region_id})
		end

		efectividadPorcion = Array.new()
		porciones.each do |porcion|
			rutas = Rutum.where(porcion_id: porcion,  mes: mes, ano: ano)
			leidos =  OrdenLectura.where(rutum_id: rutas, verificacion: false, estado_lectura_id: [4..5]).count
			efectivo =  OrdenLectura.where(rutum_id: rutas, verificacion: false, estado_lectura_id:[4..5]).joins(:clave_lectura).where(clave_lecturas: {efectivo:true}).count
			mesAnterior = mes - 1
			porcionMesAnterior = Porcion.where(codigo: porcion.codigo, mes: mesAnterior, ano: ano)
			rutasMesAnterior = Rutum.where(porcion_id: porcionMesAnterior)
			leidoMesAnterior =  OrdenLectura.where(rutum_id: rutasMesAnterior, verificacion: false, estado_lectura_id: [4..5]).count
			efectivoMesAnterior = OrdenLectura.where(rutum_id: rutasMesAnterior, verificacion: false, estado_lectura_id:[4..5]).joins(:clave_lectura).where(clave_lecturas: {efectivo:true}).count

			porceMesActual = ((efectivo*100.0)/leidos).round(2)
			porceMesAnterior = ((efectivoMesAnterior*100.0)/leidoMesAnterior).round(2)
			p porceMesActual
			p porceMesAnterior
			if porceMesActual.nan?
				 porceMesActual = 0.0
			end
			if porceMesAnterior.nan?
				porceMesAnterior = 0.0
			end

			efectividadPorcion.push([porcion.codigo, leidos, efectivo, porceMesActual ,leidoMesAnterior, efectivoMesAnterior, porceMesAnterior])
		end
		@efectividadPorcion = efectividadPorcion
	end


	# GET
	# @zonas > Devuelve objeto con porciones
	def carga_zonas
    user = current_usuario
    @zonas = user.empleado.zona.where(region_id: params[:region])
  end

  # GET
  # @porciones -> Devuelve objeto con porciones
	def carga_porciones
		zona = params[:zona].to_i
		mes = params[:mes].to_i
		ano = params[:ano].to_i
		@porciones = Porcion.where(zona_id: zona, mes: mes, ano: ano).order(:codigo)
		respond_with @porciones
	end

	# GET
  # @porciones -> Devuelve objeto con porciones
  def carga_rutas
    @rutas = Rutum.where(porcion_id: params[:porcion], abierto: true).order(:codigo)
    respond_with @rutas
	end

	# > Descargar Manual Aplicación Web
	def descargar_manual_web
		send_file "#{Rails.root}/public/Manuales/web.pdf", type: "application/pdf", x_sendfile: true
	end

	# > Descargar Manual Aplicación Movíl
	def descargar_manual_app
		send_file "#{Rails.root}/public/Manuales/app.pdf", type: "application/pdf", x_sendfile: true
	end

	def carga_porciones_reparto
		zona = params[:zona].to_i
		mes = params[:mes].to_i
		ano = params[:ano].to_i
		@porcionesReparto = PorcionReparto.where(zona_id: zona, mes: mes, ano: ano).order(:codigo)
		respond_with @porcionesReparto
	end

	def carga_graficos_efectividad_reparto
		region_id = params[:region].to_i
		zona_id = params[:zona].to_i
		porcion_id = params[:porcion].to_i
		ruta_id = params[:ruta].to_i
		mes = params[:mes].to_i
		ano = params[:ano].to_i
		# Zonas a Recorrer
		if region_id != 0 && zona_id == 0
      zonas = Zona.where(region_id: region_id)
   	else
			zonas = Zona.where(id: zona_id)
		end

		if porcion_id != 0
			porciones = PorcionReparto.where(id: porcion_id, mes: mes, ano: ano)
		end
		efectividad = Array.new()
		zonas.each do |zona|
			if !porciones.nil?
				rutas = RutaReparto.where(porcion_reparto_id: porciones,  mes: mes, ano: ano)
			else
				rutas = RutaReparto.where(mes: mes, ano: ano).joins(porcion_reparto: :zona).where(porcion_repartos: {zona_id: zona})
			end

			leidos =  OrdenReparto.where(ruta_reparto_id: rutas, estado_reparto_id: [4..5]).count
			efectivo =  OrdenReparto.where(ruta_reparto_id: rutas, estado_reparto_id:[4..5]).count
			efectividad.push([zona.nombre, leidos, efectivo])
		end
		@efectividad = efectividad
	end
	def carga_graficos_asignacion_reparto
	end
	
	def carga_zonas_reparto
    user = current_usuario
    @zonas = user.empleado.zona.where(region_id: params[:region])
	end
	
		def carga_indicadores_reparto
		region_id = params[:region].to_i
		zona_id = params[:zona].to_i
		porcion_id = params[:porcion].to_i
		mes = params[:mes].to_i
		ano = params[:ano].to_i
		# Busco todas las zonas por región
		if (region_id != 0) && (zona_id == 0)
			zonas = Zona.where(region_id: region_id)
		end
		# busco la zona
		if (region_id != 0) && (zona_id != 0)
			zonas = Zona.find(zona_id)
		end

		# > Busco Todas las Rutas
		if (porcion_id != 0)
			rutas = RutaReparto.where(porcion_reparto_id: porcion_id)
		else
			porciones = PorcionReparto.where(zona_id: zonas, mes: mes, ano: ano)
			rutas = RutaReparto.where(porcion_reparto_id: porciones, mes: mes, ano: ano)
		end
		p @pendientes =  OrdenReparto.where(ruta_reparto_id: rutas, estado_reparto_id: [1..3]).count
		p @entregas =  OrdenReparto.where(ruta_reparto_id: rutas, estado_reparto_id:[4..5]).count
	end



end
