class LecReporteLecturasEstimadasController < ApplicationController
  respond_to :js, :html, :json, :pdf

	def index # > Pagina Principal
		user = current_usuario	# > Busca usuario y zonas y porcion del usuario logueado
    zonas = user.empleado.zona
		@regiones = Region.joins(:zona).where(zonas:{id: zonas}).group(:id)
	end

	# GET
	def carga_porciones 			
		zona = params[:zona].to_i
		mes = params[:mes].to_i
		ano = params[:ano].to_i
		@porciones = Porcion.where(zona_id: zona, mes: mes, ano: ano).order(:codigo)
		respond_with @porciones
	end

	def carga_zonas
    user = current_usuario
    @zonas = user.empleado.zona.where(region_id: params[:region])
  end
	
	# > Funcion encargada de buscar ordenes..
	# > Params[porcion]
	def carga_filtro 			
		lista = []
		@totales = Array.new
	
		#Variables de Totales
		totalAsignado = 0
		totalEfectivo = 0
		totalNoEfectivo = 0
		totalClienteNoPermite = 0
		#Variables Tabla
		asignado = 0
		efectivo = 0
		noEfectivo = 0
		claveEfectivo = 0
		contador = 0
		efectividad = 0
		tipo_lectura = 1
		
		porcion_id = params[:porcion].to_i
		if porcion_id == 0
			porcion = Porcion.where(zona_id: params[:zona], mes: params[:mes].to_i, ano: params[:ano].to_i)
		else
			porcion = Porcion.find(params[:porcion])
		end
		rutas = Rutum.where(porcion_id: porcion, abierto:false, mes:params[:mes].to_i, ano: params[:ano].to_i)
rutas.each do |ruta|
		#empleados = Empleado.joins(:usuario, :asignacion_lectura).where(usuarios: {perfil_id: 5}, asignacion_lecturas: {rutum_id: rutas}).group(:id)
		#empleados.each do |empleado|
      asignaciones = AsignacionLectura.joins(:orden_lectura).where(orden_lecturas:{estado_lectura_id: [1..5], rutum_id: ruta}).group(:rutum_id)
      asignaciones.each do |asignacion|
				ordenes = AsignacionLectura.where(rutum_id: asignacion.rutum_id).joins(:orden_lectura).where(orden_lecturas:{verificacion: false})
				asignado = ordenes.joins(:orden_lectura).where(orden_lecturas: {estado_lectura_id: [2..5]}).count
        efectivo =  ordenes.joins(:orden_lectura => [:clave_lectura]).where(clave_lecturas:{ efectivo: true},orden_lecturas: {estado_lectura_id: [2..5]}).count
        noEfectivo=  ordenes.joins(:orden_lectura => [:clave_lectura]).where(clave_lecturas:{ efectivo: false}, orden_lecturas: {estado_lectura_id: [2..5]}).count
				pendientes = ordenes.joins(:orden_lectura).where(orden_lecturas: {estado_lectura_id: [4..5]}).count
				clienteNoPermite=  ordenes.joins(:orden_lectura => [:clave_lectura]).where(clave_lecturas:{id: 21}, orden_lecturas: {estado_lectura_id: [2..5]}).count

        hora = ordenes.joins(:orden_lectura).where(orden_lecturas: { estado_lectura_id: [4..5]}).order("orden_lecturas.fecha_ejecucion ASC")
        if !hora.blank? then
					horaInicio = hora.first.orden_lectura.fecha_ejecucion.strftime("%H:%M:%S")
					fechaInicio = hora.first.orden_lectura.fecha_ejecucion.strftime("%d/%m/%Y")
					fechaTermino = hora.last.orden_lectura.fecha_ejecucion.strftime("%d/%m/%Y")
          horaTermino = hora.last.orden_lectura.fecha_ejecucion.strftime("%H:%M:%S")
        else
          horaInicio = " "
          horaTermino = " "
				end
				lista.push([asignacion.rutum.porcion.codigo,asignacion.rutum.codigo, asignado,(efectivo - clienteNoPermite), noEfectivo,clienteNoPermite, (noEfectivo + clienteNoPermite), (((noEfectivo + clienteNoPermite)*100.0)/asignado).round(2).to_s + "%", (((efectivo - clienteNoPermite)*100.0)/asignado).round(2).to_s + "%"])
				totalAsignado = totalAsignado + asignado
				totalEfectivo = totalEfectivo + efectivo
				totalClienteNoPermite = totalClienteNoPermite + clienteNoPermite
				totalNoEfectivo = totalNoEfectivo + noEfectivo
			end
			@totales = [totalAsignado,(totalEfectivo - totalClienteNoPermite),totalNoEfectivo, totalClienteNoPermite, (totalNoEfectivo + totalClienteNoPermite), (((totalNoEfectivo + totalClienteNoPermite)*100.0)/totalAsignado).round(2).to_s + "%", (((totalEfectivo - totalClienteNoPermite) * 100.0)/totalAsignado).round(2).to_s + "%"]
	 end
	 # > Ordena Lista por Nombre Ruta
	 @lista = lista.sort! {|a,b| a[1] <=> b[1]}
	 respond_with @lista
	end


end
