class LecCierreLecturasController < ApplicationController
	respond_to :js, :html, :json
	require 'aws-sdk-s3'

	def index
		user = current_usuario
    zonas = user.empleado.zona
		@regiones = Region.joins(:zona).where(zonas:{id: zonas}).group(:id)
	end

	def carga_zonas
    user = current_usuario
   	@zonas = user.empleado.zona.where(region_id: params[:region]) if !params[:region].blank?
  end
  
	# GET
  # PARCIAL: Muestra las porciones
	def carga_porciones
		zona = params[:zona].to_i
		mes = params[:mes].to_i
		ano = params[:ano].to_i
		@porciones = Porcion.where(zona_id: zona, mes: mes, ano: ano).order(:codigo)
		respond_with @porciones
	end

	# GET
  # PARCIAL: Muestra las Rutas
  def carga_rutas
    @rutas = Rutum.where(porcion_id: params[:porcion]).order(:codigo)
    respond_with @rutas
	end

	# GET Cierra procesos
	# Parcial: Devuelve mensaje de exito
	def cerrar_proceso
		ruta = Rutum.find(params[:ruta])
		ordenes = OrdenLectura.where(rutum_id: ruta, verificacion: false)
		ordenes.each do |orden|
			if orden.estado_lectura_id == 1 || orden.estado_lectura_id == 2 || orden.estado_lectura_id == 3
				orden.update(estado_lectura_id: 5, gps_latitud: 0.0, gps_longitud: 0.0)
				orden.update(fecha_ejecucion: Time.now, clave_lectura_id: 21)#clave 44
			end
		end
		# Cierro la Ruta
		ruta.update(abierto: false)
    cerrar_porciones(ruta.porcion_id)
		# Si la Ruta esta Cerrada, Creo Mesaje de Respuesta...
		if !ruta.abierto
			mensaje = "Se Cerro Exitosamante la Unidad de Lectura " + ruta.codigo.to_s + " con un total de " + ordenes.count.to_s + " servicios"
		end
		# Creo Ordenes de Reparto.
		#crear_repartos(ruta)
		enviar_ftp(ruta)
		 # SalidasProviderWorker.perform_async(ruta.id)
		@mensaje = mensaje
		respond_with @mensaje
	end

  # Creo Funcion para cerrar porcion
  def cerrar_porciones(porcion_id)
    rutas = Rutum.where(porcion_id: porcion_id).count
    rutasCerradas = Rutum.where(porcion_id: porcion_id, abierto: false).count
    if rutas == rutasCerradas
      porcion = Porcion.find(porcion_id)
      porcion.update(abierto: false)
    end
  end

	# Crea las Ordenes de Reparto
	# Las Genera con las ordenes de lectura que no sean ni despacho postal ni factura electronica
	# params[:ruta]
	def crear_repartos(ruta)
		# Busco Ordenes Sin Despacho Postal
		ordenes = OrdenLectura.where(rutum_id: ruta, verificacion: false).joins(:cliente).where(clientes: {despacho_postal: 0})
		#  Busco la porcionReparto
		porcionReparto = PorcionReparto.where(codigo: ruta.porcion.codigo, mes: ruta.porcion.mes, ano: ruta.porcion.ano, abierto: true, zona_id: ruta.porcion.zona_id).first
		# Si no es Nulo la Crea
		if porcionReparto.nil?
			porcionReparto = PorcionReparto.create(codigo: ruta.porcion.codigo, mes: ruta.porcion.mes, ano: ruta.porcion.ano, abierto: true, zona_id: ruta.porcion.zona_id)
		end
		# Busco Ruta Reparto
		rutaReparto = RutaReparto.where(codigo: ruta.codigo,mes: ruta.mes, ano: ruta.ano,porcion_reparto_id: porcionReparto.id, abierto: true).first
		# Si no es nulo la Crea
		if rutaReparto.nil?
			rutaReparto = RutaReparto.create(codigo: ruta.codigo, mes: ruta.mes, ano: ruta.ano,porcion_reparto_id: porcionReparto.id, abierto: true)
		end
		#Creo las Ordenes de Reparto
		ordenes.each do |orden|
			OrdenReparto.create(codigo: orden.codigo, domicilio: orden.direccion, fecha_carga: Time.zone.now, estado_reparto_id: 1,tipo_documento_id: 1,cliente_id: orden.cliente_id, medidor_id: orden.medidor_id,instalacion_id: orden.instalacion_id,secuencia_lector: orden.secuencia_lector,direccion_entrega: orden.direccion,ruta_reparto_id: rutaReparto.id, comuna_id: orden.comuna_id, numero_cliente: orden.cliente.numero_cliente)
		end
	end

	# GET
	# Mustra el listado de las Rutas
	# Devuelve lista con cantidades de Rutas
	# List[] = rutaNombre, asignado, leido, porcentual, totalRuta, Abierto, Ruta.Id
	# params[:porcion], params[:ruta], params[:zona]
	def carga_cierre
		porcion_id = params[:porcion].to_i
		ruta_id = params[:ruta].to_i
		zona_id = params[:zona].to_i
		mes = params[:mes].to_i
		ano =  params[:ano].to_i
		listaRutas = Array.new
		if (porcion_id == 0 && ruta_id == 0)
			rutas = Rutum.where(mes:mes, ano:ano).joins(:porcion).where(porcions:{zona_id: zona_id, mes:mes, ano:ano})
		end

		if porcion_id != 0 && ruta_id == 0
			rutas = Rutum.where(porcion_id: porcion_id, mes:mes, ano:ano)
		end

		if ruta_id != 0
			rutas = Rutum.where(id: ruta_id, mes:mes, ano:ano)
		end

		asignado = 0
		leido = 0
		ruta_nombre = ""
		rutas.each do |ruta|
			ruta_nombre = ruta.codigo
			asignado = OrdenLectura.where(estado_lectura_id: [2..5], verificacion: false).joins(:rutum).where(ruta: {id: ruta}).count# ok
			leido = OrdenLectura.where(estado_lectura_id: [4..5], verificacion: false).joins(:rutum).where(ruta: {id: ruta}).count
			totalRuta = OrdenLectura.where(verificacion: false).joins(:rutum).where(ruta: {id: ruta}).count# ok
			verificacion = OrdenLectura.where(verificacion: false, tipo_lectura_id: 2, estado_lectura_id:[1..3]).joins(:rutum).where(ruta: {id: ruta}).count# ok
			if leido == 0 && asignado == 0
				porcentual = 0
			else
				porcentual = ((leido*100)/asignado).round(0)
			end
			listaRutas.push([ruta_nombre, asignado, leido,porcentual,totalRuta, ruta.abierto, ruta.id, verificacion, ruta.mes])
		end
		@listaRutas = listaRutas
		@listaRutas.sort! {|a,b| a[0] <=> b[0]}
		respond_with @listaRutas
	end

	# GET
	# Devuelve archivo formato CSV
	# Descargar Archivo
	def descargar_archivo
		ruta = Rutum.find(params[:ruta])
		ordenes = OrdenLectura.where(rutum_id: ruta, verificacion: false)
	  fecha = Time.now.strftime("%d%m%Y%H%M%S").to_s
		nameFile = ruta.codigo + "_" +  fecha.to_s + ".csv"
		header = "RUTA, FECHA EJECUCION, ESTANQUE/MEDIDOR,Nº CLIENTE,Nº INSTALACION, Nº MEDIDOR, MODELO MEDIDOR, LECTURA ACTUAL, CLAVE LECTURA\n"
	    File.open(nameFile, "w+:UTF-16LE:UTF-8") do |csv|
      	csv << header
      	ordenes.each do |l|
					fecha_ejecucion =l.fecha_ejecucion
					if !l.clave_lectura.nil?
						efectivo =l.clave_lectura.efectivo
						if  l.clave_lectura_id == 1
							clave_lectura = " "
						elsif l.clave_lectura_id == 21
							clave_lectura = "44"
						else
							clave_lectura = l.clave_lectura.codigo
						end
					end

					if !fecha_ejecucion.nil?
						fecha_ejecucion = fecha_ejecucion.to_s
					else
						fecha_ejecucion = "00/00/00"
					end

					csv << l.rutum.codigo.to_s + "," +
					fecha_ejecucion + "," +
          "0001" + ","  +
          l.cliente.numero_cliente.to_s + ","  +
          l.instalacion.codigo.to_s + "," +
          l.medidor.numero_medidor.to_s + "," +
          l.medidor.modelo_medidor.nombre.to_s + "," +
					l.lectura_actual.to_s + "," +
					clave_lectura + "\n"
    	end
    	send_file(nameFile, x_sendfile: true, buffer_size: 512, disposition: 'attachment')
  	end
	end

	def enviar_ftp(ruta)
		rutaCodigo = ruta.codigo
		listaOrdenes = OrdenLectura.where(estado_lectura_id: [4..5], verificacion: false, rutum_id: ruta.id)
		header = "ID,REGION,SECUENCIA,COMUNA,PORCION,RUTA,MES,ANO,CALLE, NUMERO,DIRECCION, CTA CONTRATO,MEDIDOR,UBICACION MEDIDOR ,FECHA LECTURA,CLAVE LECTURA, LATITUD, LONGITUD, CONSUMO MINIMO, CONSUMO MAXIMO, LECTURA ANTERIOR, LECTURA ACTUAL, EMPLEADO \n"
		fecha = Time.now.strftime("%e%m%Y_ %H%M%S")
		file = rutaCodigo + "_" + fecha.to_s + ".csv"
   	File.open(file, "w+:UTF-16LE:UTF-8") do |csv|
			csv << header
			listaOrdenes.each do |orden_lectura|
				if !orden_lectura.fecha_ejecucion.nil?
					fecha_ejecucion = orden_lectura.fecha_ejecucion.strftime("%e/%m/%Y %H:%M:%S")
				else
					fecha_ejecucion = "00/00/00"
				end
				if !orden_lectura.asignacion_lectura.nil?
				 	empleado = orden_lectura.asignacion_lectura.empleado.nombre_completo
				else
				 	empleado = ""
				end
				csv << orden_lectura.id.to_s + "," +
							 orden_lectura.rutum.porcion.zona.region.nombre + "," +
							 orden_lectura.secuencia_lector.to_s + "," +
							 orden_lectura.comuna.nombre + "," +
							 orden_lectura.rutum.porcion.codigo.to_s + "," +
							 orden_lectura.rutum.codigo + "," +
							 orden_lectura.rutum.mes.to_s + "," +
							 orden_lectura.rutum.ano.to_s + "," +
							 orden_lectura.cliente.calle.to_s + "," +
							 orden_lectura.cliente.numero_domicilio.to_s + "," +
							 orden_lectura.direccion + "," +
							 orden_lectura.cliente.numero_cliente.to_s + "," +
							 orden_lectura.medidor.numero_medidor.to_s + "," +
							 orden_lectura.medidor.ubicacion_medidor.to_s + "," +
							 fecha_ejecucion + "," +
							 orden_lectura.clave_lectura.nombre + "," +
							 orden_lectura.gps_latitud.to_s+ "," +
							 orden_lectura.gps_longitud.to_s+ "," +
					 		 orden_lectura.consumo_maximo.to_s + "," +
							 orden_lectura.consumo_minimo.to_s + "," +
							 orden_lectura.lectura_anterior.to_s + "," +
						 	 orden_lectura.lectura_actual.to_s+ "," +
						 	 empleado + "\n" 
			end
			s3 = Aws::S3::Resource.new(region:'us-east-1')
			obj = s3.bucket('provider-salidas').object(file)
			obj.upload_file(file)
		end
	end
end
