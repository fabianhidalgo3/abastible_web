class LecFiltrosController < ApplicationController
  respond_to :js, :html, :json

  # GET
  # @zonas ->  Devuelve objeto con zonas
  def carga_zonas
    user = current_usuario
    @zonas = user.empleado.zona.where(region_id: params[:region])
    @lectores = Empleado.joins(:zona).where(zonas: {id: @zonas})
  end

  # GET
  # @porciones -> Devuelve objeto con porciones
  def carga_porciones
    @porciones = Porcion.where(zona_id: params[:zona], abierto: true).order(:codigo)
    respond_with @porciones
  end

  # GET
  # @rutas -> Devuelve objeto con todas las Rutas
  def carga_rutas
    @rutas = Rutum.where(porcion_id: params[:porcion]).order(:codigo)
    respond_with @rutas
  end

  # GET
  # Retorna lista con empleados
  def carga_lectores
    p params[:zona]
    @lectores = Empleado.joins(:usuario, :zona).where(usuarios:{perfil_id: 5}, zonas:{id: params[:zona]})
    respond_with @lectores
  end

end
