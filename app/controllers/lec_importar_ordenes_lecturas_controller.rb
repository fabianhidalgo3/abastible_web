class LecImportarOrdenesLecturasController < ApplicationController
  respond_to :js, :html, :json
  require 'csv'

  # @tipoLecturas > Normal / Verificación
  def index
    @tipoLecturas = TipoLectura.all#find(:all,:select => "tipo_lectura.id,tipo_lectura.nombre")
  end

  # POST
  # IMPORT CSV
  def import_csv
    csv = params[:file]
    #ImportarOrdenesLecturaWorker.perform_async(csv.path)
    carga_sistema = CargaSistema.first
    if carga_sistema.nil?
      proceso = 1
      carga_sistema = CargaSistema.create(codigo: proceso)
    else
      proceso = carga_sistema.id + 1
      carga_sistema = CargaSistema.create(codigo: proceso)
    end
    row = Array.new
     # Arreglo de columnas para la carga
     #Crear Hilo de Ejecución
     CSV.foreach((csv.path), headers: true) do |row|
       estadoLectura = 1 # Estado de Lectura Sin Asignar
       # Validación de campos de carga masiva.
       secuencia_lector = row[0]
       secuencia_lector = "" if secuencia_lector.nil?       
       fecha = row[1]
       fecha = "" if fecha.nil?
       hora = row[2]
       hora = "" if hora.nil?
       nombre_ruta = row[3]
       nombre_ruta = "" if nombre_ruta.nil?
       nombre_porcion = row[3][0..4]
       nombre_porcion = "" if nombre_porcion.nil?
       tipo_aparato = row[4]
       tipo_aparato = "" if hora.nil?
       fecha_propuesta = row[5]
       fecha_propuesta = "" if fecha_propuesta.nil?
       codigo_zonal = row[6]
       codigo_zonal = "" if codigo_zonal.nil?
       # Agregar Tabla de Matriz
       nombre_matriz = row[7]
       nombre_matriz = "" if nombre_matriz.nil?
       nombre_comuna = row[8]
       nombre_comuna = "" if nombre_comuna.nil?
       calle = row[9]
       calle = "" if calle.nil?
       numero_domicilio = row[10]
       numero_domicilio = "" if numero_domicilio.nil?
       ubicacion_medidor = row[11]
       ubicacion_medidor = "" if ubicacion_medidor.nil?
       numero_cliente = row[12]
       numero_cliente = "Error" if numero_cliente.nil?
       numero_medidor = row[13]
       numero_medidor = "Error" if numero_medidor.nil?
       nombre_marca_medidor = row[14]
       nombre_marca_medidor = "" if nombre_marca_medidor.nil?
       nombre_modelo_medidor = row[15]
       nombre_modelo_medidor = "" if nombre_modelo_medidor.nil?
       #row[16] > No se utiliza numero de Equipo
       numero_digito_medidor = row[17]
       numero_digito_medidor = "" if numero_digito_medidor.nil?
       numero_instalacion = row[18]
       numero_instalacion = "Error" if numero_instalacion.nil?
       consumo_maximo = row[19]
       consumo_maximo = "" if consumo_maximo.nil?
       consumo_minimo = row[20]
       consumo_minimo = "" if consumo_minimo.nil?
       lectura_anterior = row[21]
       lectura_anterior = "" if lectura_anterior.nil?
       consumo_promedio = row[22]
       consumo_promedio = "" if consumo_promedio.nil?
       # row[23] DEU > Cantidad de meses de deuda
       # row[24] Capacidad de Tanque
       # row[25] PDA
       # row[26] Usuario
       # row[27] Fecha Ejecución
       color = row[28]
       color = 0 if color.nil?
       if (numero_instalacion != "Error") || (numero_cliente != "Error")
         # Cambiar Región Por defecto
         zona = Zona.where(codigo: codigo_zonal).first
         zona = Zona.create(codigo: codigo_zonal, region_id: 10) if zona.nil?
         #Busco Comunas Precargadas
         comuna = Comuna.where(nombre: nombre_comuna).first
         # Si no existe comuna vinculada la creo
         comuna = Comuna.create(nombre: nombre_comuna, zona_id: zona.id) if comuna.nil?
         # Busco la porcion
         porcion = Porcion.where(codigo:nombre_porcion, mes: 8, ano: Time.now.year, zona_id: zona.id).first
         # Si no existe la creo
         porcion = porcion = Porcion.create(codigo: nombre_porcion, mes: 8, ano: Time.now.year, zona_id: zona.id, abierto:true) if porcion.nil?
         # Busco la ruta
         ruta = Rutum.where(codigo: nombre_ruta, mes: 8, ano: Time.now.year).first
         # Si no existe lo creo
         ruta = Rutum.create(codigo: nombre_ruta, mes: 8, ano: Time.now.year, abierto:true, porcion_id: porcion.id) if ruta.nil?
         # Busco el cliente
         cliente = Cliente.where(numero_cliente: numero_cliente).first if numero_cliente != "0000"
         # Si No Existe lo Creo
         cliente = Cliente.create(numero_cliente: numero_cliente, direccion_completa: calle + " " + numero_domicilio, calle: calle, numero_domicilio: numero_domicilio) if cliente.nil?
         # Busco Instalación
         instalacion = Instalacion.where(codigo: numero_instalacion).first if numero_instalacion != "0000"
         # Si no Existe la creo
         instalacion = Instalacion.create(codigo: numero_instalacion) if instalacion.nil?
         # Busco Marca de Medidor
         marca_medidor = MarcaMedidor.where(nombre: nombre_marca_medidor).first
         # Si no la encuentro la creo
         marca_medidor = MarcaMedidor.create(nombre: nombre_marca_medidor)  if marca_medidor.nil?
         # Busco Modelo Medidor
         modelo_medidor = ModeloMedidor.where(nombre: nombre_modelo_medidor, marca_medidor_id: marca_medidor.id).first
         # Si no lo encuentr lo creo
         modelo_medidor = ModeloMedidor.create(nombre: nombre_modelo_medidor,marca_medidor_id: marca_medidor.id) if modelo_medidor.nil?
         # Creo el Medidor
         medidor = Medidor.where(numero_medidor: numero_medidor, modelo_medidor_id: modelo_medidor.id, ubicacion_medidor: ubicacion_medidor).first if numero_medidor != "0000"
         # Si no existe lo creo
         medidor = Medidor.create(numero_medidor: numero_medidor, modelo_medidor_id: modelo_medidor.id, ubicacion_medidor: ubicacion_medidor) if medidor.nil?
         #Creo el Correlativo de los Códigos de las Ordenes de Lectura
         codigoOrden = OrdenLectura.last
         if codigoOrden.nil?
           codigoOrden = 1
           codigoOrden = "AB-"+ codigoOrden.to_s
         else
         codigoOrden = codigoOrden.id
         codigoOrden = codigoOrden + 1
         codigoOrden = "AB-" + codigoOrden.to_s
         end
         # Valido si la orden Lectura Existe
         ordenLectura = OrdenLectura.where(codigo: codigoOrden).first
         # Si no Existe la creo junto con el detalle_orden_lectura
         if ordenLectura.nil?
           ordenLectura = OrdenLectura.create(numero_medidor: medidor.numero_medidor, codigo: codigoOrden,secuencia_lector: secuencia_lector,instalacion_id: instalacion.id,cliente_id:cliente.id,rutum_id: ruta.id,tipo_lectura_id: 1,estado_lectura_id: estadoLectura,fecha_carga: Time.now,direccion: calle + " " + numero_domicilio ,verificacion: false,comuna_id: comuna.id,carga_sistema_id: carga_sistema.id,medidor_id: medidor.id, color: color, lectura_anterior: row[21],consumo_maximo: row[19],consumo_minimo: row[20],consumo_promedio: row[22])
         end
       end
    end
  end



end
