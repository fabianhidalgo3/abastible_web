class LecReporteNumeroIntentosLectorController < ApplicationController
	respond_to :js, :html, :json

	def index
		user = current_usuario
    @zonas = user.empleado.zona
    @tipo_lectura = TipoLectura.all
	end

	# => Carga Porciones
	def carga_porciones
		@porciones = Porcion.where(zona_id: params[:zona], abierto: true).order(:codigo)
    respond_with @porciones
	end

	# => Carga Comunas
	def carga_comunas
		@comunas = Comuna.where(zona_id: params[:zona])
		respond_with @comunas
	end

	# => Carga Empleados
	def carga_empleados  #Busca los empleados  por zona y perfil 6, 7
		porcion = Porcion.where(id: params[:porcion]).take
		if !porcion.nil?
			@empleados = porcion.zona.empleado.joins(:usuario).where(usuarios: {perfil_id: 5})
		end
    respond_with @empleados
	end

	def carga_filtro
		#Busca asignaciones de empleado para porcion, proceso y comuna seleccionados
		ordenes = OrdenLectura.where(estado_lectura_id: [2..5], tipo_lectura_id: params[:tipo_lectura]).joins(:asignacion_lectura, :rutum).
																where(asignacion_lecturas: {empleado_id: params[:empleado]},
																ruta:{porcion_id: params[:porcion]})
		@header_hash = Hash.new()
		intentos_hash = Hash.new()
		ordenes.each do |orden|
			num_intentos = orden.detalle_orden_lectura.take.intento.count
			key = orden.rutum.codigo + "_" + num_intentos.to_s
			if intentos_hash[key].nil?
				intentos_hash[key] = 1
			else
				intentos_hash[key] = intentos_hash[key] + 1
			end
			@header_hash[num_intentos] = num_intentos
		end
		@header_hash = @header_hash.values.sort
		asignaciones = AsignacionLectura.where(empleado_id: params[:empleado]).joins(:orden_lectura).where(orden_lecturas:{estado_lectura_id: [2..5],tipo_lectura_id: params[:tipo_lectura]}).group(:rutum_id)
		@lista = Array.new
		asignaciones.each do |asignacion|
			row = Array.new
			row.push(asignacion.orden_lectura.rutum.porcion.codigo)
			row.push(asignacion.orden_lectura.comuna.nombre)
			row.push(asignacion.orden_lectura.asignacion_lectura.empleado.nombre_completo)
			row.push(asignacion.orden_lectura.rutum.codigo)
			#Total asignado al lector de esta ruta
			row.push(AsignacionLectura.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura).where(orden_lecturas: {tipo_lectura_id: params[:tipo_lectura], estado_lectura_id: [2..5],verificacion:false}).count)
			#recorrer hash
			key = asignacion.orden_lectura.rutum.codigo + "_"
			intentos = Array.new
			@header_hash.each do |header|
				if intentos_hash[key + header.to_s].nil?
					intentos.push(0)
				else
					intentos.push(intentos_hash[key + header.to_s])
				end
			end
			p intentos
			row.push(intentos)
			@lista.push(row)
		end
		@lista = @lista.paginate(:page => params[:page], :per_page => 5)
		respond_with @lista
	end

end
