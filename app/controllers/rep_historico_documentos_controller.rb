class RepHistoricoDocumentosController < ApplicationController
  respond_to :js, :html, :json

  def index
  end

  def carga_filtro
		seleccion = params[:seleccion].to_i
		palabraBuscada = "%#{params[:busqueda]}%"
		# Busca por Cliente
		if seleccion == 1
			@ordenRepartos= OrdenReparto.where(estado_reparto: [4,5]).joins(:cliente).where('clientes.numero_cliente LIKE ?', palabraBuscada)
			#Busca por medidor
		elsif seleccion == 2
			@ordenRepartos = OrdenReparto.where(estado_reparto: [4,5]).joins(:medidor).where('medidors.numero_medidor LIKE ?', palabraBuscada)
		end
		respond_with @ordenRepartos
	end

  def carga_ubicacion
    ubicaciones = Array.new
    ordenReparto = OrdenReparto.find(params[:ordenId])
    latitud = ordenReparto.gps_latitud
    longitud = ordenReparto.gps_longitud
    ubicaciones.push([latitud.to_s, longitud.to_s, ordenReparto.cliente.numero_cliente])
    @hash = Gmaps4rails.build_markers(ubicaciones) do |u, marker|
      marker.lat u[0]
      marker.lng u[1]
      marker.infowindow u[2]
    end
    respond_with @hash
  end
end
