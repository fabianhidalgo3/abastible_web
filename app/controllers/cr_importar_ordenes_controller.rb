class CrImportarOrdenesController < ApplicationController

  respond_to :js, :html, :json
  require 'csv'

  # @tipoLecturas > Normal / Verificación
  def index
  end

  # POST
  # IMPORT CSV
  def import_csv
    csv = params[:file]
    #ImportarOrdenesLecturaWorker.perform_async(csv.path)

    row = Array.new
     # Arreglo de columnas para la carga
     #Crear Hilo de Ejecución
     CSV.foreach((csv.path), headers: true) do |row|
       estadoCorte = 1 # Estado de Lectura Sin Asignar
       # Validación de campos de carga masiva.
       secuencia_lector = row[0]
       secuencia_lector = "" if secuencia_lector.nil?
       nombre_ruta = row[1]
       nombre_ruta = "" if nombre_ruta.nil?
       nombre_porcion = row[1][0..4]
       nombre_porcion = "" if nombre_porcion.nil?
       codigo_zonal = row[2]
       codigo_zonal = "" if codigo_zonal.nil?
       nombre_comuna = row[3]
       nombre_comuna = "" if nombre_comuna.nil?
       calle = row[4]
       calle = "" if calle.nil?
       numero_domicilio = row[5]
       numero_domicilio = "" if numero_domicilio.nil?
       ubicacion_medidor = row[11]
       ubicacion_medidor = "" if ubicacion_medidor.nil?
       numero_cliente = row[6]
       numero_cliente = "Error" if numero_cliente.nil?
       numero_medidor = row[7]
       numero_medidor = "Error" if numero_medidor.nil?
       nombre_marca_medidor = row[8]
       nombre_marca_medidor = "" if nombre_marca_medidor.nil?
       nombre_modelo_medidor = row[9]
       nombre_modelo_medidor = "" if nombre_modelo_medidor.nil?
       numero_instalacion = row[10]
       numero_instalacion = "Error" if numero_instalacion.nil?
       deuda_total = row[12]
       deuda_total = "" if deuda_total.nil?
       p deuda_total
       morosidad = row[13]
       morosidad = "" if morosidad.nil?

       if row[14] == "CORTE"
          tipoCorteReposicion = 1
       end
       if row[14] == "REPOSICION"
        tipoCorteReposicion = 2
       end

       p row[14]
       p tipoCorteReposicion


       if (numero_instalacion != "Error") || (numero_cliente != "Error")
         # Cambiar Región Por defecto
         zona = Zona.where(codigo: codigo_zonal).first
         zona = Zona.create(codigo: codigo_zonal, region_id: 10) if zona.nil?
         #Busco Comunas Precargadas
         comuna = Comuna.where(nombre: nombre_comuna).first
         # Si no existe comuna vinculada la creo
         comuna = Comuna.create(nombre: nombre_comuna, zona_id: zona.id) if comuna.nil?
         # Busco la porcion
         porcion = PorcionCorteReposicion.where(codigo:nombre_porcion, mes: 10, ano: Time.now.year, zona_id: zona.id).first
         # Si no existe la creo
         porcion = PorcionCorteReposicion.create(codigo: nombre_porcion, mes: 10, ano: Time.now.year, zona_id: zona.id, abierto:true) if porcion.nil?
         # Busco la ruta
         ruta = RutaCorteReposicion.where(codigo: nombre_ruta, mes: 10, ano: Time.now.year).first
         # Si no existe lo creo
         ruta = RutaCorteReposicion.create(codigo: nombre_ruta, mes: 10, ano: Time.now.year, abierto:true, porcion_corte_reposicion_id: porcion.id) if ruta.nil?
         # Busco el cliente
         cliente = Cliente.where(numero_cliente: numero_cliente).first if numero_cliente != "0000"
         # Si No Existe lo Creo
         cliente = Cliente.create(numero_cliente: numero_cliente, direccion_completa: calle + " " + numero_domicilio, calle: calle, numero_domicilio: numero_domicilio) if cliente.nil?
         # Busco Instalación
         instalacion = Instalacion.where(codigo: numero_instalacion).first if numero_instalacion != "0000"
         # Si no Existe la creo
         instalacion = Instalacion.create(codigo: numero_instalacion) if instalacion.nil?
         # Busco Marca de Medidor
         marca_medidor = MarcaMedidor.where(nombre: nombre_marca_medidor).first
         # Si no la encuentro la creo
         marca_medidor = MarcaMedidor.create(nombre: nombre_marca_medidor)  if marca_medidor.nil?
         # Busco Modelo Medidor
         modelo_medidor = ModeloMedidor.where(nombre: nombre_modelo_medidor, marca_medidor_id: marca_medidor.id).first
         # Si no lo encuentr lo creo
         modelo_medidor = ModeloMedidor.create(nombre: nombre_modelo_medidor,marca_medidor_id: marca_medidor.id) if modelo_medidor.nil?
         # Creo el Medidor
         medidor = Medidor.where(numero_medidor: numero_medidor, modelo_medidor_id: modelo_medidor.id, ubicacion_medidor: ubicacion_medidor).first if numero_medidor != "0000"
         # Si no existe lo creo
         medidor = Medidor.create(numero_medidor: numero_medidor, modelo_medidor_id: modelo_medidor.id, ubicacion_medidor: ubicacion_medidor) if medidor.nil?
         #Creo el Correlativo de los Códigos de las Ordenes de Lectura
         codigoOrden = OrdenCorteReposicion.last
         if codigoOrden.nil?
           codigoOrden = 1
           codigoOrden = "CR-"+ codigoOrden.to_s
         else
         codigoOrden = codigoOrden.id
         codigoOrden = codigoOrden + 1
         codigoOrden = "CR-" + codigoOrden.to_s
         end
         #Valido si la orden Lectura Existe
         ordenLectura = OrdenCorteReposicion.where(codigo: codigoOrden).first
         # Si no Existe la creo junto con el detalle_orden_lectura
         if ordenLectura.nil?
           ordenLectura = OrdenCorteReposicion.create(
              codigo: codigoOrden,
              secuencia: secuencia_lector,
              instalacion_id: instalacion.id,
              cliente_id:cliente.id,
              ruta_corte_reposicion_id: ruta.id,
              tipo_corte_reposicion_id: tipoCorteReposicion,
              estado_corte_reposicion_id: estadoCorte,
              proceso_corte_reposicion_id: 1,
              fecha_carga: Time.now,
              direccion: calle + " " + numero_domicilio ,
              verificacion: false,
              comuna_id: comuna.id,
              medidor_id: medidor.id,
              deuda_total: deuda_total.to_s,
              morosidad: morosidad,
              numero_medidor: medidor.numero_medidor,
              numero_cliente: cliente.numero_cliente
            )
         end
       end
    end
  end

end
