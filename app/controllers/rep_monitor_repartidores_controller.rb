class RepMonitorRepartidoresController < ApplicationController
  respond_to :js, :html, :json

	def index
		user = current_usuario
    zonas = user.empleado.zona
    @regiones = Region.joins(:zona).where(zonas:{id: zonas}).group(:id)
    @tipoLecturas = TipoLectura.all
  end

  # GET
  # Retorna lista con zonas
  def carga_zonas
    user = current_usuario
    empleado = user.empleado
    @zonas = Zona.where(region_id: params[:region]).joins(:empleado).where(empleados: {id: empleado.id})
    respond_with @zonas
  end

  # GET
  # Retorna objeto con @porciones
  def carga_porciones
    @porciones = PorcionReparto.where(zona_id: params[:zona], abierto: true).order(:codigo)
    respond_with @porciones
  end

  # GET
  # Retorna lista con empleados
  def carga_lectores
    @lectores = Empleado.joins(:usuario, :zona).where(usuarios:{perfil_id: [6,7]}, zonas:{id: params[:zona]})
    respond_with @lectores
  end

  #GET
  # Devuelve @hash con ubicaciones
  def carga_ubicacion
    ubicaciones = Array.new
    detalles = OrdenReparto.where("orden_repartos.fecha_ejecucion IS NOT NULL").joins(:asignacion_reparto).where(orden_repartos: {ruta_reparto_id: params[:ruta_id], estado_reparto_id: [4,5]},asignacion_repartos: {empleado_id: params[:emp_id]}).order("orden_repartos.fecha_ejecucion ASC")
    p detalles
      detalles.each do |detalle|
        p detalle.gps_latitud
        p detalle.gps_longitud
        fecha_ejecucion = detalle.fecha_ejecucion
        ventana = "N Cliente: " + detalle.cliente.numero_cliente.to_s + "\n" +
        detalle.fecha_ejecucion.strftime('%e/%m/%Y %I:%M:%S %p')
        ubicaciones.push([detalle.gps_latitud, detalle.gps_longitud, ventana])
      end
    @hash = Gmaps4rails.build_markers(ubicaciones) do |u, marker|
      marker.lat u[0]
      marker.lng u[1]
      marker.infowindow u[2]
    end
    respond_with @hash
  end

  #GET
  # Contenido de la tabla
  def carga_filtro
    lista = Array.new
    @totales = Array.new
    tAsignado = 0, tCargado = 0, tPendiente = 0, tEfectivo = 0, tNoEfectivo = 0, tLeido = 0, tVerificaciones = 0, tEfectividad = 0
    asignado = 0, cargado = 0, efectivo = 0, noEfectivo = 0, pendientes = 0, leido = 0, ol_repaso = 0, efectividad = 0
    contador = 0
    rutas = RutaReparto.where(porcion_reparto_id: params[:porcion])
    if rutas.blank?
      rutas = RutaReparto.joins(:porcion_reparto).where(porcion_repartos: {zona_id: params[:zona], abierto:true})
    end
    p rutas
    empleados = Empleado.joins(:usuario, :asignacion_reparto).where(usuarios: {perfil_id: 5}, asignacion_repartos: {ruta_reparto_id: rutas}).group(:id)
    p empleados
    empleados.each do |empleado|
      asignaciones = AsignacionReparto.where(empleado_id: empleado.id).joins(:orden_reparto).where(orden_repartos:{estado_reparto_id: [2..5], ruta_reparto_id: rutas}).group(:ruta_reparto_id)
      p asignaciones
      asignaciones.each do |asignacion|
        asignado = AsignacionReparto.where(empleado_id: asignacion.empleado_id, ruta_reparto_id: asignacion.ruta_reparto_id).joins(:orden_reparto).where(orden_repartos: {estado_reparto_id: [2..5],}).count
        cargado = AsignacionReparto.where(empleado_id: asignacion.empleado_id, ruta_reparto_id: asignacion.ruta_reparto_id).joins(:orden_reparto).where(orden_repartos: {estado_reparto_id: [3], }).count
        pendientes = AsignacionReparto.where(empleado_id: asignacion.empleado_id, ruta_reparto_id: asignacion.ruta_reparto_id).joins(:orden_reparto).where(orden_repartos: {estado_reparto_id: [2,3],}).count
        leido = AsignacionReparto.where(empleado_id: asignacion.empleado_id, ruta_reparto_id: asignacion.ruta_reparto_id).joins(:orden_reparto).where(orden_repartos: {estado_reparto_id: [4..5],}).count
        hora = OrdenReparto.where(ruta_reparto_id: asignacion.ruta_reparto_id, estado_reparto_id: [4..5]).joins(:asignacion_reparto).where(asignacion_repartos: {empleado_id: asignacion.empleado_id}).order("orden_repartos.fecha_ejecucion ASC")
        efectividad = ((leido*100)/asignado).round(2)
        if !hora.blank? then
          horaInicio = hora.first.fecha_ejecucion.strftime("%H:%M:%S %d/%m/%Y")
          horaTermino = hora.last.fecha_ejecucion.strftime("%H:%M:%S %d/%m/%Y")
        else
          horaInicio = " "
          horaTermino = " "
        end
        transmitido = AsignacionReparto.where(empleado_id: asignacion.empleado_id, ruta_reparto_id: asignacion.ruta_reparto_id).joins(:orden_reparto).where(orden_repartos: {estado_reparto_id: 5}).count

        lista.push([empleado.nombre_completo,
                   asignacion.orden_reparto.ruta_reparto.porcion_reparto.codigo,
                   asignacion.orden_reparto.ruta_reparto.codigo,
                   asignado,
                   cargado,
                   pendientes,
                   leido,
                   efectividad,
                   horaInicio,
                   horaTermino,
                   asignacion.ruta_reparto_id,
                   asignacion.empleado_id,
                   ])
      asignado = 0, cargado = 0, efectivo = 0, noEfectivo = 0, pendientes = 0, leido = 0, ol_repaso = 0, efectividad = 0
      end

    end
    lista.sort! {|a,b| a[1] <=> b[1]}
		@lista = lista.paginate(:page => params[:page], :per_page => 7)
    respond_with @lista
  end
end
