class RepReporteControlCoordenadasController < ApplicationController
	respond_to :js, :html, :json, :pdf
	# => Control de Coordenadas GPS
	def index
		# Busca usuario y zonas y porcion del usuario logueado
		user = current_usuario
		zonas = user.empleado.zona
		@regiones = Region.joins(:zona).where(zonas:{id: zonas}).group(:id)
	end

	def carga_zonas
		user = current_usuario
    @zonas = user.empleado.zona.where(region_id: params[:region]) if !params[:region].blank?
  end

  # GET
  # PARCIAL: Muestra las porciones
	def carga_porciones
    @porciones = PorcionReparto.where(zona_id: params[:zona], abierto: true).order(:codigo)
    respond_with @porciones
	end

	# GET
  # PARCIAL: Muestra las Rutas
  def carga_rutas
    @rutas = RutaReparto.where(porcion_reparto_id: params[:porcion]).order(:codigo)
    respond_with @rutas
	end


	# > GET
	# > Tabla contenido
	def carga_filtro
		rutas = Rutum.find(params[:ruta])
		asignaciones = AsignacionReparto.joins(:ruta_reparto, :orden_reparto).where(ruta_repartos: {id: rutas },orden_repartos:{estado_reparto_id: [4..5]} )
		@listaOrdenes = asignaciones.paginate(:page => params[:page], :per_page => 10)
		p @listaOrdenes
		respond_with @listaOrdenes
	end

	# GET
	# Devuelve @hash con coordenadas GPS
	def carga_ubicacion
		ubicaciones = Array.new
	  latitud = params[:latitud]
	  longitud = params[:longitud]
	  ubicaciones.push([latitud.to_s, longitud.to_s, ""])
	  @hash = Gmaps4rails.build_markers(ubicaciones) do |u, marker|
	    marker.lat u[0]
	    marker.lng u[1]
	    marker.infowindow u[2]
		end
		respond_with @hash
	end

	# Exportar Csv
	def exportar_csv
		rutas = Rutum.find(params[:ruta])
		listaRsiete = AsignacionLectura.joins(:rutum, :orden_lectura).where(ruta: {id: rutas },orden_lecturas:{estado_lectura_id: [4..5], verificacion: false} )
		header = "Region,Secuencia, Comuna, Lector, Ruta,Direccion, Numero Cliente,Medidor ,Fecha Lectura, Hora Lectura,Clave Lectura, Latitud, Longitud \n"
    	fecha = Time.now
    	file = "control_gps - " +  fecha.to_s + ".csv"
    	File.open(file, "w+:UTF-16LE:UTF-8") do |csv|
     		csv << header
      		listaRsiete.each do |l|
						csv << l.orden_lectura.rutum.porcion.zona.region.nombre + "," +
									 l.orden_lectura.secuencia_lector.to_s + "," +
									 l.orden_lectura.comuna.nombre + "," +
									 l.empleado.nombre_completo + "," +
									 l.rutum.codigo + "," +
									 l.orden_lectura.direccion + "," +
									 l.orden_lectura.cliente.numero_cliente.to_s + "," +
									 l.orden_lectura.medidor.numero_medidor.to_s + "," +
									 l.orden_lectura.detalle_orden_lectura.first.fecha_ejecucion.strftime("%e/%m/%Y ") + "," +
									 l.orden_lectura.detalle_orden_lectura.first.fecha_ejecucion.strftime("%H:%M:%S")+ "," +
									 l.orden_lectura.detalle_orden_lectura.first.clave_lectura.nombre + "," +
									 l.orden_lectura.gps_latitud.to_s+ "," +
									 l.orden_lectura.gps_longitud.to_s+ "\n"
      		end
    	end
    	send_file(file, x_sendfile: true, buffer_size: 512)
	end


end
