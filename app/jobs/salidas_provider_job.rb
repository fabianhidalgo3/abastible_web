class SalidasProviderJob < ApplicationJob
  queue_as :default

  def perform(export_id, region, zona, porcion, ruta, empleado, clave)
    csv_content = ExportarOrdenesCommand.exportar(region, zona, porcion, ruta, empleado, clave)
    p csv_content
    ActionCable.server.broadcast(
      "export_channel_#{export_id}",
      csv_file: {
        file_name: 'improcedencias-' + Time.zone.now.to_s + '.csv',
        content: csv_content
      }
    )
  end
end
