require 'csv'

class ExportarOrdenesCommand
  
  def self.exportar(region, zona, porcion, ruta, empleado, clave)
    clave = clave.to_i
    region_id = region.to_i
    zona_id = zona.to_i
    porcion = porcion.to_i
    rutas = ruta.to_i
    lector = lector.to_i
    # Rescato Variables de la Vista
    if region_id != 0 && zona_id == 0
      zonas = Zona.where(region_id: region_id)
    else
      zonas = Zona.find(zona_id)
    end

    # Busco Porciones Seleccionadas
    if porcion == 0 && rutas == 0
      porciones = Porcion.where(abierto:true, zona_id: zonas)
      rutas = Rutum.where(porcion_id: porciones)
    end
    if porcion !=0 && rutas == 0
      rutas = Rutum.where(porcion_id: porcion)
    end

    if porcion != 0  && rutas != 0
      rutas = rutas
    end

    if lector != 0
      empleados = Empleado.find(lector)
    else
      empleados = Empleado.joins(:usuario, :zona).where(usuarios:{perfil_id: 5}, zonas:{id: zonas})
    end

    if clave != 0
      claves = ClaveLectura.find(clave)
    else
      claves = ClaveLectura.where(efectivo: false)
    end
    ordenes = OrdenLectura.where(rutum_id: rutas, verificacion: false, estado_lectura_id:[4,5]).joins(:asignacion_lectura, :detalle_orden_lectura).where(asignacion_lecturas:{empleado_id: empleados},detalle_orden_lecturas: {clave_lectura_id: claves}).order("detalle_orden_lecturas.fecha_ejecucion DESC")
   # p ordenes
    file = CSV.generate do |csv|
      csv << %w[cliente medidor direccion ruta consumo_minimo consumo_maximo lectura_anterior lectura_actual proceso clave_lectura lector fecha_ejecucion]
       ordenes.each do |orden|
         csv << [orden.cliente.numero_cliente.to_s, orden.medidor.numero_medidor.to_s, orden.cliente.direccion_completa.to_s,orden.rutum.codigo.to_s, orden.detalle_orden_lectura.first.consumo_minimo.to_s,orden.detalle_orden_lectura.first.consumo_maximo.to_s, orden.detalle_orden_lectura.first.lectura_anterior.to_s, orden.detalle_orden_lectura.first.lectura_actual.to_s, orden.tipo_lectura.nombre, orden.detalle_orden_lectura.first.clave_lectura.nombre, orden.asignacion_lectura.empleado.nombre, orden.detalle_orden_lectura.first.fecha_ejecucion.to_s]
      #  orden.tipo_lectura.nombre, orden.detalle_orden_lectura.first.clave_lectura.nombre, orden.asignacion.empleado.nombre
        end
    end
    file
end


end