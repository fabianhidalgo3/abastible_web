# == Schema Information
#
# Table name: modelos
#
#  id              :integer          not null, primary key
#  nombre          :string(255)
#  caracteristicas :text(65535)
#  marca_id        :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
class Modelo < ApplicationRecord
  belongs_to :marca
end
