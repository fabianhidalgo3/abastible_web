# == Schema Information
#
# Table name: contratista
#
#  id         :integer          not null, primary key
#  nombre     :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Contratistum < ApplicationRecord
	has_and_belongs_to_many :subempresa
	has_many :orden_lectura
end
