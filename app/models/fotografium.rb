# == Schema Information
#
# Table name: fotografia
#
#  id                       :integer          not null, primary key
#  detalle_orden_lectura_id :integer
#  archivo                  :string(255)
#  descripcion              :string(255)
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  orden_lectura_id         :integer
#
class Fotografium < ApplicationRecord
  belongs_to :detalle_orden_lectura
  belongs_to :orden_lectura
  validates :archivo, uniqueness: true
end
