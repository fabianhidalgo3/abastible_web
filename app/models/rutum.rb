# == Schema Information
#
# Table name: ruta
#
#  id                :integer          not null, primary key
#  codigo            :string(255)
#  nombre            :string(255)
#  mes               :integer
#  ano               :integer
#  abierto           :boolean
#  porcion_id        :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  cantidad_lecturas :integer
#
class Rutum < ApplicationRecord
  belongs_to :porcion
  has_many :orden_lectura
  has_many :asignacion_lectura

  def nombre
    if self[:nombre].nil?
      ""
    else
      self[:nombre]
    end
  end

end
