# == Schema Information
#
# Table name: ruta_cambio_medidors
#
#  id                        :integer          not null, primary key
#  codigo                    :string(255)
#  nombre                    :string(255)
#  mes                       :integer
#  ano                       :integer
#  abierto                   :boolean
#  porcion_cambio_medidor_id :integer
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#
class RutaCambioMedidor < ApplicationRecord
end
