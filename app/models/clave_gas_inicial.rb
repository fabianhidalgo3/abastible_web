# == Schema Information
#
# Table name: clave_gas_inicials
#
#  id                 :integer          not null, primary key
#  codigo             :string(255)
#  nombre             :string(255)
#  requerido          :boolean
#  efectivo           :boolean
#  activo             :boolean
#  numero_fotografias :integer
#  secuencia          :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
class ClaveGasInicial < ApplicationRecord
end
