# == Schema Information
#
# Table name: estado_convenios
#
#  id         :integer          not null, primary key
#  codigo     :string(255)
#  nombre     :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class EstadoConvenio < ApplicationRecord
end
