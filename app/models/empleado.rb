# == Schema Information
#
# Table name: empleados
#
#  id               :integer          not null, primary key
#  nombre           :string(255)
#  apellido_paterno :string(255)
#  apellido_materno :string(255)
#  rut              :string(255)
#  contratista_id   :integer
#  empresa_id       :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  usuario_id       :integer
#  codigo           :string(255)
#
class Empleado < ApplicationRecord
  belongs_to :contratistum
  belongs_to :empresa
  has_many  :orden_lectura
  belongs_to  :tipo_tarifa
  belongs_to  :sector
  belongs_to  :tipo_sector
	has_many :asignacion_lectura
	has_many :asignacion_reparto
  has_and_belongs_to_many :zona
  has_one :equipo
  belongs_to :usuario

	validates :rut, uniqueness: { message: "Rut ya registrado."}

  def nombre_completo
    # Valido que exista el nombre
    if nombre.nil?
      @nombre_completo = apellido_paterno + " " + apellido_materno
    end
    # Valido que exitan los apellidos
    if apellido_paterno.nil? && apellido_materno.nil?
      @nombre_completo = nombre
    else 
      @nombre_completo = nombre + " " + apellido_paterno + " " + apellido_materno
    end
  end

  def usuario_nombre
    if !usuario.nil?
      @usuario_nombre = usuario.email + " | " + nombre_completo
    else 
      @usuario_nombre = nombre
    end
  end
end
