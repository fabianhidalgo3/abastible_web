# == Schema Information
#
# Table name: instalacions
#
#  id          :integer          not null, primary key
#  codigo      :string(255)
#  descripcion :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class Instalacion < ApplicationRecord
  has_many :orden_lectura
end
