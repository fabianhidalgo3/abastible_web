# == Schema Information
#
# Table name: fotografia_convenios
#
#  id                :integer          not null, primary key
#  orden_convenio_id :integer
#  archivo           :string(255)
#  descripcion       :string(255)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
class FotografiaConvenio < ApplicationRecord
end
