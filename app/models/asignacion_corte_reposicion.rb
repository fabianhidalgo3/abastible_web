# == Schema Information
#
# Table name: asignacion_corte_reposicions
#
#  id                        :integer          not null, primary key
#  ruta_corte_reposicion_id  :integer
#  orden_corte_reposicion_id :integer
#  empleado_id               :integer
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#
class AsignacionCorteReposicion < ApplicationRecord
  belongs_to :ruta_corte_reposicion
  belongs_to :orden_corte_reposicion
  belongs_to :empleado
end
