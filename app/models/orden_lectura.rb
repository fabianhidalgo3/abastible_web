# == Schema Information
#
# Table name: orden_lecturas
#
#  id                       :integer          not null, primary key
#  codigo                   :string(255)
#  secuencia_lector         :integer
#  direccion                :string(255)
#  direccion_entrega        :string(255)
#  numero_poste             :string(255)
#  observacion_lector       :string(255)
#  fecha_carga              :datetime
#  fecha_propuesta          :datetime
#  fecha_asignacion         :datetime
#  gps_latitud              :decimal(10, 7)
#  gps_longitud             :decimal(10, 7)
#  ajuste_sencillo_anterior :decimal(6, 2)
#  ajuste_sencillo_actual   :decimal(6, 2)
#  verificacion             :boolean
#  modulo_analisis          :integer
#  aprobado                 :boolean
#  observacion_analista     :string(255)
#  color                    :integer
#  medidor_id               :integer
#  instalacion_id           :integer
#  cliente_id               :integer
#  rutum_id                 :integer
#  tipo_lectura_id          :integer
#  estado_lectura_id        :integer
#  tarifa_id                :integer
#  comuna_id                :integer
#  contratista_id           :integer
#  carga_sistema_id         :integer
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  lectura_anterior         :decimal(10, )
#  lectura_actual           :decimal(10, )
#  lectura_modificada       :decimal(10, )
#  lectura_dictada          :decimal(10, )
#  fecha_ejecucion          :datetime
#  consumo_minimo           :decimal(10, )
#  consumo_maximo           :decimal(10, )
#  consumo_promedio         :decimal(10, )
#  consumo_actual           :decimal(10, )
#  clave_lectura_id         :integer
#  numero_medidor           :string(255)
#
class OrdenLectura < ApplicationRecord
  belongs_to :instalacion
  belongs_to :cliente
  belongs_to :contratistum
  belongs_to :rutum
  belongs_to :tipo_lectura
  belongs_to :estado_lectura
  belongs_to :tarifa
  belongs_to :porcion
  belongs_to :comuna
  belongs_to :empleado
  has_one :asignacion_lectura
  belongs_to :clave_lectura
  belongs_to :medidor
  has_many :fotografium
  #enum modulo_analisis: [consumo_cero:0, consumo_inferior:2, consumo_maximo:3, improcedencia:4]

  def direccion_completa
    @direccion_orden = direccion.to_s + " " + block.to_s + " " + numero_propieda.to_s + " " + numero_departamento.to_s
  end
end
