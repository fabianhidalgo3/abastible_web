# == Schema Information
#
# Table name: fotografia_cambio_medidors
#
#  id                      :integer          not null, primary key
#  orden_cambio_medidor_id :integer
#  archivo                 :string(255)
#  descripcion             :string(255)
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#
class FotografiaCambioMedidor < ApplicationRecord
end
