# == Schema Information
#
# Table name: orden_corte_reposicions
#
#  id                          :integer          not null, primary key
#  codigo                      :string(255)
#  secuencia                   :integer
#  direccion                   :string(255)
#  calle                       :string(255)
#  numero                      :integer
#  fecha_carga                 :datetime
#  fecha_propuesta             :datetime
#  fecha_asignacion            :datetime
#  gps_latitud                 :decimal(10, 7)
#  gps_longitud                :decimal(10, 7)
#  verificacion                :boolean
#  modulo_analisis             :integer
#  aprobado                    :boolean
#  observacion_analista        :string(255)
#  deuda_total                 :decimal(10, )
#  color                       :integer
#  lectura_encontrada          :string(255)
#  observacion_tecnico         :string(255)
#  fecha_ejecucion             :datetime
#  medidor_id                  :integer
#  instalacion_id              :integer
#  cliente_id                  :integer
#  ruta_corte_reposicion_id    :integer
#  tipo_corte_reposicion_id    :integer
#  proceso_corte_reposicion_id :integer
#  estado_corte_reposicion_id  :integer
#  comuna_id                   :integer
#  clave_corte_reposicion_id   :integer
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  tipo_corte                  :integer
#  numero_medidor              :text(65535)
#  numero_cliente              :text(65535)
#  morosidad                   :integer
#
class OrdenCorteReposicion < ApplicationRecord
  has_one :asignacion_corte_reposicion
  belongs_to :tipo_corte_reposicion
  belongs_to :proceso_corte_reposicion
  belongs_to :comuna
end
