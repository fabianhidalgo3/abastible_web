# == Schema Information
#
# Table name: modelo_medidors
#
#  id               :integer          not null, primary key
#  codigo           :string(255)
#  nombre           :string(255)
#  marca_medidor_id :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
class ModeloMedidor < ApplicationRecord
  belongs_to :marca_medidor
  has_many :medidor
end
