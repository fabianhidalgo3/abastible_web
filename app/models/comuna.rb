# == Schema Information
#
# Table name: comunas
#
#  id               :integer          not null, primary key
#  codigo           :string(255)
#  codigo_sii       :string(255)
#  codigo_tesoreria :string(255)
#  nombre           :string(255)
#  provincium_id    :integer
#  zona_id          :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
class Comuna < ApplicationRecord
	belongs_to :provincium
	has_many :orden_lectura
	has_one :tarifa
  
  def codigo_nombre
    if nombre.nil?
      @codigo_nombre = codigo
    else
      @codigo_nombre = codigo + " - " + nombre 
    end
  end

end
