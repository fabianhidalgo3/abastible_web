# == Schema Information
#
# Table name: asignacion_lecturas
#
#  id               :integer          not null, primary key
#  rutum_id         :integer
#  orden_lectura_id :integer
#  empleado_id      :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
class AsignacionLectura < ApplicationRecord
  belongs_to :rutum
  belongs_to :orden_lectura
  belongs_to :empleado
end
 
