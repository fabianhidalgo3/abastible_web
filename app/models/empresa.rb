# == Schema Information
#
# Table name: empresas
#
#  id           :integer          not null, primary key
#  rut          :string(255)
#  razon_social :string(255)
#  giro         :string(255)
#  direccion    :string(255)
#  comuna_id    :integer
#  imagen       :string(255)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
class Empresa < ApplicationRecord
  belongs_to :comuna
  has_many :empleado
end
