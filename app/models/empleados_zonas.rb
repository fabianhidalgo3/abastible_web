# == Schema Information
#
# Table name: empleados_zonas
#
#  id          :integer          not null, primary key
#  empleado_id :integer
#  zona_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class EmpleadosZonas < ApplicationRecord
  belongs_to :empleado
  belongs_to :zona
end
