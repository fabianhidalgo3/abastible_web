# == Schema Information
#
# Table name: orden_cambio_medidors
#
#  id                        :integer          not null, primary key
#  codigo                    :string(255)
#  secuencia                 :integer
#  direccion                 :string(255)
#  calle                     :string(255)
#  numero                    :integer
#  modulo_analisis           :integer
#  color                     :integer
#  fecha_carga               :datetime
#  fecha_propuesta           :datetime
#  fecha_asignacion          :datetime
#  gps_latitud               :decimal(10, 7)
#  gps_longitud              :decimal(10, 7)
#  verificacion              :boolean
#  aprobado                  :boolean
#  observacion_analista      :string(255)
#  lectura_encontrada        :string(255)
#  numero_nuevo_medidor      :string(255)
#  lectura_nuevo_medidor     :string(255)
#  nombre_cliente            :string(255)
#  rut_ciente                :string(255)
#  telefono_cliente          :string(255)
#  correo_cliente            :string(255)
#  observacion_tecnico       :string(255)
#  fecha_ejecucion           :datetime
#  medidor_id                :integer
#  instalacion_id            :integer
#  cliente_id                :integer
#  ruta_cambio_medidor_id    :integer
#  proceso_cambio_medidor_id :integer
#  estado_cambio_medidor_id  :integer
#  clave_cambio_medidore_id  :integer
#  comuna_id                 :integer
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  numero_cliente            :string(255)
#  numero_medidor            :string(255)
#
class OrdenCambioMedidor < ApplicationRecord
end
