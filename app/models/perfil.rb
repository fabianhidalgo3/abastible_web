# == Schema Information
#
# Table name: perfils
#
#  id         :integer          not null, primary key
#  nombre     :string(255)
#  imagen     :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Perfil < ApplicationRecord
	has_many :usuario
end
