# == Schema Information
#
# Table name: clave_lecturas
#
#  id                 :integer          not null, primary key
#  codigo             :string(255)
#  nombre             :string(255)
#  requerido          :boolean
#  efectivo           :boolean
#  factura            :boolean
#  numero_fotografias :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  secuencia          :integer
#  activo             :boolean
#
class ClaveLectura < ApplicationRecord
  belongs_to :tipo_cobro
  has_many	:orden_lectura
end
