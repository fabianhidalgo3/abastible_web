# == Schema Information
#
# Table name: ruta_corte_reposicions
#
#  id                          :integer          not null, primary key
#  codigo                      :string(255)
#  nombre                      :string(255)
#  mes                         :integer
#  ano                         :integer
#  abierto                     :boolean
#  porcion_corte_reposicion_id :integer
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#
class RutaCorteReposicion < ApplicationRecord
  belongs_to :porcion_corte_reposicion
  has_many :orden_corte_reposicion
  has_many :asignacion_corte_reposicion

end
