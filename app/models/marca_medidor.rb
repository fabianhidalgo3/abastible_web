# == Schema Information
#
# Table name: marca_medidors
#
#  id         :integer          not null, primary key
#  codigo     :string(255)
#  nombre     :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class MarcaMedidor < ApplicationRecord
end
