# == Schema Information
#
# Table name: asignacion_modificacion_lecturas
#
#  id                 :integer          not null, primary key
#  empleado_id        :integer
#  orden_lectura_id   :integer
#  lectura_actual     :decimal(10, )
#  clave_lectura_id   :integer
#  fecha_modificacion :datetime
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  rutum_id           :integer
#
class AsignacionModificacionLectura < ApplicationRecord
  belongs_to :empleado
  belongs_to :orden_lectura
  belongs_to :clave_lectura
end
