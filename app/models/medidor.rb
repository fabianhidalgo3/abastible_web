# == Schema Information
#
# Table name: medidors
#
#  id                :integer          not null, primary key
#  numero_medidor    :string(255)
#  numero_digitos    :integer
#  diametro          :integer
#  ano               :integer
#  ubicacion_medidor :string(255)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  modelo_medidor_id :integer
#
class Medidor < ApplicationRecord
  belongs_to :modelo_medidor

  def propiedad_cliente
    if self[:propiedad_cliente].nil?
      false
    else
      self[:propiedad_cliente]
    end
  end

  def diametro
    if self[:diametro].nil?
      0
    else
      self[:diametro]
    end
  end
  
  def nro_digitos
    if self[:nro_digitos].nil?
      0
    else
      self[:nro_digitos]
    end
  end
end
