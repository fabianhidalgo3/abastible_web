# == Schema Information
#
# Table name: porcions
#
#  id         :integer          not null, primary key
#  codigo     :string(255)
#  nombre     :string(255)
#  mes        :integer
#  ano        :integer
#  abierto    :boolean
#  zona_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Porcion < ApplicationRecord
  belongs_to :zona
  belongs_to :subempresa
  has_many :rutum
  has_many :asignacion
  has_many :orden_lectura


  def codigo_nombre
    if zona_id.nil?
      @codigo_nombre = codigo
    else  
      @codigo_nombre = codigo + " - " +  mes.to_s + "-" + ano.to_s
    end
  end
end
