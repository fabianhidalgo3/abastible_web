# == Schema Information
#
# Table name: ruta_convenios
#
#  id                  :integer          not null, primary key
#  codigo              :string(255)
#  nombre              :string(255)
#  mes                 :integer
#  ano                 :integer
#  abierto             :boolean
#  porcion_convenio_id :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#
class RutaConvenio < ApplicationRecord
  belongs_to :porcion_convenio
end
