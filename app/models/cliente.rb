# == Schema Information
#
# Table name: clientes
#
#  id                    :integer          not null, primary key
#  numero_cliente        :integer
#  rut                   :string(255)
#  nombre                :string(255)
#  apellido_paterno      :string(255)
#  apellido_materno      :string(255)
#  calle                 :string(255)
#  numero_domicilio      :string(255)
#  direccion_completa    :string(255)
#  gps_latitud           :decimal(10, 7)
#  gps_longitud          :decimal(10, 7)
#  observacion_domicilio :string(255)
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  empresa_id            :integer
#  despacho_postal       :boolean
#  factura_electronica   :boolean
#  secuencia             :integer
#
class Cliente < ApplicationRecord
	has_many :orden_lectura

	def apellido_paterno
    if self[:apellido_paterno].nil?
      ""
    else
      self[:apellido_paterno]
    end
  end

  def apellido_materno
    if self[:apellido_materno].nil?
      ""
    else
      self[:apellido_materno]
    end
  end

  def giro
    if self[:giro].nil?
      ""
    else
      self[:giro]
    end
  end

  def telefono
    if self[:telefono].nil?
      ""
    else
      self[:telefono]
    end
  end

  def memo
    if self[:memo].nil?
      ""
    else
      self[:memo]
    end
  end

  def duenorespo
    if self[:duenorespo].nil?
      ""
    else
      self[:duenorespo]
    end
  end

end
