# == Schema Information
#
# Table name: porcion_convenios
#
#  id         :integer          not null, primary key
#  codigo     :string(255)
#  nombre     :string(255)
#  mes        :integer
#  ano        :integer
#  abierto    :boolean
#  zona_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class PorcionConvenio < ApplicationRecord
  belongs_to :zona
end
