# == Schema Information
#
# Table name: equipos
#
#  id            :integer          not null, primary key
#  nombre        :string(255)
#  mac           :string(255)
#  modelo_id     :integer
#  empleado_id   :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  quick_support :string(255)
#
class Equipo < ApplicationRecord
  belongs_to :modelo
  belongs_to :empleado

	validates :mac, uniqueness: {message: "Equipo ya registrado."}
end
