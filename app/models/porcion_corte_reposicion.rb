# == Schema Information
#
# Table name: porcion_corte_reposicions
#
#  id         :integer          not null, primary key
#  codigo     :string(255)
#  nombre     :string(255)
#  mes        :integer
#  ano        :integer
#  abierto    :boolean
#  zona_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class PorcionCorteReposicion < ApplicationRecord
  has_many :ruta_corte_reposicion
end
