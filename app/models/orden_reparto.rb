# == Schema Information
#
# Table name: orden_repartos
#
#  id                :integer          not null, primary key
#  codigo            :string(255)
#  domicilio         :string(255)
#  fecha_carga       :datetime
#  fecha_asignacion  :datetime
#  fecha_ejecucion   :datetime
#  gps_latitud       :decimal(10, 7)
#  gps_longitud      :decimal(10, 7)
#  estado_reparto_id :integer
#  tipo_documento_id :integer
#  cliente_id        :integer
#  medidor_id        :integer
#  instalacion_id    :integer
#  secuencia_lector  :integer
#  direccion_entrega :string(255)
#  ruta_reparto_id   :integer
#  comuna_id         :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  numero_cliente    :string(255)
#
class OrdenReparto < ApplicationRecord
  belongs_to :estado_reparto
  belongs_to :tipo_reparto
  belongs_to :cliente
  belongs_to :medidor
  belongs_to :instalacion
  belongs_to :ruta_reparto
  belongs_to :comuna
  has_one :asignacion_reparto
end
