# == Schema Information
#
# Table name: provincia
#
#  id         :integer          not null, primary key
#  codigo     :string(255)
#  nombre     :string(255)
#  region_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Provincium < ApplicationRecord
	has_many :comuna
end
