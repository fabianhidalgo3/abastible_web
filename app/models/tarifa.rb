# == Schema Information
#
# Table name: tarifas
#
#  id         :integer          not null, primary key
#  comunas_id :integer
#  valor_m3   :decimal(10, )
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Tarifa < ApplicationRecord
  belongs_to :comuna
  has_many :orden_lectura
  has_and_belongs_to_many :comuna
end
