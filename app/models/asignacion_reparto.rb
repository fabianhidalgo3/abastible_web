# == Schema Information
#
# Table name: asignacion_repartos
#
#  id               :integer          not null, primary key
#  ruta_reparto_id  :integer
#  empleado_id      :integer
#  orden_reparto_id :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
class AsignacionReparto < ApplicationRecord
  belongs_to :ruta_reparto
  belongs_to :empleado
  belongs_to :orden_reparto
end
