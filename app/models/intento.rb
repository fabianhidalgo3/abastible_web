# == Schema Information
#
# Table name: intentos
#
#  id                       :integer          not null, primary key
#  detalle_orden_lectura_id :integer
#  lectura                  :decimal(10, )
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#
class Intento < ApplicationRecord
  belongs_to :detalle_orden_lectura
end
