# == Schema Information
#
# Table name: carga_sistemas
#
#  id          :integer          not null, primary key
#  codigo      :string(255)
#  mes         :integer
#  ano         :integer
#  porcion     :integer
#  fecha_carga :datetime
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class CargaSistema < ApplicationRecord
end
