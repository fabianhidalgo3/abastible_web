# == Schema Information
#
# Table name: clave_convenios
#
#  id                :integer          not null, primary key
#  nombre            :string(255)
#  secuencia         :integer
#  mes               :integer
#  ano               :integer
#  activo            :boolean
#  efectivo          :boolean
#  requerido         :boolean
#  numero_fotorafias :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
class ClaveConvenio < ApplicationRecord
end
