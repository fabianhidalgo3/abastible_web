# == Schema Information
#
# Table name: tipo_convenios
#
#  id         :integer          not null, primary key
#  codigo     :string(255)
#  nombre     :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  rango_a    :integer
#  rango_b    :integer
#
class TipoConvenio < ApplicationRecord
end
