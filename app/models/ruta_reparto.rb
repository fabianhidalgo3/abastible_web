# == Schema Information
#
# Table name: ruta_repartos
#
#  id                 :integer          not null, primary key
#  codigo             :string(255)
#  mes                :integer
#  ano                :integer
#  abierto            :boolean
#  porcion_reparto_id :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  nombre             :string(255)
#
class RutaReparto < ApplicationRecord
  belongs_to :porcion_reparto
end
