# == Schema Information
#
# Table name: orden_convenios
#
#  id                   :integer          not null, primary key
#  codigo               :string(255)
#  secuencia            :integer
#  direccion            :string(255)
#  calle                :string(255)
#  numero               :integer
#  fecha_carga          :datetime
#  fecha_propuesta      :datetime
#  fecha_asignacion     :datetime
#  fecha_visita         :datetime
#  fecha_reagendada     :datetime
#  gps_latitud          :decimal(10, 7)
#  gps_longitud         :decimal(10, 7)
#  verificacion         :boolean
#  modulo_analisis      :integer
#  aprobado             :boolean
#  observacion_analista :string(255)
#  color                :integer
#  monto_deuda          :decimal(10, )
#  morosidad            :integer
#  abono                :decimal(10, )
#  numero_cuotas        :integer
#  valor_couta          :decimal(10, )
#  rut                  :string(255)
#  nombre               :string(255)
#  correo_electronico   :string(255)
#  telefono             :string(255)
#  observacion_gestor   :string(255)
#  medidor_id           :integer
#  instalacion_id       :integer
#  cliente_id           :integer
#  ruta_convenio_id     :integer
#  tipo_convenio_id     :integer
#  proceso_convenio_id  :integer
#  estado_convenio_id   :integer
#  comuna_id            :integer
#  clave_convenio_id    :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  tipo_tarjeta_id      :integer
#  numero_medidor       :string(255)
#  numero_cliente       :string(255)
#
class OrdenConvenio < ApplicationRecord
  belongs_to :proceso_convenio
  belongs_to :comuna
end
