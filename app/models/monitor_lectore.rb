# == Schema Information
#
# Table name: monitor_lectores
#
#  id                :integer          not null, primary key
#  rutum_id          :integer
#  empleado_id       :integer
#  asignacion        :integer
#  cargado           :integer
#  pendiente_visita  :integer
#  total_efectivo    :integer
#  total_no_efectivo :integer
#  leidos            :integer
#  efectividad       :string(255)
#  fecha_inicio      :datetime
#  fecha_termino     :datetime
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
class MonitorLectore < ApplicationRecord
  belongs_to :rutum
  belongs_to :empleado
end
