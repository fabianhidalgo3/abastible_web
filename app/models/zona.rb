# == Schema Information
#
# Table name: zonas
#
#  id         :integer          not null, primary key
#  codigo     :string(255)
#  nombre     :string(255)
#  region_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Zona < ApplicationRecord
  has_and_belongs_to_many :empleado
  has_and_belongs_to_many :subempresa
  has_many :asignacion
  belongs_to :region
  has_many :porcion

  def codigo_nombre
    if nombre.nil?
      @codigo_nombre = codigo
    else
      @codigo_nombre = nombre
    end
  end
end
