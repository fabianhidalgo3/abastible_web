class SalidasProviderWorker < ApplicationWorker
  require 'csv'
  require 'aws-sdk-s3'
  include Sidekiq::Worker
  sidekiq_options retry: false

	def perform(ruta_id)
		rutaCodigo=Rutum.find(ruta_id).codigo
		listaOrdenes = OrdenLectura.where(estado_lectura_id: [4..5], verificacion: false, rutum_id: ruta_id)
		p listaOrdenes
		header = "ID,REGION,SECUENCIA,COMUNA,PORCION,RUTA,MES,ANO,CALLE, NUMERO,DIRECCION, CTA CONTRATO,MEDIDOR,UBICACION MEDIDOR ,FECHA LECTURA,CLAVE LECTURA, LATITUD, LONGITUD, CONSUMO MINIMO, CONSUMO MAXIMO, LECTURA ANTERIOR, LECTURA ACTUAL, EMPLEADO \n"
		fecha = Time.now.strftime("%e%m%Y_ %H%M%S")
		p fecha
		file = rutaCodigo + "_" + fecha.to_s + ".csv"
		p file
    File.open(file, "w+:UTF-16LE:UTF-8") do |csv|
				 csv << header
				 p csv	
				 	listaOrdenes.each do |orden_lectura|
						p orden_lectura
						if !orden_lectura.fecha_ejecucion.nil?
							fecha_ejecucion = orden_lectura.fecha_ejecucion.strftime("%e/%m/%Y %H:%M:%S")
						else
							fecha_ejecucion = "00/00/00"
						end
						if !orden_lectura.asignacion_lectura.nil?
						 	empleado = orden_lectura.asignacion_lectura.empleado.nombre_completo
						else
						 	empleado = ""
						end
						csv << orden_lectura.id.to_s + "," +
									 orden_lectura.rutum.porcion.zona.region.nombre + "," +
									 orden_lectura.secuencia_lector.to_s + "," +
									 orden_lectura.comuna.nombre + "," +
									 orden_lectura.rutum.porcion.codigo.to_s + "," +
									 orden_lectura.rutum.codigo + "," +
									 orden_lectura.rutum.mes.to_s + "," +
									 orden_lectura.rutum.ano.to_s + "," +
									 orden_lectura.cliente.calle.to_s + "," +
									 orden_lectura.cliente.numero_domicilio.to_s + "," +
									 orden_lectura.direccion + "," +
									 orden_lectura.cliente.numero_cliente.to_s + "," +
									 orden_lectura.medidor.numero_medidor.to_s + "," +
									 orden_lectura.medidor.ubicacion_medidor.to_s + "," +
									 fecha_ejecucion + "," +
									 orden_lectura.clave_lectura.nombre + "," +
									 orden_lectura.gps_latitud.to_s+ "," +
									 orden_lectura.gps_longitud.to_s+ "," +
							 		 orden_lectura.consumo_maximo.to_s + "," +
									 orden_lectura.consumo_minimo.to_s + "," +
									 orden_lectura.lectura_anterior.to_s + "," +
								 	 orden_lectura.lectura_actual.to_s+ "," +
								 	 empleado + "\n" 

					end
			end
			p file
			s3 = Aws::S3::Resource.new(region:'us-east-1')
			obj = s3.bucket('provider-salidas').object(file)
			obj.upload_file(file)
			p obj
	end
	
end
