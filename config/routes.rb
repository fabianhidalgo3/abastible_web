Rails.application.routes.draw do
  mount ActionCable.server => "/cable"
  # > Ruta Principal
  root 'dashboard#index'
  devise_for :usuarios

  resources :lec_filtros do
    collection do
      get 'carga_zonas'
      get 'carga_porciones'
      get 'carga_rutas'
      get 'carga_lectores'
    end
  end

  resources :lec_consumos_cero do
    collection do
      get 'carga_ordenes'
    end
  end
  match 'analisis/consumos_cero' => 'lec_consumos_cero#index', via: [:get, :post]

  resources :lec_consumos_inferiores do
    collection do
      get 'exportar_csv'
      get 'carga_ordenes'
    end
  end
  #match 'analisis/consumos_inferiores' => 'lec_consumos_inferiores#carga_ordenes', via: [:get, :post]
  #match 'analisis/consumos_inferiores' => 'lec_consumos_inferiores#index', via: [:get, :post]

  resources :lec_consumos_maximos do
    collection do
      get 'exportar_csv'
      get 'carga_ordenes'
    end
  end
  match 'analisis/consumos_maximos' => 'lec_consumos_maximos#index', via: [:get, :post]

  resources :lec_improcedencias do
    collection do
      get 'carga_ordenes'
    end
  end
  match 'analisis/improcedencias' => 'lec_improcedencias#index', via: [:get, :post]
  match 'analisis/improcedencias/csv' => 'lec_improcedencias#exportar_csv', via: [:get]

  resources :lec_mensajes_alerta do
    collection do
      get 'aprobar_orden'
      get 'enviar_verificacion'
      get 'modificar_lectura'
    end
  end

  resources :lec_servicios_verificados do
    collection do
      get 'mensajes_alerta'
      get 'carga_ordenes'
    end
  end

  resources :lec_operaciones do
    collection do
      get 'ver_fotografia'
      get 'ver_ubicacion'
      get 'modificar_orden'
      get 'editar_orden'
      get 'ver_ubicacion_cliente'
      get 'actualizar_ubicacion'
      get 'remplazar_ubicacion'
    end
  end

  # > General
  resources :dashboard do
    collection do
      get 'carga_zonas'
      get 'carga_zonas_reparto'
      get 'carga_porciones'
      get 'carga_porciones_reparto'
      get 'carga_rutas'
      get 'carga_indicadores'
      get 'carga_indicadores_reparto'
      get 'carga_indicadores_cambio_medidor'
      get 'carga_indicadores_corte_reposicion'
      get 'carga_indicadores_gas_inicial'
      get 'carga_indicadores_conveniosria'
      get 'carga_graficos_asignacion'
      get 'carga_graficos_efectividad'
      get 'carga_graficos_efectividad_porcion'
      get 'descargar_manual_web'
      get 'descargar_manual_app'
      get 'carga_graficos_asignacion_reparto'
      get 'carga_graficos_efectividad_reparto'
      get 'carga_graficos_asignacion_corte_reposicion'
      get 'carga_graficos_efectividad_corte_reposicion'
      get 'carga_graficos_asignacion_cambio_medidor'
      get 'carga_graficos_efectividad_cambio_medidor'
      get 'carga_graficos_asignacion_gas_inicial'
      get 'carga_graficos_efectividad_gas_inicial'
      get 'carga_graficos_asignacion_convenios'
      get 'carga_graficos_efectividad_convenios'
    end
  end

  # > Rutas Catastros
  # > Perfiles de Usuarios
  resources :inv_perfiles do
    collection do
      get 'exportar_csv'
      get 'exportar_pdf'
    end
  end
  match 'inventarios/perfiles' => 'inv_perfiles#index', via: [:get, :post]

  # > Usuarios Administrativos
  resources :inv_usuarios_administrativos do
    collection do
    	post 'cambiar'
      get 'modificar_contrasena'
      get 'exportar_pdf'
      get 'carga_listado'
      get 'exportar_csv'
    end
  end
  match 'catastro/empleados/administrativos' => 'inv_usuarios_administrativos#index', via: [:post, :get]

  # > Usuarios Agente de Terreno
  resources :inv_usuarios_agentes_terreno do
    collection do
    	post 'cambiar'
      get 'modificar_contrasena'
      get 'exportar_pdf'
      get 'exportar_csv'
    end
  end
  match 'catastro/empleados/agentes_terreno' => 'inv_usuarios_agentes_terreno#index', via: [:post, :get]

  resources :inv_empleados do 
    collection do
      get 'exportar_pdf'
      get 'carga_listado'
      get 'carga_subempresas'
      get 'carga_zonas'
      get 'carga_modelos'
      get 'carga_equipos'
    end
  end
  match 'inventarios/empleados' => 'inv_empleados#index', via: [:post, :get]

  # > Claves y Observaciones de lectura
  resources :lec_claves do
    collection do
      get 'exportar_csv'
    end
  end
  match 'catastro/clave_lecturas' => 'lec_claves#index', via: [:get, :post]

  # > Routes Lectura
  # > Importar Ordenes de Lectura
  resources :lec_importar_ordenes_lecturas do
    collection do
      # get :importar_csv, :as => :importar_csv
      post :importar_xml, :as => :importar_xml
      get 'carga_zonas'
      post 'actualizar_clientes'
      get 'carga_porciones'
    end
  end
  match 'lectura/operaciones/importar_ordenes_lectura' => 'lec_importar_ordenes_lecturas#index', via: [:post, :get]
  match  'importar_ordenes_lecturas/import_csv'        =>          'lec_importar_ordenes_lecturas#import_csv', via: [:post]
  match  'importar_ordenes_lecturas/import_xml'        =>          'lec_importar_ordenes_lecturas#importar_xml', via: [:post]

  resources :lec_importar_despachos_postales do
    collection do
      # get :importar_csv, :as => :importar_csv
      get 'carga_zonas'
      post 'actualizar_clientes'
      get 'carga_porciones'
    end
  end
  match 'lectura/operaciones/importar_despachos_postales' => 'lec_importar_despachos_postales#index', via: [:post, :get]
  match  'importar_despachos_postales/import_csv'        =>          'lec_importar_despachos_postales#import_csv', via: [:post]




  # Asignacion de Ordenes de Lectura
  resources :lec_asignacion do
    collection do
      get 'carga_rutas'#Listado de Rutas a Asignar
      get 'carga_porciones'#listado de porciones abiertas
      get 'carga_comunas'#Listado de comunas por Zonal
      get 'carga_zonas' #Listado de Zonas por Región
      get 'dividir_ruta_asignacion'
      get 'dividir_ruta_desasignacion'
      get 'asignacion_lecturas'
      get 'asignacion_completa'
      get 'desasignacion_completa'
      get 'desasignacion_parcial'
      get 'asignacion_parcial'
    end
  end
  match 'lectura/operaciones/asignacion_lecturas' => 'lec_asignacion#index', via: [:post, :get]

  # Impresión de Ruteros
  resources :lec_impresion_ruteros do
    collection do
      get 'carga_filtro'
      get 'carga_porciones'
      get 'carga_rutas'
      get 'exportar_csv'
      get 'exportar_pdf'
    end
  end
  match 'lectura/operaciones/impresion_ruteros' => 'lec_impresion_ruteros#index', via: [:post, :get]

  # Monitor de Lectores en Terreno
  resources :lec_monitor_lectores do
    collection do
      get 'carga_filtro'
      get 'carga_ubicacion'
      get 'carga_zonas'
      get 'carga_porciones'
      get 'carga_rutas'
      get 'carga_comunas'
      get 'carga_lectores'
    end
  end
  match 'lectura/operaciones/monitor_lectores' => 'lec_monitor_lectores#index', via: [:post, :get]

  # Cerrar Procesos de lectura
  resources :lec_cierre_lecturas do
    collection do
      get 'carga_porciones'
      get 'carga_zonas'
      get 'carga_cierre'
      get 'carga_rutas'
      get 'descargar_archivo'
      get 'cerrar_proceso'
    end
  end
  match 'lectura/operaciones/cierre_lecturas' => 'lec_cierre_lecturas#index', via: [:post, :get]

  # Lecturas Dictadas por Cliente
  resources :lec_lectura_dictada_cliente do
    collection do
      get 'tabla_contenido'
      get 'form_lectura_dictada'
      get 'agregar_lectura_dictada'
      get 'alertas'
    end
  end
  match 'lectura/analisis_online/lectura_dictada_cliente' => 'lec_lectura_dictada_cliente#index', via: [:post, :get]

  # Historico de lecturas de un cliente
  resources :lec_historico_lecturas do
    collection do
      get 'carga_filtro'
      get 'carga_fotografia'
      get 'exportar_csv'
      get 'exportar_pdf'
      get 'carga_ubicacion'
    end
  end
  match 'lectura/analisis_online/historico_lecturas' => 'lec_historico_lecturas#index', via: [:get, :post]

    # Historico de lecturas de un cliente
    resources :lec_historico_improcedencias do
      collection do
        get 'carga_filtro'
        get 'carga_zonas'
        get 'exportar_csv'
        get 'carga_porciones'
      end
    end
    match 'lectura/analisis_online/historico_improcedencias' => 'lec_historico_improcedencias#index', via: [:get, :post]

  # Clientes Pendientes de Verificación
  resources :lec_clientes_repaso do
    collection do
      get 'exportar_csv'
      get 'carga_ordenes'
    end
  end
  match 'lectura/analisis_online/clientes_pendientes_verificacion' => 'lec_clientes_repaso#index', via: [:post, :get]


  # Reporte Efectividad Porción
  resources :lec_reporte_efectividad_porcion do
    collection do
      get 'carga_porciones'
      get 'carga_zonas'
      get 'carga_filtro'
      get 'exportar_csv'
      get 'exportar_pdf'
    end
  end
  match 'lectura/reportes/efectividad_porcion' => 'lec_reporte_efectividad_porcion#index', via: [:post, :get]

  # Resumen General Lecturas
  resources :lec_reporte_resumen_general_lecturas do
    collection do
      get 'carga_porciones'
      get 'carga_zonas'
      get 'carga_lectores'
      get 'carga_filtro'
      get 'exportar_csv'
      get 'exportar_pdf'
    end
  end
  match 'lectura/reportes/resumen_general_lecturas' => 'lec_reporte_resumen_general_lecturas#index', via: [:post, :get]
  #Reporte Efectividad por porcion
	resources :lec_reporte_efectividad_lector do
    collection do
      get 'carga_lectores'
      get 'carga_zonas'
      get 'carga_filtro'
      get 'exportar_csv'
    end
  end
  match 'lectura/reportes/efectividad_lector' => 'lec_reporte_efectividad_lector#index', via: [:post, :get]

  resources :lec_reporte_visitas do # => OK
    collection do
      get 'carga_rutas'
      get 'carga_porciones'
      get 'carga_zonas'
      get 'carga_filtro'
      get 'carga_ubicacion'
      get 'exportar_csv'
      get 'exportar_pdf'
    end
  end
	match 'lectura/reportes/visitas' => 'lec_reporte_visitas#index', via: [:post, :get]

  resources :lec_reporte_control_coordenadas do # => OK
    collection do
      get 'carga_rutas'
      get 'carga_porciones'
      get 'carga_zonas'
      get 'carga_filtro'
      get 'carga_ubicacion'
      get 'exportar_csv'
      get 'exportar_pdf'
    end
  end
  match 'lectura/reportes/control_coordenadas_gps' => 'lec_reporte_control_coordenadas#index', via: [:post, :get]
  #Reporte numero de intentos por lector
  resources :lec_reporte_numero_intentos_lector do
    collection do
      get 'carga_porciones'
      get 'carga_empleados'
      get 'carga_comunas'
      get 'carga_filtro'
      get 'exportar_excel'
      get 'exportar_pdf'
    end
  end
  match 'lectura/reportes/numero_intentos_lector' => 'lec_reporte_numero_intentos_lector#index', via: [:post, :get]

  # Reporte Tipo de Claves
	resources :lec_reporte_tipos_claves do
    collection do
      get 'carga_porciones'
      get 'carga_filtro'
      get 'exportar_csv'
    end
  end
  match 'lectura/reportes/tipos_claves' => 'lec_reporte_tipos_claves#index', via: [:post, :get]


  resources :lec_reporte_control_horas_terreno do
    collection do
      get 'carga_porciones'
      get 'carga_zonas'
      get 'carga_filtro'
      get 'exportar_csv'
    end
  end
  match 'lectura/reportes/control_horas_lector_terreno' => 'lec_reporte_control_horas_terreno#index', via: [:post, :get]

  resources :lec_reporte_lecturas_estimadas do
    collection do
      get 'carga_porciones'
      get 'carga_zonas'
      get 'carga_filtro'
      get 'exportar_csv'
      get 'exportar_pdf'
    end
  end
  match 'lectura/reportes/lecturas_estimadas' => 'lec_reporte_lecturas_estimadas#index', via: [:post, :get]

  resources :lec_reporte_gestion_calidad do
    collection do
      get 'carga_porciones'
      get 'carga_zonas'
      get 'carga_filtro'
      get 'exportar_csv'
      get 'exportar_pdf'
    end
  end
  match 'lectura/reportes/gestion_calidad' => 'lec_reporte_gestion_calidad#index', via: [:post, :get]

  resources :lec_reporte_lectura_dictada_cliente do
    collection do
      get 'carga_porciones'
      get 'carga_zonas'
      get 'carga_filtro'
      get 'exportar_csv'
      get 'exportar_pdf'
    end
  end
  match 'lectura/reportes/lectura_dictada_cliente' => 'lec_reporte_lectura_dictada_cliente#index', via: [:post, :get]



	resources :lec_reporte_nveces_sinlecturaefectiva do
    collection do
      get 'carga_porciones'
      get 'carga_filtro'
      get 'carga_comunas'
      get 'exportar_csv'
      get 'exportar_pdf'
    end
	end
  match 'reportes/nveces_lectura_efectiva' => 'lec_reporte_nveces_sinlecturaefectiva#index', via: [:post, :get]


  # ---------------------------------------------------------------------- Reparto-------###############
  resources :rep_monitor_repartidores do #Monitor de Repartidores
    collection do
      get 'carga_filtro'
      get 'carga_ubicacion'
      get 'carga_zonas'
      get 'carga_porciones'
      get 'carga_rutas'
      get 'carga_comunas'
      get 'carga_lectores'
    end
  end
  match 'reparto/operaciones/monitor_repartidores' => 'rep_monitor_repartidores#index', via: [:post, :get]


	resources :rep_cerrar_procesos_repartos do #Cerrar Procesos de Repartos
		collection do
			get 'carga_filtro'
		end
	end
  match 'reparto/operaciones/cerrar_procesos_repartos' => 'rep_cerrar_procesos_repartos#index', via: [:post, :get]

  resources :rep_visualizacion_entregas do #Historico de Documentos
		collection do
			get 'carga_ordenes'
      get 'carga_fotografia'
      get 'carga_rutas'
      get 'carga_porciones'
      get 'carga_zonas'
			get 'exportar_excel'
			get 'exportar_pdf'
		end
	end
  match 'reparto/gestion_repartos/visualizacion_entregas' => 'rep_visualizacion_entregas#index', via: [:post, :get]

	resources :rep_historico_documentos do #Historico de Documentos
		collection do
			get 'carga_filtro'
      get 'carga_ubicacion'
			get 'carga_fotografia'
			get 'exportar_excel'
			get 'exportar_pdf'
		end
	end
  match 'reparto/gestion_repartos/historico_documentos' => 'rep_historico_documentos#index', via: [:post, :get]

	resources :devoluciones do #Devolución de Documentos
		collection do
			get 'carga_filtro'
			get 'crear_devolucion'
		end
	end
  match 'reparto/gestion_repartos/devoluciones' => 'devoluciones#index', via: [:post, :get]

	resources :completitud_repartos #Reporte de Efectividad de Procesos
  match 'reparto/reportes/efectividad_procesos' => 'completitud_repartos#index', via: [:post, :get]


	resources :reporte_devoluciones do #Reporte de Devoluciones
		collection do
			get 'carga_filtro'
		end
	end
  match 'reparto/reportes/devoluciones' => 'reporte_devoluciones#index', via: [:post, :get]

  resources :reporte_control_entregas do #Reporte Control de Entregas
		collection do
			get 'carga_filtro'
		end
	end
  match 'reparto/reportes/control_entregas' => 'reporte_control_entregas#index', via: [:post, :get]

  # ---------------------------------------------------------------------- Mantenedores
  resources :empleados do
    collection do
      get 'carga_subempresas'
      get 'carga_zonas'
      get 'carga_equipos'
      get 'carga_modelos'
      get 'carga_contratistas'
    end
  end

  # -------------------------------------------------------------------------Repartos

  # Asignacion de Ordenes de Lectura
  resources :rep_asignacion do
    collection do
      get 'carga_rutas'#Listado de Rutas a Asignar
      get 'carga_porciones'#listado de porciones abiertas
      get 'carga_comunas'#Listado de comunas por Zonal
      get 'carga_zonas' #Listado de Zonas por Región
      get 'dividir_ruta_asignacion'
      get 'dividir_ruta_desasignacion'
      get 'asignacion_completa'
      get 'desasignacion_completa'
      get 'desasignacion_parcial'
      get 'asignacion_parcial'
    end
  end
  match 'reparto/operaciones/asignacion_repartos' => 'rep_asignacion#index', via: [:post, :get]

  resources :rep_reporte_efectividad_porcion do
    collection do
      get 'carga_porciones'
      get 'carga_zonas'
      get 'carga_filtro'
      get 'exportar_csv'
      get 'exportar_pdf'
    end
  end
  match 'reparto/reportes/efectividad_porcion_reparto' => 'rep_reporte_efectividad_porcion#index', via: [:post, :get]

  resources :rep_reporte_control_coordenadas do # => OK
    collection do
      get 'carga_rutas'
      get 'carga_porciones'
      get 'carga_zonas'
      get 'carga_filtro'
      get 'carga_ubicacion'
      get 'exportar_csv'
      get 'exportar_pdf'
    end
  end
	match 'reparto/reportes/control_coordenadas_gps' => 'rep_reporte_control_coordenadas#index', via: [:post, :get]

  resources :rep_reporte_control_horario do
    collection do
      get 'carga_porciones'
      get 'carga_empleados'
      get 'carga_comunas'
      get 'carga_filtro'
      get 'exportar_csv'
    end
  end
  match 'reparto/reportes/control_horas' => 'rep_reporte_control_horario#index', via: [:post, :get]


 # ---------------------------------------------------------------------- Corte y Reposición
 resources :cr_importar_ordenes do
  collection do
    # get :importar_csv, :as => :importar_csv
    post :importar_xml, :as => :importar_xml
    get 'carga_zonas'
    post 'actualizar_clientes'
    get 'carga_porciones'
  end
end
match 'corte_reposicion/operaciones/importar_ordenes' => 'cr_importar_ordenes#index', via: [:post, :get]
match  'importar_ordenes/import_csv'        =>          'cr_importar_ordenes#import_csv', via: [:post]
 
 
 
 # Asignacion de Ordenes de Lectura
  resources :cr_asignacion_ordenes do
    collection do
      get 'carga_rutas'#Listado de Rutas a Asignar
      get 'carga_porciones'#listado de porciones abiertas
      get 'carga_comunas'#Listado de comunas por Zonal
      get 'carga_zonas' #Listado de Zonas por Región
      get 'dividir_ruta_asignacion'
      get 'dividir_ruta_desasignacion'
      get 'asignacion_lecturas'
      get 'asignacion_completa'
      get 'desasignacion_completa'
      get 'desasignacion_parcial'
      get 'asignacion_parcial'
    end
  end
  match 'corte_reposicion/operaciones/asignacion_corte_reposicion' => 'cr_asignacion_ordenes#index', via: [:post, :get]

  resources :cr_monitor_ordenes do
    collection do
      get 'carga_filtro'
      get 'carga_ubicacion'
      get 'carga_zonas'
      get 'carga_porciones'
      get 'carga_rutas'
      get 'carga_comunas'
      get 'carga_lectores'
    end
  end
  match 'corte_reposicion/operaciones/monitor_corte_reposicion' => 'cr_monitor_ordenes#index', via: [:post, :get]

  resources :cr_claves_reposicion do
    collection do
      get 'exportar_csv'
    end
  end
  match 'corte_reposicion/operaciones/clave_corte_reposicion' => 'cr_claves#index', via: [:post, :get]

  resources :cr_historico_trabajos do
    collection do
      get 'carga_filtro'
      get 'carga_ubicacion'
    end
  end
  match 'corte_reposicion/historico/historico_corte_reposicion' => 'cr_historico_trabajos#index', via: [:post, :get]

  resources :visualizacion_trabajos do
    collection do
      get 'carga_filtro'
      get 'carga_ubicacion'
    end
  end
  match 'corte_reposicion/auditoria/visualizacion_trabajos_corte_reposicion' => 'cr_visualizacion_trabajos#index', via: [:post, :get]

  resources :cr_reporte_efectividad_tecnico do
    collection do
      get 'carga_filtro'
    end
  end
  match 'corte_reposicion/reportes/efectividad_tecnico' => 'cr_reporte_efectividad_tecnico#index', via: [:post, :get]

  resources :cr_reporte_efectividad_porcion do
    collection do
      get 'carga_filtro'
    end
  end
  match 'corte_reposicion/reportes/efectividad_porcion' => 'cr_reporte_efectividad_porcion#index', via: [:post, :get]

  resources :cr_reporte_coordenadas_gps do
    collection do
      get 'carga_filtro'
    end
  end
  match 'corte_reposicion/reportes/coordenadas_gps' => 'cr_reporte_coordenadas_gps#index', via: [:post, :get]

  resources :cr_reporte_horas_tecnico do
    collection do
      get 'carga_filtro'
    end
  end
  match 'corte_reposicion/reportes/control_horarios' => 'cr_reporte_horas_tecnico#index', via: [:post, :get]

 # ---------------------------------------------------------------------- Cambio Medidores
 # > Routes Lectura
 # > Importar Ordenes de Lectura
 resources :cm_importar_ordenes do
   collection do
     get :importar_csv, :as => :importar_csv
     get 'carga_zonas'
     get 'carga_porciones'
   end
 end
 match 'cambio_medidor/importar/importar_ordenes_cambio_medidor' => 'cm_importar_ordenes#index', via: [:post, :get]
 match  'importar_ordenes_cambio_medidor/import_csv' => 'cm_importar_ordenes#import_csv', via: [:post]

 resources :cm_asignacion_ordenes do
  collection do
    get 'carga_rutas'#Listado de Rutas a Asignar
    get 'carga_porciones'#listado de porciones abiertas
    get 'carga_comunas'#Listado de comunas por Zonal
    get 'carga_zonas' #Listado de Zonas por Región
    get 'dividir_ruta_asignacion'
    get 'dividir_ruta_desasignacion'
    get 'asignacion_lecturas'
    get 'asignacion_completa'
    get 'desasignacion_completa'
    get 'desasignacion_parcial'
    get 'asignacion_parcial'
  end
end
match 'cambio_medidor/operaciones/asignacion' => 'cm_asignacion_ordenes#index', via: [:post, :get]

resources :cm_monitor_ordenes do
  collection do
    get 'carga_filtro'
  end
end
match 'cambio_medidor/operaciones/monitor_tecnicos' => 'cm_monitor_ordenes#index', via: [:post, :get]

resources :cm_claves do
  collection do
    get 'carga_filtro'
  end
end
match 'cambio_medidor/catastros/claves_cambio_medidor' => 'cm_claves#index', via: [:post, :get]

resources :cm_historico_visitas do
  collection do
    get 'carga_filtro'
  end
end
match 'cambio_medidor/historicos/historico_visitas' => 'cm_historico_visitas#index', via: [:post, :get]

resources :cm_servicios_improcedentes do
  collection do
    get 'carga_filtro'
  end
end
match 'cambio_medidor/auditoria/servicios_improcedentes' => 'cm_servicios_improcedentes#index', via: [:post, :get]

resources :cm_validacion_ejecutados do
  collection do
    get 'carga_filtro'
  end
end
match 'cambio_medidor/auditoria/validacion_ejecutados' => 'cm_validacion_ejecutados#index', via: [:post, :get]

resources :cm_reporte_efectividad_porcion do
  collection do
    get 'carga_filtro'
  end
end
match 'cambio_medidor/reportes/efectividad_porcion' => 'cm_reporte_efectividad_porcion#index', via: [:post, :get]

resources :cm_reporte_efectividad_tecnico do
  collection do
    get 'carga_filtro'
  end
end
match 'cambio_medidor/reportes/efectividad_tecnico' => 'cm_reporte_efectividad_tecnico#index', via: [:post, :get]

resources :cm_reporte_coordenadas_gps do
  collection do
    get 'carga_filtro'
  end
end
match 'cambio_medidor/reportes/coordenadas_gps' => 'cm_reporte_coordenadas_gps#index', via: [:post, :get]

resources :cm_reporte_horas_tecnico do
  collection do
    get 'carga_filtro'
  end
end
match 'cambio_medidor/reportes/control_horario' => 'cm_reporte_horas_tecnico#index', via: [:post, :get]


resources :cm_cuadratura_mensual do
  collection do
    get 'carga_filtro'
  end
end
match 'cambio_medidor/reportes/cuadratura_mensual' => 'cm_cuadratura_mensual#index', via: [:post, :get]


# ---------------------------------------------------------------------- Gas Inicial
resources :gi_asignacion_ordenes do
  collection do
    get 'carga_rutas'#Listado de Rutas a Asignar
    get 'carga_porciones'#listado de porciones abiertas
    get 'carga_comunas'#Listado de comunas por Zonal
    get 'carga_zonas' #Listado de Zonas por Región
    get 'dividir_ruta_asignacion'
    get 'dividir_ruta_desasignacion'
    get 'asignacion_lecturas'
    get 'asignacion_completa'
    get 'desasignacion_completa'
    get 'desasignacion_parcial'
    get 'asignacion_parcial'
  end
end
match 'gas_inicial/operaciones/asignacion' => 'gi_asignacion_ordenes#index', via: [:post, :get]

resources :gi_monitor_ordenes do
  collection do
    get 'carga_filtro'
  end
end
match 'gas_inicial/operaciones/monitor_tecnicos' => 'gi_monitor_ordenes#index', via: [:post, :get]

resources :gi_claves do
  collection do
    get 'carga_filtro'
  end
end
match 'gas_inicial/catastros/claves_gas_inicial' => 'gi_claves#index', via: [:post, :get]

resources :gi_historico_trabajos do
  collection do
    get 'carga_filtro'
  end
end
match 'gas_inicial/historicos/historico_trabajos' => 'gi_historico_trabajos#index', via: [:post, :get]

resources :gi_servicios_improcedentes do
  collection do
    get 'carga_filtro'
  end
end
match 'gas_inicial/auditoria/servicios_improcedentes' => 'gi_servicios_improcedentes#index', via: [:post, :get]

resources :gi_visualizacion_ejecutados do
  collection do
    get 'carga_filtro'
  end
end
match 'gas_inicial/auditoria/visualizacion_ejecutados' => 'gi_visualizacion_ejecutados#index', via: [:post, :get]

resources :gi_reporte_efectividad_porcion do
  collection do
    get 'carga_filtro'
  end
end
match 'gas_inicial/reportes/efectividad_porcion' => 'gi_reporte_efectividad_porcion#index', via: [:post, :get]

resources :gi_reporte_efectividad_tecnico do
  collection do
    get 'carga_filtro'
  end
end
match 'gas_inicial/reportes/efectividad_tecnico' => 'gi_reporte_efectividad_tecnico#index', via: [:post, :get]

resources :gi_reporte_coordenadas_gps do
  collection do
    get 'carga_filtro'
  end
end
match 'gas_inicial/reportes/coordenadas_gps' => 'gi_reporte_coordenadas_gps#index', via: [:post, :get]

resources :gi_reporte_horas_tecnico do
  collection do
    get 'carga_filtro'
  end
end
match 'gas_inicial/reportes/control_horario' => 'gi_reporte_horas_tecnico#index', via: [:post, :get]


# -----------------------------------------------------------------------Gestión Convenios
resources :con_importar_ordenes do
  collection do
    post :importar_csv, :as => :importar_csv
    #post :importar_xml, :as => :importar_xml
  end
end
match 'convenios/importar/importar_ordenes' => 'con_importar_ordenes#index', via: [:post, :get]
match  'importar_ordenes_convenios/import_csv'        =>          'con_importar_ordene#import_csv', via: [:post]

resources :con_asignacion_ordenes do
  collection do
    get 'carga_rutas'#Listado de Rutas a Asignar
    get 'carga_porciones'#listado de porciones abiertas
    get 'carga_comunas'#Listado de comunas por Zonal
    get 'carga_zonas' #Listado de Zonas por Región
    get 'dividir_ruta_asignacion'
    get 'dividir_ruta_desasignacion'
    get 'asignacion_lecturas'
    get 'asignacion_completa'
    get 'desasignacion_completa'
    get 'desasignacion_parcial'
    get 'asignacion_parcial'
  end
end
match 'convenios/operaciones/asignacion' => 'con_asignacion_ordenes#index', via: [:post, :get]

resources :gi_monitor_ordenes do
  collection do
    get 'carga_filtro'
  end
end
match 'convenios/operaciones/monitor_agentes' => 'con_monitor_agentes#index', via: [:post, :get]

resources :gi_claves do
  collection do
    get 'carga_filtro'
  end
end
match 'convenios/catastros/claves_convenios' => 'con_claves_convenios#index', via: [:post, :get]

resources :con_historico_trabajos do
  collection do
    get 'carga_filtro'
  end
end
match 'convenios/historicos/historico_trabajos' => 'con_historico_convenios#index', via: [:post, :get]

resources :con_visualizacion_improcedentes do
  collection do
    get 'carga_filtro'
  end
end
match 'convenios/auditoria/servicios_improcedentes' => 'con_visualizacion_improcedentes#index', via: [:post, :get]

resources :con_visualizacion_ejecutados do
  collection do
    get 'carga_filtro'
  end
end
match 'convenios/auditoria/visualizacion_ejecutados' => 'con_visualizacion_ejecutados#index', via: [:post, :get]

resources :con_reporte_efectividad_cartera do
  collection do
    get 'carga_filtro'
  end
end
match 'convenios/reportes/efectividad_cartera' => 'con_reporte_efectividad_cartera#index', via: [:post, :get]

resources :con_reporte_coordenadas_gps do
  collection do
    get 'carga_filtro'
  end
end
match 'convenios/reportes/coordenadas_gps' => 'con_reporte_coordenadas_gps#index', via: [:post, :get]

resources :con_reporte_horas_agente do
  collection do
    get 'carga_filtro'
  end
end
match 'convenios/reportes/control_horario' => 'con_reporte_horas_agente#index', via: [:post, :get]
 # ---------------------------------------------------------------------- Facturación

  resources :libro_ventas do
    collection do
      get 'crear_libro'
      get 'rectificar_libro'
    end
  end

  resources :folios_disponibles do
    collection do
    end
  end

  resources :documentos_emitidos do
    collection do
      get 'carga_filtro'
    end
  end

  resources :pendiente_facturacion do
    collection do
    end
  end

  resources :facturacion do
    collection do
      get 'facturacion_pendiente'
      get 'consumo_folios'
      get 'index'
    end
  end



  #Colores Modulos
  resources :modulos_colors do
    collection do
      get 'convenios'
    end
  end
end
