class AddNumeroClienteToOrdenConvenio < ActiveRecord::Migration[5.0]
  def change
    add_column :orden_convenios, :numero_medidor, :string
    add_column :orden_convenios, :numero_cliente, :string
  end
end
