class CreateClaveConvenios < ActiveRecord::Migration[5.0]
  def change
    create_table :clave_convenios do |t|
      t.string :nombre
      t.integer :secuencia
      t.integer :mes
      t.integer :ano
      t.boolean :activo
      t.boolean :efectivo
      t.boolean :requerido
      t.integer :numero_fotorafias

      t.timestamps
    end
  end
end
