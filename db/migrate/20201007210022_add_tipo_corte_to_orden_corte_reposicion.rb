class AddTipoCorteToOrdenCorteReposicion < ActiveRecord::Migration[5.0]
  def change
    add_column :orden_corte_reposicions, :tipo_corte, :integer
  end
end
