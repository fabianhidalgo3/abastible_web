class AddMorosidadToOrdenCorteReposicion < ActiveRecord::Migration[5.0]
  def change
    add_column :orden_corte_reposicions, :numero_medidor, :text
    add_column :orden_corte_reposicions, :numero_cliente, :text
    add_column :orden_corte_reposicions, :morosidad, :integer
  end
end
