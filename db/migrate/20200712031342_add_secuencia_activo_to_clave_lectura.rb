class AddSecuenciaActivoToClaveLectura < ActiveRecord::Migration[5.0]
  def change
    add_column :clave_lecturas, :secuencia, :integer
    add_column :clave_lecturas, :activo, :boolean
  end
end
