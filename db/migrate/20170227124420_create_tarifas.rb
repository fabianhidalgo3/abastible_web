class CreateTarifas < ActiveRecord::Migration[5.0]
  def change
    create_table :tarifas do |t|

      t.references :comunas, foreign_key: true
      t.decimal :valor_m3
      t.timestamps
    end
  end
end
