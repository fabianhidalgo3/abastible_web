class CreateRutaGasInicials < ActiveRecord::Migration[5.0]
  def change
    create_table :ruta_gas_inicials do |t|
      t.string :codigo
      t.string :nombre
      t.integer :mes
      t.integer :ano
      t.boolean :abierto
      t.references :porcion_gas_inicial, foreign_key: true
      t.timestamps
    end
  end
end
