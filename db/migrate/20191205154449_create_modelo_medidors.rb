class CreateModeloMedidors < ActiveRecord::Migration[5.0]
  def change
    create_table :modelo_medidors do |t|
      t.string :codigo
      t.string :nombre
      t.references :marca_medidor, foreign_key: true

      t.timestamps
    end
  end
end
