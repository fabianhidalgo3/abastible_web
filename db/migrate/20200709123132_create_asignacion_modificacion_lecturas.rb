class CreateAsignacionModificacionLecturas < ActiveRecord::Migration[5.0]
  def change
    create_table :asignacion_modificacion_lecturas do |t|
      t.references :empleado, foreign_key: true
      t.references :orden_lectura, foreign_key: true
      t.decimal :lectura_actual
      t.references :clave_lectura, foreign_key: true
      t.datetime :fecha_modificacion

      t.timestamps
    end
  end
end
