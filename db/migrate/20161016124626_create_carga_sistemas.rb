class CreateCargaSistemas < ActiveRecord::Migration[5.0]
  def change
    create_table :carga_sistemas do |t|
      t.string :codigo
      t.integer :mes
      t.integer :ano
      t.integer :porcion
      t.datetime :fecha_carga

      t.timestamps
    end
  end
end
