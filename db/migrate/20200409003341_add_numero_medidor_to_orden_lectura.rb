class AddNumeroMedidorToOrdenLectura < ActiveRecord::Migration[5.0]
  def change
    add_column :orden_lecturas, :numero_medidor, :string
  end
end
