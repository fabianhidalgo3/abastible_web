class AddModeloMedidorToMedidor < ActiveRecord::Migration[5.0]
  def change
    add_reference :medidors, :modelo_medidor, foreign_key: true
  end
end
