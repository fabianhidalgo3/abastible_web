class CreateAsignacionCambioMedidors < ActiveRecord::Migration[5.0]
  def change
    create_table :asignacion_cambio_medidors do |t|
      t.references :ruta_cambio_medidor, foreign_key: true
      t.references :orden_cambio_medidor, foreign_key: true
      t.references :empleado, foreign_key: true
      t.timestamps
    end
  end
end
