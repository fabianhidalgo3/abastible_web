class CreateMedidors < ActiveRecord::Migration[5.0]
  def change
    create_table :medidors do |t|
      t.string :numero_medidor
      t.integer :numero_digitos
      t.integer :diametro
      t.integer :ano
      t.string :ubicacion_medidor
      t.timestamps
    end
  end
end
