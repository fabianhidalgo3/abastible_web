class ChangePresicionGpsReparto < ActiveRecord::Migration[5.0]
  def change
       change_column :orden_repartos, :gps_latitud, :decimal, :precision => 10, :scale => 7
       change_column :orden_repartos, :gps_longitud, :decimal, :precision => 10, :scale => 7
  end
end
