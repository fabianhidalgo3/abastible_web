class AddActiveToUsuario < ActiveRecord::Migration[5.0]
  def change
    add_column :usuarios, :activo, :boolean
    add_column :usuarios, :imei, :string
  end
end
