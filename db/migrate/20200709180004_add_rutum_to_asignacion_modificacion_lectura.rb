class AddRutumToAsignacionModificacionLectura < ActiveRecord::Migration[5.0]
  def change
    add_reference :asignacion_modificacion_lecturas, :rutum, foreign_key: true
  end
end
