class CreateClaveLecturas < ActiveRecord::Migration[5.0]
  def change
    create_table :clave_lecturas do |t|
      t.string :codigo
      t.string :nombre
      t.boolean :requerido
      t.boolean :efectivo
      t.boolean :factura
      t.integer :numero_fotografias
      t.timestamps
    end
  end
end
