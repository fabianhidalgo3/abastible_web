class CreateClaveCorteReposicions < ActiveRecord::Migration[5.0]
  def change
    create_table :clave_corte_reposicions do |t|
      t.string :codigo
      t.string :nombre
      t.boolean :requerido
      t.boolean :efectivo
      t.boolean :activo
      t.integer :numero_fotografias
      t.integer :secuencia
      t.timestamps
    end
  end
end
