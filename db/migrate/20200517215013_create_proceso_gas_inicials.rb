class CreateProcesoGasInicials < ActiveRecord::Migration[5.0]
  def change
    create_table :proceso_gas_inicials do |t|
      t.string :nombre
      t.timestamps
    end
  end
end
