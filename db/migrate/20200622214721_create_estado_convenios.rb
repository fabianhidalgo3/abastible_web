class CreateEstadoConvenios < ActiveRecord::Migration[5.0]
  def change
    create_table :estado_convenios do |t|
      t.string :codigo
      t.string :nombre
      t.timestamps
    end
  end
end
