class Clave
  def initialize
    ClaveCambioMedidore.create(activo: true, nombre:"Cambio normal", efectivo: true, secuencia: 1, requerido: true)
    ClaveCambioMedidore.create(activo: true, nombre:"Casa cerrada", efectivo: false, secuencia: 2, requerido: false)
    ClaveCambioMedidore.create(activo: true, nombre:"Cliente no permite cambio", efectivo: true, secuencia: 3, requerido: false)
    ClaveCambioMedidore.create(activo: true, nombre:"Dificil Acceso", efectivo: false, secuencia: 4, requerido: false)
    ClaveCambioMedidore.create(activo: true, nombre:"Deshabitado", efectivo: false, secuencia: 5, requerido: false)
    ClaveCambioMedidore.create(activo: true, nombre:"Nicho con llave", efectivo: false, secuencia: 6, requerido: false)
    ClaveCambioMedidore.create(activo: true, nombre:"Medidor enterrado o mal empotrado", efectivo: false, secuencia: 7, requerido: false)
    ClaveCambioMedidore.create(activo: true, nombre:"Medidor nuevo", efectivo: false, secuencia: 8, requerido: false)
    ClaveCambioMedidore.create(activo: true, nombre:"Dirección sin ubicar", efectivo: false, secuencia: 9, requerido: false)
    ClaveCambioMedidore.create(activo: true, nombre:"Medidor no ubicado", efectivo: false, secuencia: 10, requerido: false)
    ClaveCambioMedidore.create(activo: true, nombre:"Llave de paso en mal estado", efectivo: false, secuencia: 11, requerido: false)
    ClaveCambioMedidore.create(activo: true, nombre:"No corresponde datos del medidor", efectivo: false, secuencia: 12, requerido: false)
    ClaveCambioMedidore.create(activo: true, nombre:"Suministro cortado", efectivo: false, secuencia: 13, requerido: false)
    ClaveCambioMedidore.create(activo: true, nombre:"Sitio eriazo", efectivo: false, secuencia: 14, requerido: false)
    ClaveCambioMedidore.create(activo: true, nombre:"Arranque sin medidor", efectivo: false, secuencia: 15, requerido: false)
    ClaveCambioMedidore.create(activo: true, nombre:"Medidor Invertidoe", efectivo: false, secuencia: 16, requerido: false)
    ClaveCambioMedidore.create(activo: true, nombre:"Nicho con llave", efectivo: false, secuencia: 17, requerido: false)
    
    
    ClaveCorteReposicion.create(activo: true, secuencia: 1, nombre:"Corte efectivo", efectivo: true, requerido: true)
    ClaveCorteReposicion.create(activo: true, secuencia: 2, nombre:"Reposición efectivo", efectivo: true, requerido: true)
    ClaveCorteReposicion.create(activo: true, secuencia: 3, nombre:"Casa Cerrada", efectivo: true, requerido: true)
    ClaveCorteReposicion.create(activo: true, secuencia: 4, nombre:"Cliente impide corte", efectivo: true, requerido: true)
    ClaveCorteReposicion.create(activo: true, secuencia: 5, nombre:"Cortado Anteriormente", efectivo: true, requerido: true)
    ClaveCorteReposicion.create(activo: true, secuencia: 6, nombre:"Cliente pago o abono", efectivo: true, requerido: true)
    ClaveCorteReposicion.create(activo: true, secuencia: 7, nombre:"Problemas Técnicos", efectivo: true, requerido: true)
    ClaveCorteReposicion.create(activo: true, secuencia: 8, nombre:"Propiedad Deshabitada", efectivo: true, requerido: true)
    ClaveCorteReposicion.create(activo: true, secuencia: 9, nombre:"Servicio Eliminado", efectivo: true, requerido: true)
    ClaveCorteReposicion.create(activo: true, secuencia: 10, nombre:"Medidor distinto al señalado", efectivo: true, requerido: true)
  end
end

Clave.new